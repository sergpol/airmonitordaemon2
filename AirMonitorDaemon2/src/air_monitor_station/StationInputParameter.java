/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

/**
 *
 * @author SAP
 */
public class StationInputParameter {
    private byte fCode;
    private String fVal;
    
    public StationInputParameter(byte code, String val){
        fCode = code;
        fVal = val;
    }
    
    public byte getCode(){
        return fCode;
    }
    
    public String getVal(){
        return fVal;
    }
}
