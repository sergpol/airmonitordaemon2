/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorDefs;
import air_monitor_station.SensorParams;
import air_monitor_station.Voltage;

public abstract class ResistiveSensorType extends AnalogSensorType{
    private static final double Vref = 1200.0; //мВ
    protected boolean checkRange = false;
    protected double fromRange = 0;
    protected double toRange = 0;
    protected double A = 0;
    protected double B = 0;
    //protected int sensorType = 0; //Sensors
//    private double Rl = 0; //sensor parameter
//    private double R0 = 0; //calibration parameter
    private double fVal; 
    
    
    public ResistiveSensorType(){
        super();
    }
    
    public void setCheckRange(boolean val){
        checkRange = val;
    }
    
    @Override
    public double getVal(){
        return fVal;
    }

    public double calcRs1(double Vin /*adc voltage*/, double Rl){
        //TGS2442 (CO), AS-MLC (CO), TGS2611 (CH4), AS-MLK(CH4), TGS2602 (VOC)
        double rez = (Vref/(Vin + Vref/2) - 1) * Rl;
        return rez;
    }
    
    public double calcRs2(double Vin /*adc voltage*/, double Rl){
        //MiCS2614 (O3)
        double rez = (Vref/(-Vin + Vref/2) - 1) * Rl;
        return rez;
    }

    public double calcRs3(double Vin /*adc voltage*/, double Rl){
        //MiCS6814 (NO2)
        double rez = Rl/(Vref/(Vin + Vref/2) - 1) ;
        return rez;
    }
    
    public double calcRs4(double Vin /*adc voltage*/, double Rl){
        //MiCS6814 (CO, NH3)
        double rez = Rl/(Vref/(-Vin + Vref/2) - 1);
        return rez;
    }
    
    public double calcRs(double Vin /*adc voltage*/, double Rl){
        switch (getSensorType()){
            case SensorDefs.TGS2442_CO:
            case SensorDefs.ASMLC_CO: 
            case SensorDefs.TGS2611_CH4:
            case SensorDefs.ASMLK_CH4:
            case SensorDefs.TGS2602_VOC:
                return calcRs1(Vin, Rl);
            case SensorDefs.MICS2614_O3:    
                return calcRs2(Vin, Rl);
            case SensorDefs.MICS6814_NO2:
                return calcRs3(Vin, Rl);
            case SensorDefs.MICS6814_CO:
            case SensorDefs.MICS6814_NH3:
                return calcRs4(Vin, Rl);
            default:
                throw new UnsupportedOperationException("Unknown sensor type " + Integer.toString(getSensorType())); 
        }
    }
    
    public abstract double calcKTemper(double temp); //temperature compensation coefficient
    
    public abstract double calcKHum(double humidity); //relative humidity compensation coefficient
    
    @Override
    public byte getMeasureUnit(){
        return SensorDefs.MU_CONCENTRATION_PPM;
    }
    
    @Override
    public double calcVal(Voltage volt, SensorParams.SensorParamVal paramVal, AirParam airParam){
        double temperature = airParam.temperature;
        double humidity = airParam.humidity;
        SensorParams.ResSensorParamVal p = (SensorParams.ResSensorParamVal)paramVal;
        
        double Rl = p.getRl() * 1000; //kOm -> Om
        double R0 = p.getR0() * 1000;//kOm -> Om calibration parameter 
                
        double voltCalibr = volt.getMilliVolt();
        double Rs = calcRs(voltCalibr, Rl);
        double y = Rs/(R0*calcKTemper(temperature)*calcKHum(humidity));
        fVal = A * Math.pow(y, B);
        //truncate if out of range
        if(checkRange){
            if (fVal<fromRange)
                fVal = fromRange;
            else if(fVal>toRange)
                fVal = toRange;
        }
        return fVal;
    }
}
