/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.AdcData;
import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

public class EC420SO2_SO2_Ech extends ElectrochemicalSensorType{

    public EC420SO2_SO2_Ech(){
        super();
        S_TIA = 100;
        S_NOM = 0.4;
        
        this.checkRange = false;
        this.fromRange = 0;
        this.toRange = 20;        
        
    }
    
        @Override
    public int getSensorType(){
        return SensorDefs.EC420SO2_SO2;
    }

    @Override
    public double calcCoefK(double x){
        double K = 3.05556E-09 * Math.pow(x, 6)
            -2.45513E-07 * Math.pow(x, 5)
            -9.41239E-06 * Math.pow(x, 4)
            + 0.000954575 * Math.pow(x, 3)
            -0.011117288 * Math.pow(x, 2)
            -0.110811199 * x 
            + 101.820979;
        return K;
    }
    
    @Override
    public double calcCoefC(double x){
        double C = -6.94444E-12 * Math.pow(x, 6)
                + 2.34038E-09 * Math.pow(x, 5)
                + 2.38248E-09 * Math.pow(x, 4)
                -1.34234E-06 * Math.pow(x, 3)
                + 6.51808E-05 * Math.pow(x, 2)
                + 0.001328662 * x
                -0.049851749;
        return C;
    }
    
//    @Override
//    public void calc(AdcData adc, double temperature, SensorParamLst.EchSensorParam p){
//        double S_TIA = 100;
//        double S_NOM = 0.4;
//        double x = temperature;
//        double K = 3.05556E-09 * Math.pow(x, 6)
//                -2.45513E-07 * Math.pow(x, 5)
//                -9.41239E-06 * Math.pow(x, 4)
//                + 0.000954575 * Math.pow(x, 3)
//                -0.011117288 * Math.pow(x, 2)
//                -0.110811199 * x 
//                + 101.820979;
//        double A = 1;
//        double B = 0;
//
//        if(p!=null){
//            A = p.getA();
//            B = p.getB();
//        }
//
//        double C = -6.94444E-12 * Math.pow(x, 6)
//                + 2.34038E-09 * Math.pow(x, 5)
//                + 2.38248E-09 * Math.pow(x, 4)
//                -1.34234E-06 * Math.pow(x, 3)
//                + 6.51808E-05 * Math.pow(x, 2)
//                + 0.001328662 * x
//                -0.049851749;
//        
//        val_ppm = adc.getMilliVolt()/(S_TIA * S_NOM * K * A) + B + C;
//    }
    
}
