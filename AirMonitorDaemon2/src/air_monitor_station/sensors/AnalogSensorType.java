/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.VoltageSeries;

public abstract class AnalogSensorType extends SensorType{
    
    public AnalogSensorType(){
        super();
    }
    
    @Override
    public SensorKind getSensorKind(){
        return SensorKind.ANALOG;
    }

}
