/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorDefs;
import air_monitor_station.SensorParams;
import air_monitor_station.Voltage;

/**
 *
 * @author SAP
 */
public abstract class ElectrochemicalSensorType extends AnalogSensorType {

    protected double val_ppm = 0;
    protected double S_TIA = 0;
    protected double S_NOM = 0;
    protected boolean checkRange = false;
    protected double fromRange = 0;
    protected double toRange = 0;

    public ElectrochemicalSensorType() {
        super();
    }

    public void setCheckRange(boolean val) {
        checkRange = val;
    }

    @Override
    public byte getMeasureUnit() {
        return SensorDefs.MU_CONCENTRATION_PPM;
    }

    @Override
    public double getVal() {
        return val_ppm;
    }

    public abstract double calcCoefK(double x);

    public abstract double calcCoefC(double x);

    @Override
    public double calcVal(Voltage volt, SensorParams.SensorParamVal paramVal, AirParam airParam) {
        double temperature = airParam.temperature;
        SensorParams.EchSensorParamVal p = (SensorParams.EchSensorParamVal)paramVal;

        //calibration coef
        double A = p.getA();
        double B = p.getB();

        double K = calcCoefK(temperature)/100.0;
        double C = calcCoefC(temperature);

        double voltAdjusted = volt.getMilliVolt();
        val_ppm = voltAdjusted / (S_TIA * S_NOM * K * A) + B + C;
        if (checkRange) {
            if (val_ppm < fromRange) {
                val_ppm = fromRange;
            } else if (val_ppm > toRange) {
                val_ppm = toRange;
            }
        }
        return val_ppm;
    }

    public double calcMictovolts(double ppm, double temperature) {

        double K = calcCoefK(temperature)/100;
        double C = calcCoefC(temperature);

        double millivolts = (ppm - C)*(S_TIA * S_NOM * K);
        
        return millivolts * 1000;
    }
    
    
    
}

