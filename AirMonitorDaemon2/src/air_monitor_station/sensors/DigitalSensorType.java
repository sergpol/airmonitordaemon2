/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.VoltageSeries;

/**
 *
 * @author SAP
 */


public abstract class DigitalSensorType extends SensorType{
    
    public DigitalSensorType(){
        super();
    }

    @Override
    public SensorKind getSensorKind(){
        return SensorKind.DIGITAL;
    }
    
    //public abstract double calcVal(int sensorUID, VoltageSeries vs);
    public abstract double unpackVal(int sensorData);
}
