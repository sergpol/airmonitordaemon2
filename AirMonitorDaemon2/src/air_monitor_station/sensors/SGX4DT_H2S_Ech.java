/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.AdcData;
import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;
import air_monitor_debug.DebugOutput;

public class SGX4DT_H2S_Ech extends ElectrochemicalSensorType{

    public SGX4DT_H2S_Ech(){
        super();
        S_TIA = 5.76;
        S_NOM = 0.775;
        this.checkRange = false;
        this.fromRange = 0;
        this.toRange = 200;        
    }

    @Override
    public int getSensorType(){
        return SensorDefs.SGX4DT_H2S;
    }
    
    @Override
    public double calcCoefK(double x){
        double K = -1.11111E-09 * Math.pow(x, 6)
                + 6.66667E-08 * Math.pow(x, 5)
                + 1.19658E-06 * Math.pow(x, 4)
                -0.000108974 * Math.pow(x, 3)
                -0.002706449 * Math.pow(x, 2)
                + 0.401034963 * x 
                + 93.97855493;
        return K;
    }
    
    @Override
    public double calcCoefC(double x){
        return 0;
    }
    
    
//    @Override
//    public void calc(AdcData adc, double temperature, SensorParamLst.EchSensorParam p){
//        double S_TIA = 5.76;
//        double S_NOM = 0.775;
//        double x = temperature;
//        double K = -1.11111E-09 * Math.pow(x, 6)
//                + 6.66667E-08 * Math.pow(x, 5)
//                + 1.19658E-06 * Math.pow(x, 4)
//                -0.000108974 * Math.pow(x, 3)
//                -0.002706449 * Math.pow(x, 2)
//                + 0.401034963 * x 
//                + 93.97855493;
//        double A = 1;
//        double B = 0;
//        if(p!=null){
//            A = p.getA();
//            B = p.getB();
//        }
//        
//        double C_0 = 0;
//        double voltage = adc.getMilliVolt();
//        //DebugOutput.log("*** SGX4DT_H2S rawdata = " + adc.getRawData() + ", voltage = " + Double.toString(voltage));
//        
//        val_ppm = voltage / (S_TIA * S_NOM * K * A) + B + C_0;
//    }
}
