/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

/**
 *
 * @author SAP
 */
public class TGS2611_CH4_Res extends ResistiveSensorType{
    public TGS2611_CH4_Res(){
        super();
        A = 5045.977768;
        B = -2.300250079;
        checkRange = false;
        fromRange = 500;
        toRange = 10000;        
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.TGS2611_CH4;
    }
        
    @Override
    public double calcKTemper(double x){ //temperature compensation coefficient
        double y = 4.16667E-08 * Math.pow(x, 4)
                -5.83333E-06 * Math.pow(x, 3)
                + 0.000495833 * Math.pow(x, 2)
                -0.030416667 * x 
                + 1.45;
        return y;
    }

    @Override
    public double calcKHum(double x){//humidity compensation coefficient
        double y = -4.33681E-19 * Math.pow(x, 3) 
                + 6.66667E-05 * Math.pow(x, 2)
                -0.015 * x 
                + 1.693333333;
        return y;
    }

}

