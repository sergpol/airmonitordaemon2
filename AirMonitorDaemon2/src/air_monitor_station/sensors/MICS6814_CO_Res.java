/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

/**
 *
 * @author SAP
 */
public class MICS6814_CO_Res extends ResistiveSensorType{
    public MICS6814_CO_Res(){
        super();
        A = 4.459382565;
        B = -1.189465608;
        checkRange = false;
        fromRange = 1;
        toRange = 1000;
    }

    @Override
    public int getSensorType(){
        return SensorDefs.MICS6814_CO;
    }
        
    @Override
    public double calcKTemper(double x){ //temperature compensation coefficient
        return 1.0;
    }

    @Override
    public double calcKHum(double x){//humidity compensation coefficient
        return 1.0;
    }

}


