/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

/**
 *
 * @author SAP
 */
public class MICS6814_NH3_Res extends ResistiveSensorType{
    public MICS6814_NH3_Res(){
        super();
        this.A = 0.663342241;
        this.B = -1.844880796;
        this.checkRange = false;
        this.fromRange = 1;
        this.toRange = 300; 
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.MICS6814_NH3;
    }
    
    @Override
    public double calcKTemper(double x){ //temperature compensation coefficient
        return 1.0;
    }

    @Override
    public double calcKHum(double x){//humidity compensation coefficient
        return 1.0;
    }

}



