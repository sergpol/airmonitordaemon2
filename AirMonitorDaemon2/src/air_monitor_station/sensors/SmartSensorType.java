/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorParams;
import air_monitor_station.Voltage;

/**
 *
 * @author SAP
 */
public class SmartSensorType implements ISensorType {

    protected double fValue = 0;
    protected byte fMeasureUnit;
    protected int fSensorType;
    protected SensorKind fSensorKind; 

    public SmartSensorType(SensorKind sensorKind, int sensorType, byte measureUnit){
        fMeasureUnit = measureUnit;
        fSensorType = sensorType;
        fSensorKind = sensorKind; 
    }
    
    public double getVal() {
        return fValue;
    }

    public byte getMeasureUnit() {
        return fMeasureUnit;
    }

    public int getSensorType() {
        return fSensorType;
    }

    public SensorKind getSensorKind() {
        return SensorKind.ANALOG;
    }

    public double calcVal(Voltage vs) {
        fValue = vs.getMicroVolt() / 1000.0;
        return fValue;
    }

}
