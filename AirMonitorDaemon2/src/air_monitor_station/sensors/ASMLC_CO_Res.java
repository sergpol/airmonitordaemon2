/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

/**
 *
 * @author SAP
 */
public class ASMLC_CO_Res extends ResistiveSensorType{
    
    public ASMLC_CO_Res(){
        super();
        A = 49.77633482;
        B = -1.728462523;
        checkRange = false;
        fromRange = 0.5;
        toRange = 500;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.ASMLC_CO;
    }

    
    @Override
    public double calcKTemper(double x){ //temperature compensation coefficient
        return 1.0;
    }

    @Override
    public double calcKHum(double x){//humidity compensation coefficient
        return 1.0;
    }

}
