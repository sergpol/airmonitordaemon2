package air_monitor_station.sensors;

import air_monitor_station.AdcData;
import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

public class ME3O3_O3_Ech extends ElectrochemicalSensorType{

    public ME3O3_O3_Ech(){
        super();
        S_TIA = 80.6;
        S_NOM = -0.6;
        this.checkRange = false;
        this.fromRange = 0;
        this.toRange = 20;        
    }

    @Override
    public int getSensorType(){
        return SensorDefs.ME3O3_O3;
    }
    
    @Override
    public double calcCoefK(double x){
        double K = -1.3125E-08 * Math.pow(x, 6)
                + 1.19247E-06 * Math.pow(x, 5)
                -1.76122E-05 * Math.pow(x, 4)
                -0.000962944 * Math.pow(x, 3)
                + 0.011194916 * Math.pow(x, 2)
                + 0.422044574 * x
                + 93.8098775;;
        return K;
    }
    
    @Override
    public double calcCoefC(double x){
        double C = 1.52778E-11 * Math.pow(x, 6)
                + 1.9391E-09 * Math.pow(x, 5)
                -1.72863E-07 * Math.pow(x, 4)
                + 6.74679E-07 * Math.pow(x, 3)
                -9.43377E-07 * Math.pow(x, 2)
                -0.003347115 * x
                + 0.071403846;
        return C;
    }
}
    

