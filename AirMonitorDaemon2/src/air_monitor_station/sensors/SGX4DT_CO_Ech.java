/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

//import static air_monitor_box.Sensors.NULL_TEMP;

import air_monitor_station.AdcData;
import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

//import static air_monitor_box.Sensors.fTemperature;

/**
 *
 * @author SAP
 */
public class SGX4DT_CO_Ech extends ElectrochemicalSensorType{

    public SGX4DT_CO_Ech(){
        super();
        S_TIA = 22;
        S_NOM = 0.08;
        
        this.checkRange = false;
        this.fromRange = 0;
        this.toRange = 500;        
        
    }

    @Override
    public int getSensorType(){
        return SensorDefs.SGX4DT_CO;
    }

    @Override
    public double calcCoefK(double x){
        double K = -6.2037E-09 * Math.pow(x, 6) 
                + 3.44658E-07 * Math.pow(x, 5)
                + 7.30057E-06* Math.pow(x, 4)
                -0.000704011 * Math.pow(x, 3)
                -0.004114407 * Math.pow(x, 2)
                + 1.691231935 * x
                + 72.34506623;
        return K;
    }
    
    @Override
    public double calcCoefC(double x){
        return 0;
    }
        
//    @Override
//    public void calc(AdcData adc, double temperature, SensorParamLst.EchSensorParam p){
//        double S_TIA = 22;
//        double S_NOM = 0.08;
//        double x = temperature;
//        double K = -6.2037E-09 * Math.pow(x, 6) 
//                + 3.44658E-07 * Math.pow(x, 5)
//                + 7.30057E-06* Math.pow(x, 4)
//                -0.000704011 * Math.pow(x, 3)
//                -0.004114407 * Math.pow(x, 2)
//                + 1.691231935 * x
//                + 72.34506623;
//        double A = 1;
//        double B = 0;
//        if(p!=null){
//            A = p.getA();
//            B = p.getB();
//        }
//        
//        double C = 0;
//
//        val_ppm = adc.getMilliVolt()/(S_TIA * S_NOM * K * A) + B + C;
//    }
    
}
