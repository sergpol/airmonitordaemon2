/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;
import air_monitor_station.SensorParams;
import air_monitor_station.Voltage;

/**
 *
 * @author SAP
 */
public class ASMLK_CH4_Res extends ResistiveSensorType{
    public ASMLK_CH4_Res(){
        super();
        A = 5184.303454;
        B = -2.573215753;
        checkRange = false;
        fromRange = 100.0;
        toRange = 40000.0;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.ASMLK_CH4;
    }
    
    @Override
    public double calcKTemper(double x){ //temperature compensation coefficient
        return 1.0;
    }

    @Override
    public double calcKHum(double x){//humidity compensation coefficient
        return 1.0;
    }
    
//    @Override
//    public double calcVal(Voltage volt, SensorParams.SensorParamVal paramVal, AirParam airParam){
//        return super.calcVal(volt, paramVal, airParam);
//    }

}


