/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorDefs;
import static air_monitor_station.SensorDefs.ASMLC_CO;
import static air_monitor_station.SensorDefs.ASMLK_CH4;
import static air_monitor_station.SensorDefs.EC420SO2_SO2;
import static air_monitor_station.SensorDefs.GP2Y1010AU0F_PM;
import static air_monitor_station.SensorDefs.ME3NO2_NO2;
import static air_monitor_station.SensorDefs.ME3O3_O3;
import static air_monitor_station.SensorDefs.MICS2614_O3;
import static air_monitor_station.SensorDefs.MICS6814_CO;
import static air_monitor_station.SensorDefs.MICS6814_NH3;
import static air_monitor_station.SensorDefs.MICS6814_NO2;
import static air_monitor_station.SensorDefs.MPL3115A2_ABS_PRESSURE;
import static air_monitor_station.SensorDefs.MPL3115A2_TEMPERATURE;
import static air_monitor_station.SensorDefs.SDS011_PM10;
import static air_monitor_station.SensorDefs.SDS011_PM25;
import static air_monitor_station.SensorDefs.SGX4DT_CO;
import static air_monitor_station.SensorDefs.SGX4DT_H2S;
import static air_monitor_station.SensorDefs.SGX7NH3_NH3;
import static air_monitor_station.SensorDefs.SHT21_HUMIDITY;
import static air_monitor_station.SensorDefs.SHT21_TEMPERATURE;
import static air_monitor_station.SensorDefs.TGS2442_CO;
import static air_monitor_station.SensorDefs.TGS2602_VOC;
import static air_monitor_station.SensorDefs.TGS2611_CH4;

/**
 *
 * @author SAP
 */
public class SensorTypesArray {

    private ISensorType types[] = {
        //электрохимические
        new SmartSensorType(SensorKind.ANALOG, SGX4DT_CO, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, SGX4DT_H2S, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, ME3O3_O3, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, ME3NO2_NO2, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, EC420SO2_SO2, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, SGX7NH3_NH3, SensorDefs.MU_CONCENTRATION_PPM),
        // Резистивные с нагревателями
        new SmartSensorType(SensorKind.ANALOG, MICS6814_NH3, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, MICS6814_CO, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, MICS6814_NO2, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, TGS2442_CO, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, ASMLC_CO, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, TGS2611_CH4, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, ASMLK_CH4, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, MICS2614_O3, SensorDefs.MU_CONCENTRATION_PPM),
        new SmartSensorType(SensorKind.ANALOG, TGS2602_VOC, SensorDefs.MU_CONCENTRATION_PPM),
        // Оптические
        new SmartSensorType(SensorKind.ANALOG, GP2Y1010AU0F_PM, SensorDefs.MU_DUST_WEIGHT_UG),
        new SmartSensorType(SensorKind.ANALOG, SDS011_PM25, SensorDefs.MU_DUST_WEIGHT_UG),
        new SmartSensorType(SensorKind.ANALOG, SDS011_PM10, SensorDefs.MU_DUST_WEIGHT_UG),
        // Цифровые сенсоры
        new SmartSensorType(SensorKind.ANALOG, MPL3115A2_ABS_PRESSURE, SensorDefs.MU_PRESSURE),
        new SmartSensorType(SensorKind.ANALOG, MPL3115A2_TEMPERATURE, SensorDefs.MU_TEMPERATURE),
        new SmartSensorType(SensorKind.ANALOG, SHT21_HUMIDITY, SensorDefs.MU_HUMIDITY),
        new SmartSensorType(SensorKind.ANALOG, SHT21_TEMPERATURE, SensorDefs.MU_TEMPERATURE)
    };

    public ISensorType find(int sensorType) {
        for (ISensorType t : types) {
            if (t.getSensorType() == sensorType) {
                return t;
            }
        }
        return null;
    }
}
