/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorDefs;
import air_monitor_station.SensorParams;
import air_monitor_station.Voltage;

/**
 *
 * @author SAP
 */
public class SHT21_Humidity_Dig extends DigitalSensorType{
    public double fVal;
    //public int humidityRawData;

    protected boolean checkRange = false;
    protected static final double fromHumidityRange = 0;
    protected static final double toHumidityRange = 0;

    public SHT21_Humidity_Dig() {
        super();
        fVal = 0;
    }

    @Override
    public double getVal(){
        return fVal;
    }
    
    @Override
    public byte getMeasureUnit(){
        return SensorDefs.MU_HUMIDITY;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.SHT21_HUMIDITY;
    }
    
    @Override
    public double unpackVal(int sensorData) {
        // Цифровой датчик влажности. Возвращает температуру в старших 16 битах и влажность в младших 16 битах
        // Выделяем 1 число
        int humidityRawData = sensorData & 0x0000FFFF; //unsigned short

        // Получаем значения в процентах
        double humidity = (double) (-6.0 + 125.0 / 65536 * (humidityRawData));
        return humidity;
    }

    @Override
    public double calcVal(Voltage volt, SensorParams.SensorParamVal paramVal, AirParam airParam) {
        // Цифровой датчик влажности. Возвращает температуру в старших 16 битах и влажность в младших 16 битах

        SensorParams.DigSensorParamVal p = (SensorParams.DigSensorParamVal)paramVal;
        
        fVal = volt.getMicroVolt();

        if (checkRange) {
            if (fVal < fromHumidityRange) {
                fVal = fromHumidityRange;
            } else if (fVal > toHumidityRange) {
                fVal = toHumidityRange;
            }
        }

        return fVal;
    }
    
}
