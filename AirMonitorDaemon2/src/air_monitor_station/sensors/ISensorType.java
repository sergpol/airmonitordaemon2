/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

/**
 *
 * @author SAP
 */
public interface ISensorType {
    double getVal();
    byte getMeasureUnit();
    int getSensorType();
    SensorKind getSensorKind();
}
