/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

/**
 *
 * @author SAP
 */
public class AirParam {
    public double temperature = 0;
    public double humidity = 0;
    
    public AirParam(){
        temperature = 0;
        humidity = 0;
    }
    
    public AirParam(double temperature, double humidity){
        this.temperature = temperature;
        this.humidity = humidity;
    }
}
