package air_monitor_station.sensors;

import air_monitor_station.SensorDefs;
import air_monitor_station.SensorParams;
import air_monitor_station.Voltage;

public class SHT21_Temperature_Dig extends DigitalSensorType{

    protected double fVal;
    protected boolean checkRange = false;
    protected static final double fromTempRange = 0;
    protected static final double totempRange = 0;

    public SHT21_Temperature_Dig() {
        super();
        fVal = 0;
    }

    @Override
    public double getVal(){
        return fVal;
    }
    
    @Override
    public byte getMeasureUnit(){
        return SensorDefs.MU_TEMPERATURE;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.SHT21_TEMPERATURE;
    }
    
    @Override
    public double unpackVal(int sensorData) {
        // Цифровой датчик влажности. Возвращает температуру в старших 16 битах и влажность в младших 16 битах

        // Выделяем 2 числа
        int tempRawData = sensorData >> 16; //signed short

        // Получаем значения в цельсиях и процентах
        double temperature = (double) (-46.85 + 175.72 / 65536 * (double) (tempRawData));
        return temperature;
    }

    @Override
    public double calcVal(Voltage volt, SensorParams.SensorParamVal paramVal, AirParam airParam) {
        // Цифровой датчик влажности. Возвращает температуру в старших 16 битах и влажность в младших 16 битах

        SensorParams.DigSensorParamVal p = (SensorParams.DigSensorParamVal)paramVal;
        
        fVal = volt.getMicroVolt();

        if (checkRange) {
            if (fVal < fromTempRange) {
                fVal = fromTempRange;
            } else if (fVal > totempRange) {
                fVal = totempRange;
            }
        }

        return fVal;
    }
}
