/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

/**
 *
 * @author SAP
 */
public class MICS2614_O3_Res extends ResistiveSensorType{
    public MICS2614_O3_Res(){
        super();
        A = 0.112452099;
        B = 1.039037367;
        checkRange = false;
        fromRange = 0.01;
        toRange = 1;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.MICS2614_O3;
    }

    
    @Override
    public double calcKTemper(double x){ //temperature compensation coefficient
        return 1.0;
    }

    @Override
    public double calcKHum(double x){//humidity compensation coefficient
        return 1.0;
    }

}
