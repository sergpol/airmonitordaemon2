/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

public class MICS6814_NO2_Res extends ResistiveSensorType{
    public MICS6814_NO2_Res(){
        super();
        A = 0.155055832;
        B = 0.991979901;
        checkRange = false;
        fromRange = 0.05;
        toRange = 10;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.MICS6814_NO2;
    }
    
    @Override
    public double calcKTemper(double x){ //temperature compensation coefficient
        return 1.0;
    }

    @Override
    public double calcKHum(double x){//humidity compensation coefficient
        return 1.0;
    }
}
