/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorDefs;
import air_monitor_station.SensorParams;
import air_monitor_station.Voltage;

/**
 *
 * @author SAP
 */
public class GP2Y1010AU0F_PM_Opt extends AnalogSensorType{
//    private double A = 1.0;
//    private double B = 0.0;
    protected boolean checkRange = false;
    protected double fromRange = 0;
    protected double toRange = 500;
    
    private static final double Kdiv = 3.33;
    private static final double Snom = 5.0;
    private static final double Dnom = -0.18;
    
    private double fVal = 0;
    
    public GP2Y1010AU0F_PM_Opt(){
        super();
    }
    
    @Override
    public double getVal(){
        return fVal;
    }
    
    @Override
    public byte getMeasureUnit(){
        return SensorDefs.MU_DUST_WEIGHT_UG;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.GP2Y1010AU0F_PM;
    }

    
    public void setCheckRange(boolean check){
        checkRange = check;
    }
    
    @Override
    public double calcVal(Voltage volt, SensorParams.SensorParamVal paramVal, AirParam airParam){
        SensorParams.OptSensorParamVal p = (SensorParams.OptSensorParamVal)paramVal;
        
        //calibration coef
        double A = p.getA();
        double B = p.getB();
        
        double voltCalibr = volt.getVolt();
        double y = ((voltCalibr * Kdiv)/Snom) * A + Dnom + B;
        y *= 1000;//convert to ug/m3
        if(checkRange){
            if (y<fromRange)
                y = fromRange;
            else if(y>toRange)
                y = toRange;
        }
        fVal = y;
        return y; 
    }
}
