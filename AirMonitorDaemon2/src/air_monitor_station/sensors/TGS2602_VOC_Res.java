/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

/**
 *
 * @author SAP
 */
public class TGS2602_VOC_Res extends ResistiveSensorType{
    public TGS2602_VOC_Res(){
        super();
        this.A = 0.148822502;
        this.B = -1.660197389;
        this.checkRange = false;
        this.fromRange = 1;
        this.toRange = 10;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.TGS2602_VOC;
    }
    
    @Override
    public double calcKTemper(double x){ //temperature compensation coefficient
        return 1.0;
    }

    @Override
    public double calcKHum(double x){//humidity compensation coefficient
        return 1.0;
    }

}