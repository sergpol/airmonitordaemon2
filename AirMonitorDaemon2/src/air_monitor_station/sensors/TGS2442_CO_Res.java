/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

/**
 *
 * @author SAP
 */
public class TGS2442_CO_Res extends ResistiveSensorType{
    public TGS2442_CO_Res(){
        super();
        A = 100.0601728;
        B = -0.878584709;
        checkRange = false;
        fromRange = 30;
        toRange = 300;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.TGS2442_CO;
    }
    
    @Override
    public double calcKTemper(double x){ //temperature compensation coefficient
        double y = 6.38889E-10 * Math.pow(x, 6) 
                - 1.05E-07 * Math.pow(x, 5) 
                + 6.38889E-06 * Math.pow(x, 4)
                -0.000173333 * Math.pow(x, 3) 
                + 0.002354722 * Math.pow(x, 2)
                -0.046616667 * x 
                + 1.649999998;
        return y;
    }

    @Override
    public double calcKHum(double x){//humidity compensation coefficient
        double y = 3.33333E-10 * Math.pow(x, 6)
                -1.03333E-07 * Math.pow(x, 5)
                +1.29167E-05 * Math.pow(x, 4)
                -0.00083 * Math.pow(x, 3)
                + 0.028805 * Math.pow(x, 2)
                -0.514966667 * x
                + 4.840000004;
        return y;
    }

}
