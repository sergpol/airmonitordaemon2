/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.SensorDefs;
import air_monitor_station.SensorParams;
import air_monitor_station.Voltage;

/**
 *
 * @author SAP
 */
public class MPL3115A2_Temperature_Dig extends DigitalSensorType{

    protected double fVal = 0;

    protected boolean checkRange = false;
    protected static final double fromTempRange = -40;
    protected static final double totempRange = 85;

    public MPL3115A2_Temperature_Dig() {
        super();
        fVal = 0;
    }

    @Override
    public double getVal(){
        return fVal;
    }
    
    @Override
    public byte getMeasureUnit(){
        return SensorDefs.MU_TEMPERATURE;
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.MPL3115A2_TEMPERATURE;
    }

    @Override
    public double unpackVal(int sensorData) {
    // Цифровой датчик давления. Возвращает температуру в С и абсолютное давление в Па
        // Размер данных от датчика - 32 бит
        // Формат представления данных и порядок бит:
        // Биты 0..19 (20бит) - давление в Па в беззнаковом формате 18.2
        // Биты 20..31 (12бит) - температура в С в формате 8.4 со знаком

//        //Выделяем 2 числа и приводим их к 32 битному и 16 битному целому
//        int pressureRawData = (sensorData & 0x000FFFFF) << 4;
//        int tempRawData = ((sensorData & 0xFFF00000) >> 20) << 4;
//        // Получаем значения в Па и С с плавающей точкой
//        double pressure = (double)(pressureRawData) / 64;
//        double temperature = (double)(tempRawData) / 256;
        //Выделяем 2 числа и приводим их к 32 битному и 16 битному целому
        int tempRawData = (sensorData & 0xFFF00000) >> 20;
        double temperature = (double) (tempRawData) / 16.0;
        return temperature;
    }

    
    @Override
    public double calcVal(Voltage volt, SensorParams.SensorParamVal paramVal, AirParam airParam) {
    // Цифровой датчик давления. Возвращает температуру в С и абсолютное давление в Па
        // Размер данных от датчика - 32 бит
        // Формат представления данных и порядок бит:
        // Биты 0..19 (20бит) - давление в Па в беззнаковом формате 18.2
        // Биты 20..31 (12бит) - температура в С в формате 8.4 со знаком

//        //Выделяем 2 числа и приводим их к 32 битному и 16 битному целому
//        int pressureRawData = (sensorData & 0x000FFFFF) << 4;
//        int tempRawData = ((sensorData & 0xFFF00000) >> 20) << 4;
//        // Получаем значения в Па и С с плавающей точкой
//        double pressure = (double)(pressureRawData) / 64;
//        double temperature = (double)(tempRawData) / 256;
        //Выделяем 2 числа и приводим их к 32 битному и 16 битному целому
        
        SensorParams.DigSensorParamVal p = (SensorParams.DigSensorParamVal)paramVal;
        
        fVal = volt.getMicroVolt();

        if (checkRange) {
            if (fVal < fromTempRange) {
                fVal = fromTempRange;
            } else if (fVal > totempRange) {
                fVal = totempRange;
            }
        }
        
        return fVal;
    }
    
}
