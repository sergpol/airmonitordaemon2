/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.AdcData;
import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

/**
 *
 * @author SAP
 */
public class SGX7NH3_NH3_Ech extends ElectrochemicalSensorType {

    public SGX7NH3_NH3_Ech() {
        super();
        S_TIA = 140;
        S_NOM = 0.12;
        this.checkRange = false;
        this.fromRange = 0;
        this.toRange = 50;
    }

    @Override
    public int getSensorType() {
        return SensorDefs.SGX7NH3_NH3;
    }

    @Override
    public double calcCoefK(double x) {
        double y = 7.10813E-09 * Math.pow(x, 6)
                + 1.04167E-08 * Math.pow(x, 5)
                - 2.87475E-05 * Math.pow(x, 4)
                - 4.79167E-05 * Math.pow(x, 3)
                + 0.022236706 * Math.pow(x, 2)
                + 0.445000003 * x
                + 86.69999991;

        return y;
    }

    @Override
    public double calcCoefC(double x) {
        return 0.0;
    }

//    @Override
//    public void calc(AdcData adc, double temperature, SensorParamLst.EchSensorParam p){
//            double S_TIA = 140;
//            double S_NOM = 0.12;
//            double x = temperature;
//            double K = 1;
//            double A = 1;
//            double B = 0;
//            if(p!=null){
//                A = p.getA();
//                B = p.getB();
//            }
//            
//            double C = 0;
//
//            val_ppm = adc.getMilliVolt()/(S_TIA * S_NOM * K * A) + B + C;
//        }
}
