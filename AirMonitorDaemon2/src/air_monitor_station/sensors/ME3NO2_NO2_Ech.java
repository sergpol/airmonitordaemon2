/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station.sensors;

import air_monitor_station.AdcData;
import air_monitor_station.SensorsArray;
import air_monitor_station.SensorDefs;

/**
 *
 * @author SAP
 */
public class ME3NO2_NO2_Ech extends ElectrochemicalSensorType{
    
    public ME3NO2_NO2_Ech(){
        super();
        S_TIA = 80.6;
        S_NOM = 0.6;
        this.checkRange = false;
        this.fromRange = 0;
        this.toRange = 20;        
    }
    
    @Override
    public int getSensorType(){
        return SensorDefs.ME3NO2_NO2;
    }
    
    
    @Override
    public double calcCoefK(double x){
        double K = -1.82292E-08 * Math.pow(x, 5)
                + 8.59375E-07 * Math.pow(x, 4)
                + 2.08333E-05 * Math.pow(x, 3)
                -0.00109375 * Math.pow(x, 2)
                + 0.544583333 * x
                + 89.29999999;
        return K;
    }
    
    @Override
    public double calcCoefC(double x){
        return 0;
    }

}
