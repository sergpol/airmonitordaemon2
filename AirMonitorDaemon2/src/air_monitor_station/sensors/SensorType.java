package air_monitor_station.sensors;

import air_monitor_station.SensorParams;
import air_monitor_station.Voltage;

enum SensorKind {DIGITAL, ANALOG};

public abstract class SensorType implements ISensorType{
    
    public SensorType(){
    }
    
    public abstract double getVal();
    public abstract byte getMeasureUnit();
    public abstract int getSensorType();
    public abstract SensorKind getSensorKind();
  
    public abstract double calcVal(Voltage vs, SensorParams.SensorParamVal paramVal, AirParam airParam);
    
}
