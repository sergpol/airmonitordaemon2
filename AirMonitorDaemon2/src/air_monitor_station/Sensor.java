/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import air_monitor_station.sensors.AirParam;
import air_monitor_station.sensors.SensorType;
import air_monitor_station.sensors.ISensorType;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author SAP
 */
public class Sensor extends SensorBase{
    protected SensorParams.SensorParamVal fParamVal;
    protected AirParam fAirParam;
    
    public Sensor(SensorsArrayBase meterings, ISensorType sensor, int sensorUID,
            SensorParams.SensorParamVal paramVal, AirParam airParam){
        super(meterings, sensor, sensorUID);
        fParamVal = paramVal;
        this.fAirParam = airParam;
    } 
    
    public double calcAvgVal() throws AirMonitorSensorError{
        if (fParamVal == null) {
            throw new AirMonitorSensorError("Cann't find sensor params UID="
                    + Integer.toString(fSensorUID) + " type=" + getSensorType());
        }

        Voltage volt = new Voltage(fVoltSeries.getAvgMicroVolt());
        double val = ((SensorType)fSensorType).calcVal(volt, fParamVal, fAirParam);
        return val;
    }
    
    public double calcVal() throws AirMonitorSensorError{
        if (fParamVal == null) {
            throw new AirMonitorSensorError("Cann't find sensor params UID="
                    + Integer.toString(fSensorUID) + " type=" + getSensorType());
        }

        Voltage volt = new Voltage(fLastMicroVolt);
        double val = ((SensorType)fSensorType).calcVal(volt, fParamVal, fAirParam);
        return val;
    }
    
}
