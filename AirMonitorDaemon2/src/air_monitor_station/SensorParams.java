package air_monitor_station;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public abstract class SensorParams {
    protected List<SensorParamVal> params = new ArrayList<>();
    int fBoxUID = 0;
    
    public SensorParams(int boxUID){
        fBoxUID = boxUID;
    }
    
    public int getBoxUID(){
        return fBoxUID;
    }
    
    public static class SensorParamVal {
        protected int fSensorUID = 0;
        protected int fSensorType = 0;
        
        public SensorParamVal(int sensorType){
            fSensorType = sensorType;
        }
        
        public int getSensorUid(){
            return fSensorUID;
        }
        
        public int getSensorType(){
            return fSensorType;
        }
        
        public void parseSensorFromJson(int sensorUID, JSONObject obj) throws AirMonitorSensorError{
            fSensorUID = sensorUID;
            String sensorName = (String)obj.get("sensor_name");
            fSensorType = SensorDefs.getSensorType(sensorName);
        }
    }

    public static class OptSensorParamVal extends SensorParamVal{
        private boolean fContainsA = false;
        private boolean fContainsB = false;
        private double A = 1;
        private double B = 0;
        
        public OptSensorParamVal(int sensorType){
            super(sensorType);
        }
        
        public double getA(){
            return A;
        }

        public double getB(){
            return B;
        }

        public boolean containsA(){
            return fContainsA;
        }
        
        public boolean containsB(){
            return fContainsB;
        } 
        
        @Override
        public void parseSensorFromJson(int sensorUID, JSONObject obj) throws AirMonitorSensorError{
            super.parseSensorFromJson(sensorUID, obj);
            if (obj.containsKey("A")){
                A = Double.parseDouble(obj.get("A").toString());
                fContainsA = true;
            }
            else{
                A = 1;
                fContainsA = false;
            }

            if (obj.containsKey("B")){
                B = Double.parseDouble(obj.get("B").toString());
                fContainsB = true;
            }
            else{
                B = 0;
                fContainsB = false;
            }
        }
    }
    
    public static class EchSensorParamVal extends SensorParamVal{
        private boolean fContainsA = false;
        private boolean fContainsB = false;
        private double A = 1;
        private double B = 0;
        
        public EchSensorParamVal(int sensorType){
            super(sensorType);
        }
        
        public double getA(){
            return A;
        }

        public double getB(){
            return B;
        }

        public boolean containsA(){
            return fContainsA;
        }
        
        public boolean containsB(){
            return fContainsB;
        } 
        
        @Override
        public void parseSensorFromJson(int sensorUID, JSONObject obj) throws AirMonitorSensorError{
            super.parseSensorFromJson(sensorUID, obj);
            if (obj.containsKey("A")){
                A = Double.parseDouble(obj.get("A").toString());
                fContainsA = true;
            }
            else{
                A = 1;
                fContainsA = false;
            }

            if (obj.containsKey("B")){
                B = Double.parseDouble(obj.get("B").toString());
                fContainsB = true;
            }
            else{
                B = 0;
                fContainsB = false;
            }
        }
    }
    
    public static class ResSensorParamVal extends SensorParamVal{
        private boolean fContainsR0 = false;
       
        private double R0 = 0;
        private double Rl = 0;

        public ResSensorParamVal(int sensorType){
            super(sensorType);
        }

        public double getRl(){
            return Rl;
        }
        
        public double getR0(){
            return R0;
        }

        public boolean containsR0(){
            return fContainsR0;
        }
        
        @Override
        public void parseSensorFromJson(int sensorUID, JSONObject obj) throws AirMonitorSensorError{
            super.parseSensorFromJson(sensorUID, obj);
            Rl = Double.parseDouble(obj.get("Rl").toString());
            if(obj.containsKey("R0")){
                R0 = Double.parseDouble(obj.get("R0").toString());
                fContainsR0 = true;
            }
            else{
                R0 = Rl;
                fContainsR0 = false;
            }
        }
    }
    
    public static class DigSensorParamVal extends SensorParamVal{

        public DigSensorParamVal(int sensorType){
            super(sensorType);
        }
        
        @Override
        public void parseSensorFromJson(int sensorUID, JSONObject obj) throws AirMonitorSensorError{
            super.parseSensorFromJson(sensorUID, obj);
        }
    }

    public SensorParamVal getSensorParam(int sensorUID, int sensorType){
        for(SensorParamVal p : params){
            if (p.fSensorUID == sensorUID && p.fSensorType == sensorType){
                return p;
            }
        }
        return null;
    }
    
    public ResSensorParamVal getResSensorParam(int sensorUID, int sensorType){
        SensorParamVal p = getSensorParam(sensorUID, sensorType);
        if (p==null)
            return null;
        else 
            return (ResSensorParamVal)p;
    }

    public OptSensorParamVal getOptSensorParam(int sensorUID, int sensorType){
        SensorParamVal p = getSensorParam(sensorUID, sensorType);
        if (p==null)
            return null;
        else 
            return (OptSensorParamVal)p;
    }

    public EchSensorParamVal getEchSensorParam(int sensorUID, int sensorType){
        SensorParamVal p = getSensorParam(sensorUID, sensorType);
        if (p==null)
            return null;
        else 
            return (EchSensorParamVal)p;
    }

    public DigSensorParamVal getDigSensorParam(int sensorUID, int sensorType){
        SensorParamVal p = getSensorParam(sensorUID, sensorType);
        if (p==null)
            return null;
        else 
            return (DigSensorParamVal)p;
    }
        

    public abstract void loadParams(List<Integer> uids);
    
}
