/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

/**
 *
 * @author SAP
 */
public class AirMonitorResponseError  extends AirMonitorException{
        public AirMonitorResponseError() {
            super();
    }

    //Constructor that accepts a message
    public AirMonitorResponseError(String message) {
        super(message);
    }

}
