/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;

import air_monitor_debug.DebugOutput;
import ambience_db.AmbienceDbManager;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SAP
 */
public class StationDescription extends StationDescriptionBase implements IStationDescription {

    //DataBaseWorker fDBW;
    private AmbienceDbManager fDbManager = null;

    public StationDescription(AmbienceDbManager dbManager) {
        super();
        fDbManager = dbManager;
    }

    public void parsePacket(TcpPacketIO packet) throws UnsupportedEncodingException {
        int curParamOffset = packet.getBoxParametersStart();
        int endParamsList = packet.getPacketLength() - 2; //crc
        byte paramCode;
        int paramLen;
        String paramVal;
        while (curParamOffset < endParamsList) {
            paramCode = packet.getParameterCode(curParamOffset);
            paramLen = packet.calcParamLength(curParamOffset);
            paramVal = packet.getParameter(curParamOffset, paramLen);
            parseParam(paramCode, paramVal);
            curParamOffset += paramLen + 2;
        }
    }

    private void parseParam(byte paramCode, String paramVal) {
        switch (paramCode) {
            case CODE_SERIAL_NUMBER:
                PARAM_SERIAL_NUMBER = paramVal;
                break;
            case CODE_HW_VERSION:
                PARAM_HW_VERSION = paramVal;
                break;
            case CODE_FW_VERSION:
                PARAM_FW_VERSION = paramVal;
                if(PARAM_SERIAL_NUMBER == "1.1")
                    existsLocation = true;
                break;
            case CODE_SYSTEM_TIME:
                PARAM_SYSTEM_TIME = paramVal;
                break;
            case CODE_AMBIENT_TEMPERATURE:
                PARAM_AMBIENT_TEMPERATURE = paramVal;
                break;
            case CODE_AMBIENT_HUMIDITY:
                PARAM_AMBIENT_HUMIDITY = paramVal;
                break;
            case CODE_AMBIENT_LIGHT:
                PARAM_AMBIENT_LIGHT = paramVal;
                break;
            case CODE_SOLAR_BAT_VOLTAGE:
                PARAM_SOLAR_BAT_VOLTAGE = paramVal;
                break;
            case CODE_LIFE_BAT_VOLTAGE:
                PARAM_LIFE_BAT_VOLTAGE = paramVal;
                break;
            case CODE_LIFE_BAT_VOLUME:
                PARAM_LIFE_BAT_VOLUME = paramVal;
                break;
            case CODE_EXT_POWER_VOLTAGE:
                PARAM_EXT_POWER_VOLTAGE = paramVal;
                break;
            case CODE_SD_CARD_VOLUME:
                PARAM_SD_CARD_VOLUME = paramVal;
                break;
            case CODE_SD_CARD_FREE_SPACE:
                PARAM_SD_CARD_FREE_SPACE = paramVal;
                break;
            case CODE_CONNECTED_SENSORS:
                parseSensorList(paramVal);
                break;
            case CODE_CONNECTION_PERIOD:
                PARAM_CONNECTION_PERIOD = paramVal;
                break;
            case CODE_MAIN_SERVER:
                PARAM_MAIN_SERVER = paramVal;
                break;
            case CODE_MAIN_LOGIN:
                PARAM_MAIN_LOGIN = paramVal;
                break;
            case CODE_MAIN_PASSWORD:
                PARAM_MAIN_PASSWORD = paramVal;
                break;
            case CODE_ALTERNATE_SERVER:
                PARAM_ALTERNATE_SERVER = paramVal;
                break;
            case CODE_ALTERNATE_LOGIN:
                PARAM_ALTERNATE_LOGIN = paramVal;
                break;
            case CODE_ALTERNATE_PASSWORD:
                PARAM_ALTERNATE_PASSWORD = paramVal;
                break;
            case CODE_GSM_WIFI_CONNECTION:
                PARAM_GSM_WIFI_CONNECTION = paramVal;
                break;
            case CODE_GSM_SIGNAL_QUALITY:
                PARAM_GSM_SIGNAL_QUALITY = paramVal;
                break;
            case CODE_SIM_CARD_NUMBER:
                PARAM_SIM_CARD_NUMBER = paramVal;
                break;
            case CODE_APN_FOR_GPRS:
                PARAM_APN_FOR_GPRS = paramVal;
                break;
            case CODE_GPRS_LOGIN:
                PARAM_GPRS_LOGIN = paramVal;
                break;
            case CODE_GPRS_PASSWORD:
                PARAM_GPRS_PASSWORD = paramVal;
                break;
            case CODE_WIFI_SIGNAL_QUALITY:
                PARAM_WIFI_SIGNAL_QUALITY = paramVal;
                break;
            case CODE_WIFI_NET_NAME:
                PARAM_WIFI_NET_NAME = paramVal;
                break;
            case CODE_WIFI_LOGIN:
                PARAM_WIFI_LOGIN = paramVal;
                break;
            case CODE_WIFI_PASSWORD:
                PARAM_WIFI_PASSWORD = paramVal;
                break;
            case CODE_LAT:
                PARAM_LATTITUDE = paramVal;
                existsLocation = true;
                break;
            case CODE_LON:
                PARAM_LONGITUDE = paramVal;
                existsLocation = true;
                break;
            default:
        }
    }

    public void parseSensorList(String paramVal) {
        PARAM_SENSOR_UIDS = new ArrayList<>();
        PARAM_SENSOR_TYPES = new ArrayList<>();
        String pairs[] = paramVal.split(",");
        for (String s : pairs) {
            String p[] = s.split(":");
            PARAM_SENSOR_UIDS.add(Integer.parseInt(p[0].trim()));
            PARAM_SENSOR_TYPES.add(Integer.parseInt(p[1].trim()));
        }
    }

    public void saveStationDesription() {
        fDbManager.saveStationDesription(this);
    }

}
