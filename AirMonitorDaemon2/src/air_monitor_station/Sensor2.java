/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import air_monitor_station.sensors.SmartSensorType;
import air_monitor_station.sensors.ISensorType;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author SAP
 */
public class Sensor2 extends SensorBase{
    
    public Sensor2(SensorsArrayBase meterings, ISensorType sensor, int sensorUID){
        super(meterings, sensor, sensorUID);
    } 
    
    public double calcAvgVal() throws AirMonitorSensorError{
        Voltage volt = new Voltage(fVoltSeries.getAvgMicroVolt());
        double val = ((SmartSensorType)fSensorType).calcVal(volt);
        return val;
    }
    
    public double calcVal() throws AirMonitorSensorError{
        Voltage volt = new Voltage(fLastMicroVolt);
        double val = ((SmartSensorType)fSensorType).calcVal(volt);
        return val;
    }
    
}
