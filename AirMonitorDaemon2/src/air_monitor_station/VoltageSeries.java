package air_monitor_station;

public class VoltageSeries {
    private double voltage;
    private long seriesCnt = 0;
    
    
    public VoltageSeries(){
        voltage = 0;
        seriesCnt = 0;
    }
    
    public void reset(){
        voltage = 0;
        seriesCnt = 0;
    }
    
    public double getAvgMicroVolt(){
        return voltage/seriesCnt;
    }

    public void addMicroVolt(double val){
        this.voltage += val;
        this.seriesCnt++;
    }
    
    public double getAvgMilliVolt(){
        return getAvgMicroVolt() / 1000.0; //convert from microvolt to millivolt
    }
    
    public double getAvgVolt(){
        return getAvgMicroVolt() / 1000000.0; //convert from microvolt to volt
    }
}
