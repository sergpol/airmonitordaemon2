/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author SAP
 */
public interface IStationDescription {

    public int getPARAM_SERIAL_NUMBER();

    public List<Integer> getPARAM_SENSOR_UIDS();

    public List<Integer> getPARAM_SENSOR_TYPES();

    public String getPARAM_HW_VERSION();

    public String getPARAM_FW_VERSION();

    public Timestamp getPARAM_SYSTEM_TIME();

    public float getPARAM_AMBIENT_TEMPERATURE();

    public float getPARAM_AMBIENT_HUMIDITY();

    public float getPARAM_AMBIENT_LIGHT();

    public float getPARAM_SOLAR_BAT_VOLTAGE();

    public float getPARAM_LIFE_BAT_VOLTAGE();

    public float getPARAM_LIFE_BAT_VOLUME();

    public float getPARAM_EXT_POWER_VOLTAGE();

    public int getPARAM_SD_CARD_VOLUME();

    public int getPARAM_SD_CARD_FREE_SPACE();

    public int getPARAM_CONNECTION_PERIOD();

    public String getPARAM_MAIN_SERVER();

    public String getPARAM_MAIN_LOGIN();

    public String getPARAM_MAIN_PASSWORD();

    public String getPARAM_ALTERNATE_SERVER();

    public String getPARAM_ALTERNATE_LOGIN();

    public String getPARAM_ALTERNATE_PASSWORD();

    public String getPARAM_GSM_WIFI_CONNECTION();

    public float getPARAM_GSM_SIGNAL_QUALITY();

    public String getPARAM_SIM_CARD_NUMBER();

    public String getPARAM_APN_FOR_GPRS();

    public String getPARAM_GPRS_LOGIN();

    public String getPARAM_GPRS_PASSWORD();

    public float getPARAM_WIFI_SIGNAL_QUALITY();

    public String getPARAM_WIFI_NET_NAME();

    public String getPARAM_WIFI_LOGIN();

    public String getPARAM_WIFI_PASSWORD();

    public double getPARAM_LONGITUDE();

    public double getPARAM_LATTITUDE();
    
    public boolean existsLocation();

    public String getLocation();
    
    public int getStationID();
    
    public void setLATTITUDE(String latitude);

    public void setLONGITUDE(String longitude);
}
