package air_monitor_station;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import air_monitor_station.StationInputParameter;

public class TcpPacketIO {

    public static final byte GET_CLIENT_INFO = 0x01;
    public static final byte GET_CURRENT_DATA = 0x03;

    public static final int REQUEST_CMD_OFFSET = 15;
    public static final int RESPONSE_CMD_OFFSET = 9;
    public static final int BOX_DESCRIPTION_OFFSET = 10;
    public static final int TIMESTAMP_OFFSET = 10;
    public static final int METERINGS_OFFSET = 14;

    private byte fBuffer[] = new byte[8192];
    private int fBufSize = 0;
    private byte fPacketData[] = new byte[8192];
    private short fPacketSize = 0;
    private Socket fSocket;
    OutputStream fOut;
    InputStream fIn;

    public TcpPacketIO(Socket socket) throws IOException {
        this.fSocket = socket;
        fOut = socket.getOutputStream();
        fIn = socket.getInputStream();
    }
    
    public int getTimestampOffset(){
        return TIMESTAMP_OFFSET;
    }
    
    public int getMeteringsOffset(){
        return METERINGS_OFFSET;
    }

    public void readPacket() throws IOException, IndexOutOfBoundsException,
            AirMonitorCrcError {
        readPacket(true);
    }

    public void readPacket(boolean checkCrcCode) throws IOException, IndexOutOfBoundsException,
            AirMonitorCrcError {
        fBufSize = 0;
        fPacketSize = -1;
        short packetIndex = 0; //index of the first free byte 
        boolean receive = false;
        do {
            fBufSize = fIn.read(fBuffer);
            //copy to packet
            for (int i = 0; i < fBufSize; i++, packetIndex++) {
                fPacketData[packetIndex] = fBuffer[i];
            }

            if (packetIndex >= 3) {
                if (fPacketSize == -1) {//extract length
                    ByteBuffer buf = ByteBuffer.wrap(fPacketData, 1, 2);
                    buf.order(ByteOrder.LITTLE_ENDIAN);
                    fPacketSize = buf.getShort();
                }
                if (packetIndex == fPacketSize) {
                    receive = true;
                } else if (packetIndex > fPacketSize) {
                    throw new IndexOutOfBoundsException(String.format("Wrong packet lenght fPacketSize = %i, packetIndex = %i", fPacketSize, packetIndex));
                }
            }
        } while (!receive);

        boolean crc = checkCRC();
        if (!crc) {
            throw new AirMonitorCrcError();
        }
    }

    public void clearPacket() {
        fPacketSize = 0;
        fBufSize = 0;
    }

    public void writePacket() throws IOException {
        fOut.flush();
        fOut.write(fPacketData, 0, getPacketLength());
    }

    public boolean checkCRC() {
        int len, i, p = 0;
        short CRC;

        if (fPacketSize == 0) {
            return false;
        }

        len = getPacketLength() - 2;

        try {
            CRC = (short) 0xFFFF;
            while (len != 0) {
                CRC = (short) (CRC ^ (fPacketData[p] << 8));
                for (i = 0; i < 8; i++) {
                    if ((CRC & 0x8000) != 0) {
                        CRC = (short) ((CRC << 1) ^ 0x1021);
                    } else {
                        CRC = (short) (CRC << 1);
                    }
                }
                p++;
                len--;
            }

            return ((byte) (CRC & 0xFF) == fPacketData[fPacketSize - 2])
                    && ((byte) ((CRC >> 8) & 0xFF) == fPacketData[fPacketSize - 1]);
        } catch (Exception e) {
            return false;
        }
    }

    public short getPacketLength() {
        return fPacketSize;
    }

    public byte[] getPacketData() {
        return fPacketData;
    }

    public long buildRequestGetDeviceInfo() {
        long packetId = generatePacketId();

        clearPacket();
        addSync();
        addLen((short) 0);
        addPacketId(packetId);
        addSuccessfullID();
        addCmd(GET_CLIENT_INFO);
        setLen(getPacketLength());
        addCRC();
        return packetId;
    }

    public long buildRequestGetCurrentData() {
        long packetId = generatePacketId();

        clearPacket();
        addSync();
        addLen((short) 0);
        addPacketId(packetId);
        addSuccessfullID();
        addCmd(GET_CURRENT_DATA);
        setLen(getPacketLength());
        addCRC();
        return packetId;
    }
       
    public void buildDeviceInfoResponse(long packetId, StationInputParameter theBoxParameter[]) {
        int size = theBoxParameter.length;
        clearPacket();
        addSync();
        addLen((short) 0);
        addPacketId(packetId);
        addCmd(GET_CLIENT_INFO);
        //addData(data, size);
        for (int i = 0; i < size; i++) {
            addBoxParameter(theBoxParameter[i].getCode(), theBoxParameter[i].getVal());
        }
        setLen(getPacketLength());
        addCRC();
    }

    public void buildGetCurrentDataResponse(long packetId, byte data[], int size) {
        clearPacket();
        addSync();
        addLen((short) 0);
        addPacketId(packetId);
        addCmd(GET_CURRENT_DATA);
        addSampleTime();
        addData(data, size);
        setLen(getPacketLength());
        addCRC();
    }

    public void addSampleTime() {
        long t = System.currentTimeMillis();
        ByteBuffer buf = ByteBuffer.wrap(fPacketData, fPacketSize, 8);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        buf.putLong(t);
        fPacketSize += 8;
    }

    public void setLen(short len) {
        ByteBuffer buf = ByteBuffer.wrap(fPacketData, 1, 2);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        buf.putShort(len);
    }

    public void addCmd(byte cmd) {
        fPacketData[fPacketSize++] = cmd;
    }

    public void addData(byte data[], int size) {
        for (int i = 0; i < size; i++) {
            fPacketData[fPacketSize++] = data[i];
        }
    }

    public void addSync() {
        fPacketData[0] = 0x55;
        fPacketSize = 1;
    }

    public void addLen(short len) {
        ByteBuffer buf = ByteBuffer.wrap(fPacketData, fPacketSize, 2);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        buf.putShort(len);
        fPacketSize += 2;
    }

    public static final int PACKET_ID_LEN = 6;
    public static final int PACKET_ID_OFFSET = 3;

    public long generatePacketId() {
        long id;
        //GMT
        id = System.currentTimeMillis();
        id = ((id / 1000) << 16) | ((id % 1000) & 0xFFFF);
        return id;
    }

    public void addPacketId(long id) {
        ByteBuffer buf = ByteBuffer.allocate(8);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        buf.putLong(id);
        for (int i = 0; i < 6; i++) {//copy lower 6 bytes
            fPacketData[fPacketSize++] = buf.get(i);
        }
//        ByteBuffer buf = ByteBuffer.wrap(fPacketData, fPacketSize, PACKET_ID_LEN);
//        buf.order(ByteOrder.LITTLE_ENDIAN);
//        buf.putLong(id);
//        fPacketSize += PACKET_ID_LEN;
    }

    public void addSuccessfullID() {
        for (int i = 0; i < 6; i++) {//fill with 0
            fPacketData[fPacketSize++] = 0;
        }

//        ByteBuffer buf = ByteBuffer.wrap(fPacketData, fPacketSize, PACKET_ID_LEN);
//        buf.order(ByteOrder.LITTLE_ENDIAN);
//        buf.putLong(0l);
//        fPacketSize += PACKET_ID_LEN;
    }

    public void addBoxParameter(byte paramCode, String paramVal) {
        fPacketData[fPacketSize++] = paramCode;
        //string.getBytes(Charset.forName("UTF-8"));
        for (int i = 0; i < paramVal.length(); i++) {
            fPacketData[fPacketSize++] = (byte) paramVal.charAt(i);
        }
        fPacketData[fPacketSize++] = 0x00;
    }

    public boolean addCRC() {
        short len, i;
        short CRC, p = 0;

        if (fPacketSize == 0) {
            return false;
        }

        len = getPacketLength();
        setLen((short) (len + 2));

        try {
            CRC = (short) 0xFFFF;

            while (len != 0) {
                CRC ^= ((short) (fPacketData[p] << 8));
                for (i = 0; i < 8; i++) {
                    if ((CRC & 0x8000) != 0) {
                        CRC = (short) ((CRC << 1) ^ 0x1021);
                    } else {
                        CRC = (short) (CRC << 1);
                    }
                }
                p++;
                len--;
            }

            fPacketSize += 2;
            ByteBuffer buf = ByteBuffer.wrap(fPacketData, fPacketSize - 2, 2);
            buf.order(ByteOrder.LITTLE_ENDIAN);
            buf.putShort(CRC);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean setCRC() {
        int len, i;
        short CRC, p = 0;

        if (fPacketSize == 0) {
            return false;
        }

        len = getPacketLength() - 2;

        try {
            CRC = (short) 0xFFFF;

            while (len != 0) {
                CRC ^= ((short) (fPacketData[p] << 8));
                for (i = 0; i < 8; i++) {
                    if ((CRC & 0x8000) != 0) {
                        CRC = (short) ((CRC << 1) ^ 0x1021);
                    } else {
                        CRC = (short) (CRC << 1);
                    }
                }
                p++;
                len--;
            }

            ByteBuffer buf = ByteBuffer.wrap(fPacketData, fPacketSize - 2, 2);
            buf.order(ByteOrder.LITTLE_ENDIAN);
            buf.putShort(CRC);
//            fPacketData[fPacketSize - 2] = (byte) (CRC & 0xFF);
//            fPacketData[fPacketSize - 1] = (byte) ((CRC >> 8) & 0xFF);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public long getPacketID() {
        ByteBuffer buf = ByteBuffer.wrap(fPacketData, PACKET_ID_OFFSET, PACKET_ID_LEN);
        return buf.getLong(0);
    }

    public byte getCommandId(int offset) {
        return fPacketData[offset];
    }

    public int getBoxParametersStart() {
        return BOX_DESCRIPTION_OFFSET;
    }

    public byte getParameterCode(int curParamOffset) {
        return fPacketData[curParamOffset];
    }

    public int calcParamLength(int paramOffset) {
        int size = 0;
        paramOffset++; //skip byte with parameter code
        int packetSize = getPacketLength();
        while (paramOffset + size < packetSize) {
            if (fPacketData[paramOffset + size] == 0) {
                break;
            }
            size++;
        }

        return size;
    }

    public String getParameter(int curParamOffset, int paramLength) throws UnsupportedEncodingException {
        // Specify the appropriate encoding as the last argument
        String param = new String(fPacketData, curParamOffset + 1, paramLength, "UTF-8");
        return param;
    }
}
