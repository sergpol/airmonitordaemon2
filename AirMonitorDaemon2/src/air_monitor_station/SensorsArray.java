package air_monitor_station;

import air_monitor_station.sensors.ASMLC_CO_Res;
import air_monitor_station.sensors.ASMLK_CH4_Res;
import air_monitor_station.sensors.AirParam;
//import air_monitor_station.sensors.DigitalSensor;
import air_monitor_station.sensors.EC420SO2_SO2_Ech;
import air_monitor_station.sensors.GP2Y1010AU0F_PM_Opt;
import air_monitor_station.sensors.ME3NO2_NO2_Ech;
import air_monitor_station.sensors.ME3O3_O3_Ech;
import air_monitor_station.sensors.MICS2614_O3_Res;
import air_monitor_station.sensors.MICS6814_CO_Res;
import air_monitor_station.sensors.MICS6814_NH3_Res;
import air_monitor_station.sensors.MICS6814_NO2_Res;
import air_monitor_station.sensors.MPL3115A2_AbsPressure_Dig;
import air_monitor_station.sensors.MPL3115A2_Temperature_Dig;
import air_monitor_station.sensors.SGX4DT_CO_Ech;
import air_monitor_station.sensors.SGX4DT_H2S_Ech;
import air_monitor_station.sensors.SGX7NH3_NH3_Ech;
import air_monitor_station.sensors.SHT21_Humidity_Dig;
import air_monitor_station.sensors.SHT21_Temperature_Dig;
import air_monitor_station.sensors.SensorType;
import air_monitor_station.sensors.TGS2442_CO_Res;
import air_monitor_station.sensors.TGS2602_VOC_Res;
import air_monitor_station.sensors.TGS2611_CH4_Res;
import air_monitor_debug.DebugOutput;
import ambience_db.AmbienceDbManager;
import ambience_db.CassandraDBManager;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import ambience_db.IDbSensorManager;

/**
 *
 * @author SAP
 */
public class SensorsArray extends SensorsArrayBase{
    //электрохимические
    private SGX4DT_CO_Ech fSGX4DT_CO = new SGX4DT_CO_Ech();
    private SGX4DT_H2S_Ech fSGX4DT_H2S = new SGX4DT_H2S_Ech();
    private ME3O3_O3_Ech fME3O3_O3 = new ME3O3_O3_Ech();
    private ME3NO2_NO2_Ech fME3NO2_NO2 = new ME3NO2_NO2_Ech();
    private EC420SO2_SO2_Ech fEC420SO2_SO2 = new EC420SO2_SO2_Ech();
    private SGX7NH3_NH3_Ech fSGX7NH3_NH3 = new SGX7NH3_NH3_Ech();
    //resistive sensors
    private TGS2442_CO_Res fTGS2442_CO = new TGS2442_CO_Res();
    private ASMLC_CO_Res fASMLC_CO = new ASMLC_CO_Res();
    private TGS2611_CH4_Res fTGS2611_CH4 = new TGS2611_CH4_Res();
    private ASMLK_CH4_Res fASMLK_CH4 = new ASMLK_CH4_Res();
    private TGS2602_VOC_Res fTGS2602_VOC = new TGS2602_VOC_Res();
    private MICS2614_O3_Res fMICS2614_O3 = new MICS2614_O3_Res();
    private MICS6814_NO2_Res fMICS6814_NO2 = new MICS6814_NO2_Res();
    private MICS6814_CO_Res fMICS6814_CO = new MICS6814_CO_Res();
    private MICS6814_NH3_Res fMICS6814_NH3 = new MICS6814_NH3_Res();
    //optical
    private GP2Y1010AU0F_PM_Opt fGP2Y1010AU0F_PM = new GP2Y1010AU0F_PM_Opt();
    //digital
    private MPL3115A2_Temperature_Dig fMPL3115A2_Temp = new MPL3115A2_Temperature_Dig();
    private MPL3115A2_AbsPressure_Dig fMPL3115A2_Press = new MPL3115A2_AbsPressure_Dig();
    private SHT21_Temperature_Dig fSHT21_Temp = new SHT21_Temperature_Dig();
    private SHT21_Humidity_Dig fSHT21_Humidity = new SHT21_Humidity_Dig();

    private SensorParams fSensorParam;
    private AirParam fAirParam = new AirParam();

    public SensorsArray(int stationUID,
            StationDescription stationDescription,
            //List<Integer> sensorUIDs,
            double temperature,
            double humidity,
            String parmPath) {

        super(stationUID, stationDescription);    
        setTemperature(temperature);
        setHumidity(humidity);
        fSensorParam = new SensorParamJsonFile(stationUID, parmPath);
        fSensorParam.loadParams(stationDescription.getPARAM_SENSOR_UIDS()); // sensorUIDs);
    }


    private SensorParams getSensorParamsLst() {
        return fSensorParam;
    }

    private void setTemperature(double temperature) {
        getAirParam().temperature = temperature;
    }

    private void setHumidity(double humidity) {
        getAirParam().humidity = humidity;
    }

    private AirParam getAirParam() {
        return fAirParam;
    }

    protected void parseSensorMetering(int sensorUID, int sensorType, int rawValue) {

        AdcData adc = new AdcData();
        switch (sensorType) {
            //электрохиимческие
            case SensorDefs.SGX4DT_CO:
                assert sensorType == fSGX4DT_CO.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fSGX4DT_CO, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.SGX4DT_H2S:
                assert fSGX4DT_H2S.getSensorType() == sensorType;
                adc.unpackAdcData(rawValue);
                addMetering(fSGX4DT_H2S, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.ME3O3_O3:
                assert sensorType == fME3O3_O3.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fME3O3_O3, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.ME3NO2_NO2:
                assert sensorType == fME3NO2_NO2.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fME3NO2_NO2, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.EC420SO2_SO2:
                assert sensorType == fEC420SO2_SO2.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fEC420SO2_SO2, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.SGX7NH3_NH3:
                assert sensorType == fSGX7NH3_NH3.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fSGX7NH3_NH3, sensorUID, adc.getMicroVolt());
                break;
            // Резистивные с нагревателями
            case SensorDefs.MICS6814_NH3:
                assert sensorType == fMICS6814_NH3.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fMICS6814_NH3, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.MICS6814_CO:
                assert sensorType == fMICS6814_CO.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fMICS6814_CO, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.MICS6814_NO2:
                assert sensorType == fMICS6814_NO2.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fMICS6814_NO2, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.TGS2442_CO:
                assert sensorType == fTGS2442_CO.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fTGS2442_CO, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.ASMLC_CO:
                assert sensorType == fASMLC_CO.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fASMLC_CO, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.TGS2611_CH4:
                assert sensorType == fTGS2611_CH4.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fTGS2611_CH4, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.ASMLK_CH4:
                assert sensorType == fASMLK_CH4.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fASMLK_CH4, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.MICS2614_O3:
                assert sensorType == fMICS2614_O3.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fMICS2614_O3, sensorUID, adc.getMicroVolt());
                break;
            case SensorDefs.TGS2602_VOC:
                assert sensorType == fTGS2602_VOC.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fTGS2602_VOC, sensorUID, adc.getMicroVolt());
                break;
            // Оптические
            case SensorDefs.GP2Y1010AU0F_PM:
                assert sensorType == fGP2Y1010AU0F_PM.getSensorType();
                adc.unpackAdcData(rawValue);
                addMetering(fGP2Y1010AU0F_PM, sensorUID, adc.getMicroVolt());
                break;
            // Пустышка чтобы забить аналоговый канал
            case SensorDefs.ANALOG_DUMMY:
                //adc.unpackAdcData(rawValue);
                //meterings.addMetering(MU_ADC_VOLTAGE, 0, 0, sensorUID, sensorType, rawValue);
                break;
            // Цифровые сенсоры
            case SensorDefs.MPL3115A2_ABS_PRESSURE: //pressure and temperature 
            case SensorDefs.MPL3115A2_TEMPERATURE: //pressure and temperature 
                assert sensorType == fMPL3115A2_Press.getSensorType();
                assert SensorDefs.MPL3115A2_TEMPERATURE == fMPL3115A2_Temp.getSensorType();
                double temp1 = fMPL3115A2_Temp.unpackVal(rawValue);
                addMetering(fMPL3115A2_Temp, sensorUID, temp1);
                double press = fMPL3115A2_Press.unpackVal(rawValue);
                addMetering(fMPL3115A2_Press, sensorUID, press);
                break;
            case SensorDefs.SHT21_HUMIDITY: //humidy and temperature
            case SensorDefs.SHT21_TEMPERATURE: //humidy and temperature
                assert sensorType == fSHT21_Humidity.getSensorType();
                assert SensorDefs.SHT21_TEMPERATURE == fSHT21_Temp.getSensorType();
                double temp2 = fSHT21_Temp.unpackVal(rawValue);
                addMetering(fSHT21_Temp, sensorUID, temp2);
                double humidity = fSHT21_Humidity.unpackVal(rawValue);
                addMetering(fSHT21_Humidity, sensorUID, humidity);
                break;
            default:
                //TODO: send message
                DebugOutput.log("ERROR: unknown sensor type: " + Integer.toString(sensorType));
                break;
        }
    }
    
    private void addMetering(SensorType sensorType, int sensorUID, double microVolt) {
        SensorBase sensor = findSensorMetering(sensorUID, sensorType.getSensorType());
        if (sensor == null) {
            SensorParams.SensorParamVal p = getSensorParamsLst().getSensorParam(
                    sensorUID, sensorType.getSensorType());
            sensor = new Sensor(this, sensorType, sensorUID, p, fAirParam);
            
            fSensorLst.add(sensor);
        }
        sensor.setMetering(microVolt);
    }

//    @Override
//    public void saveAvgMeterings() {
//        Timestamp currentsampletime = getSqlTimestamp();
//        int serialnumber = getStationID();
//
//        try {
//            updateTemperature();
//            updateHumidity();
//        } catch (AirMonitorSensorError e) {
//            DebugOutput.logStation(getStationID(), e.getMessage());
//            for (SensorBase m : fSensorLst) {
//                fDbManager.saveSensor(serialnumber,
//                        currentsampletime,
//                        m.getSensorUID(), //sensorUid,
//                        m.getSensorType(), // sensorType,
//                        SensorDefs.MU_NO_VALUE, //measure unit
//                        SensorDefs.ERR_TEMPERATURE_OR_HUMIDITY, //avgVal,
//                        0.0);
//            }
//            return;
//        }
//
//        super.saveAvgMeterings();
//    }

    @Override
    public void saveAvgMeterings(IDbSensorManager dbManager) {
        //Timestamp currentsampletime = getSqlTimestamp();
        Date currentsampletime = new Date();
        int serialnumber = getStationID();

        try {
            updateTemperature();
            updateHumidity();
        } catch (AirMonitorSensorError e) {
            DebugOutput.logStation(getStationID(), e.getMessage());
            for (SensorBase m : fSensorLst) {
                dbManager.saveSensor(serialnumber,
                        currentsampletime,
                        m.getSensorUID(), //sensorUid,
                        m.getSensorType(), // sensorType,
                        SensorDefs.MU_NO_VALUE, //measure unit
                        SensorDefs.ERR_TEMPERATURE_OR_HUMIDITY, //avgVal,
                        0.0);
            }
            return;
        }

        super.saveAvgMeterings(dbManager);
    }
    
//    private void updateSensorRow(
//            int serialnumber,
//            Timestamp currentsampletime,
//            int sensorUid,
//            int sensorType,
//            byte measureUnit,
//            double avgVal,
//            double avgMicroVolt) {
//
//        if (existsSensor(serialnumber, sensorUid, sensorType)) {
//            updateSensor(currentsampletime,
//                    measureUnit,
//                    avgVal,
//                    sensorUid,
//                    sensorType,
//                    serialnumber,
//                    avgMicroVolt);
//        } else {
//            insertSensor(currentsampletime,
//                    measureUnit,
//                    avgVal,
//                    sensorUid,
//                    sensorType,
//                    serialnumber,
//                    avgMicroVolt);
//        }
//    }

    @Override
    public void saveAvgMeteringsHistory(IDbSensorManager dbManager) {
        //Timestamp currentsampletime = getSqlTimestamp();
        Date currentsampletime = new Date();
        //byte measureUnit;
        //double avgVal;
        //double avgMicroVolt;
        //int sensorUid;
        //int sensorType;
        int serialnumber = getStationID();
        try {
            updateTemperature();
            updateHumidity();
        } catch (AirMonitorSensorError e) {
            DebugOutput.logStation(getStationID(), e.getMessage());
            for (SensorBase m : fSensorLst) {
                dbManager.saveSensorHistory(currentsampletime,
                        SensorDefs.MU_NO_VALUE, //measureUnit,
                        SensorDefs.ERR_TEMPERATURE_OR_HUMIDITY,//avgVal,
                        m.getSensorUID(),
                        m.getSensorType(),
                        serialnumber,
                        0.0);//avgMicroVolt);
            }
            return;
        }
        super.saveAvgMeteringsHistory(dbManager);
    }

    @Override
    public void saveMeteringsHistory(IDbSensorManager dbManager) {
        //save to Cassandra
        try {
            updateTemperature();
            updateHumidity();
        } catch (AirMonitorSensorError e) {
            DebugOutput.logStation(getStationID(), e.getMessage());
        }
        super.saveMeteringsHistory(dbManager);
    }

    private void updateTemperature() throws AirMonitorSensorError {
        SensorBase m = findSensorMeteringByType(SensorDefs.SHT21_TEMPERATURE);
        if (m == null) {
            throw new AirMonitorSensorError("Sensor "
                    + SensorDefs.getSensorName(SensorDefs.SHT21_TEMPERATURE)
                    + " cofig params were not found.");
        }

        double temperature = m.calcAvgVal();
        if (Double.isNaN(temperature)) {
            throw new AirMonitorSensorError("Formula for sensor "
                    + SensorDefs.getSensorName(SensorDefs.SHT21_TEMPERATURE)
                    + " return NaN value");
        }

        setTemperature(temperature);
        DebugOutput.logStation(fStationUID, "Base temperature updated");
    }

    private void updateHumidity() throws AirMonitorSensorError {
        SensorBase m = findSensorMeteringByType(SensorDefs.SHT21_HUMIDITY);
        if (m == null) {
            throw new AirMonitorSensorError("Sensor "
                    + SensorDefs.getSensorName(SensorDefs.SHT21_HUMIDITY)
                    + " cofig params were not found.");
        }

        double humidity = m.calcAvgVal();
        if (Double.isNaN(humidity)) {
            throw new AirMonitorSensorError("Formula for sensor "
                    + SensorDefs.getSensorName(SensorDefs.SHT21_HUMIDITY)
                    + " return NaN value");
        }

        setHumidity(humidity);
        DebugOutput.logStation(fStationUID, "Base humidity updated");

    }

}
