/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

/**
 *
 * @author SAP
 */
public class AirMonitorException extends Exception {

    //Parameterless Constructor
    public AirMonitorException() {
        super();
    }

    //Constructor that accepts a message
    public AirMonitorException(String message) {
        super(message);
    }
}
