package air_monitor_station;

public class Voltage {
    private double fMicrovolt;
        
    public Voltage(double microvolt){
        fMicrovolt = microvolt;
    }
    
    public double getMicroVolt(){
        return fMicrovolt;
    }

    public double getMilliVolt(){
        return fMicrovolt / 1000.0; //convert from microvolt to millivolt
    }
    
    public double getVolt(){
        return fMicrovolt / 1000000.0; //convert from microvolt to volt
    }
}
