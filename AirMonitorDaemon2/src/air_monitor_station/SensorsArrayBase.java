package air_monitor_station;

import air_monitor_debug.DebugOutput;
import ambience_db.AmbienceDbManager;
import ambience_db.CassandraDBManager;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import ambience_db.IDbSensorManager;

/**
 *
 * @author SAP
 */
public abstract class SensorsArrayBase {
    private static final int SENSOR_METERING_SIZE = 10; //int sensor UID; short sensor type; int sensor value
    protected List<SensorBase> fSensorLst = new ArrayList<SensorBase>();
    protected int fTimestamp; //in seconds ?
    protected int fStationUID;
    protected StationDescription fStationDescription;
    
    public SensorsArrayBase(int stationUID, StationDescription stationDescr) {
        fStationUID = stationUID;
        fStationDescription = stationDescr;
    }

    public static Timestamp calcNewSavePoint(int intervalSec, Timestamp ts, Timestamp savePoint) {
        final long period = intervalSec * 1000;
        long ms = ts.getTime();
        long s1 = ((ms / period) + 1) * period;
        savePoint.setTime(s1);
        return savePoint;
    }

    protected String getDateAsString() {
        long unixSeconds = fTimestamp;
        Date date = new Date(unixSeconds * 1000L); // *1000 to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT")); // give a timezone reference for formating
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public void parsePacket(TcpPacketIO packet) {
        //parse responce from box
        //timestamp
        int timestampOffset = packet.getTimestampOffset();
        ByteBuffer buf = ByteBuffer.wrap(packet.getPacketData(), timestampOffset, 4);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        fTimestamp = buf.getInt();

        //calc meterings count
        int packetLen = packet.getPacketLength();
        int meteringsOffset = packet.getMeteringsOffset();
        int meteringsSize = (packetLen - meteringsOffset - 2);
        ByteBuffer meteringBuf = ByteBuffer.wrap(packet.getPacketData(), meteringsOffset, meteringsSize);
        meteringBuf.order(ByteOrder.LITTLE_ENDIAN);
        assert(meteringsSize % SENSOR_METERING_SIZE == 0);
        for (int i = 0; i < meteringsSize; i += SENSOR_METERING_SIZE) {

            int uid = meteringBuf.getInt();
            int type = meteringBuf.getShort();
            if (type < 0) {
                type += 65536;  // convert to positive
            }
            int rawVal = meteringBuf.getInt();
            parseSensorMetering(uid, type, rawVal);
        }
    }

    protected abstract void parseSensorMetering(int sensorUID, int sensorType, int rawValue);
    
    public int getStationID() {
        return fStationUID;
    }

    public void setTimestamp(int timestamp) {
        fTimestamp = timestamp;
    }

    public int getTimestamp() {
        return fTimestamp;
    }


    public void resetMetering() {
        for (SensorBase temp : fSensorLst) {
            temp.reset();
        }
    }

    protected SensorBase findSensorMetering(int sensorUID, int sensorType) {
        for (SensorBase temp : fSensorLst) {
            if (temp.equal2(sensorUID, sensorType)) {
                return temp;
            }
        }
        return null;
    }

    protected SensorBase findSensorMeteringByType(int sensorType) {
        //returns first sensor of the specified type
        for (SensorBase temp : fSensorLst) {
            if (temp.equalByType(sensorType)) {
                return temp;
            }
        }
        return null;
    }

    public Timestamp getSqlTimestamp() {
        Timestamp timeStamp = new Timestamp(fTimestamp * 1000L); // *1000 is to convert seconds to milliseconds
        return timeStamp;
    }

    public void saveMeterings(IDbSensorManager dbManager) {
        Date currentsampletime = new Date();
        byte measureUnit;
        double val;
        double microVolt;
        int sensorUid;
        int sensorType;
        int serialnumber = getStationID();

        if(dbManager == null) return;        
        
        DebugOutput.log("Insert/Update db");

        for (SensorBase m : fSensorLst) {
            sensorUid = m.getSensorUID();
            sensorType = m.getSensorType();
            measureUnit = m.getMeasureUnit();

            try {
                microVolt = m.getMicroVolt();
                if (isBadDouble(microVolt)) {
                    microVolt = 0;
                    measureUnit = SensorDefs.MU_NO_VALUE;
                    val = SensorDefs.ERR_VOLTAGE;
                    String sensorName = SensorDefs.getSensorName(sensorType);
                    DebugOutput.logStation(getStationID(), "Sensor " + sensorName
                            + " UID = " + Integer.toString(sensorUid)
                            + " send NaN value"
                    );
                } else {
                    val = m.calcVal();
                    if (isBadDouble(val)) {
                        measureUnit = SensorDefs.MU_NO_VALUE;
                        val = SensorDefs.ERR_FOTMULA;
                        String sensorName = SensorDefs.getSensorName(sensorType);
                        DebugOutput.logStation(getStationID(), "Formula for sensor " + sensorName
                                + " UID = " + Integer.toString(sensorUid)
                                + " calc NaN value");
                    }
                }
                dbManager.saveSensor(serialnumber,
                        currentsampletime,
                        sensorUid,
                        sensorType,
                        measureUnit,
                        val,
                        microVolt);

            } catch (AirMonitorSensorError e) {
                DebugOutput.logStation(getStationID(), e.getMessage());
                dbManager.saveSensor(
                        serialnumber,
                        currentsampletime,
                        sensorUid,
                        sensorType,
                        SensorDefs.MU_NO_VALUE, //measureUnit,
                        SensorDefs.ERR_SENSOR, //avgVal,
                        0.0);//avgMicroVolt);

            }
        }
    }
    
    public void saveAvgMeterings(IDbSensorManager dbManager) {
        Date currentsampletime = new Date();
        byte measureUnit;
        double avgVal;
        double avgMicroVolt;
        int sensorUid;
        int sensorType;
        int serialnumber = getStationID();

        if(dbManager == null) return;        
        
        DebugOutput.log("Insert/Update db");

        for (SensorBase m : fSensorLst) {
            sensorUid = m.getSensorUID();
            sensorType = m.getSensorType();
            measureUnit = m.getMeasureUnit();

            try {
                avgMicroVolt = m.getAvgMicroVolt();
                if (isBadDouble(avgMicroVolt)) {
                    avgMicroVolt = 0;
                    measureUnit = SensorDefs.MU_NO_VALUE;
                    avgVal = SensorDefs.ERR_VOLTAGE;
                    String sensorName = SensorDefs.getSensorName(sensorType);
                    DebugOutput.logStation(getStationID(), "Sensor " + sensorName
                            + " UID = " + Integer.toString(sensorUid)
                            + " send NaN value"
                    );
                } else {
                    avgVal = m.calcAvgVal();
                    if (isBadDouble(avgVal)) {
                        measureUnit = SensorDefs.MU_NO_VALUE;
                        avgVal = SensorDefs.ERR_FOTMULA;
                        String sensorName = SensorDefs.getSensorName(sensorType);
                        DebugOutput.logStation(getStationID(), "Formula for sensor " + sensorName
                                + " UID = " + Integer.toString(sensorUid)
                                + " calc NaN value");
                    }
                }

                dbManager.saveSensor(
                        serialnumber,
                        currentsampletime,
                        sensorUid,
                        sensorType,
                        measureUnit,
                        avgVal,
                        avgMicroVolt);

            } catch (AirMonitorSensorError e) {
                DebugOutput.logStation(getStationID(), e.getMessage());
                dbManager.saveSensor(
                        serialnumber,
                        currentsampletime,
                        sensorUid,
                        sensorType,
                        SensorDefs.MU_NO_VALUE, //measureUnit,
                        SensorDefs.ERR_SENSOR, //avgVal,
                        0.0);//avgMicroVolt);

            }
        }
    }

    public void saveAirstationGPSHistory(IDbSensorManager dbManager, Date currentsampletime, int serialnumber){
        if(dbManager == null) return;
        byte measureUnit = SensorDefs.MU_GPS;
        double lattitude = fStationDescription.getPARAM_LATTITUDE(); //avgVal;
        double longitude = fStationDescription.getPARAM_LONGITUDE(); //avgMicroVolt;
        
        dbManager.saveSensorHistory(currentsampletime,
                measureUnit,
                lattitude,
                0,
                0,
                serialnumber,
                longitude);

        DebugOutput.log("GPS history was saved to db");
        
    }
    
    public void saveAvgMeteringsHistory(IDbSensorManager dbManager) {
        Date currentsampletime = getSqlTimestamp(); //new Date();
        byte measureUnit;
        double avgVal;
        double avgMicroVolt;
        int sensorUid;
        int sensorType;
        int serialnumber = getStationID();

        if(dbManager == null) return;
        
        DebugOutput.log("Insert/Update db");

        for (SensorBase m : fSensorLst) {
            sensorUid = m.getSensorUID();
            sensorType = m.getSensorType();
            measureUnit = m.getMeasureUnit();
            try {
                avgMicroVolt = m.getAvgMicroVolt();
                if (isBadDouble(avgMicroVolt)) {
                    measureUnit = SensorDefs.MU_NO_VALUE;
                    avgMicroVolt = 0;
                    avgVal = SensorDefs.ERR_VOLTAGE;
                    String sensorName = SensorDefs.getSensorName(sensorType);
                    DebugOutput.logStation(getStationID(), "Sensor " + sensorName
                            + " UID = " + Integer.toString(sensorUid)
                            + " send NaN value"
                    );
                } else {
                    avgVal = m.calcAvgVal();
                    if (isBadDouble(avgVal)) {
                        avgVal = SensorDefs.ERR_FOTMULA;
                        measureUnit = SensorDefs.MU_NO_VALUE;
                        String sensorName = SensorDefs.getSensorName(sensorType);
                        DebugOutput.logStation(getStationID(), "Formula for sensor " + sensorName
                                + " UID = " + Integer.toString(sensorUid)
                                + " calc NaN value"
                        );
                    }
                }

                dbManager.saveSensorHistory(currentsampletime,
                        measureUnit,
                        avgVal,
                        sensorUid,
                        sensorType,
                        serialnumber,
                        avgMicroVolt);
            } catch (AirMonitorSensorError e) {
                DebugOutput.logStation(getStationID(), e.getMessage());
                dbManager.saveSensorHistory(currentsampletime,
                        SensorDefs.MU_NO_VALUE, //measureUnit,
                        SensorDefs.ERR_SENSOR,//avgVal,
                        sensorUid,
                        sensorType,
                        serialnumber,
                        0.0);//avgMicroVolt);
            }
        }//for
        
        saveAirstationGPSHistory(dbManager, currentsampletime, serialnumber);
    }

    public void saveMeteringsHistory(IDbSensorManager dbManager) {
        Date currentsampletime = getSqlTimestamp(); //new Date();
        byte measureUnit;
        double sensorVal;
        double microVolt;
        int sensorUid;
        int sensorType;
        int serialnumber = getStationID();

        if(dbManager == null) return;
        
        for (SensorBase m : fSensorLst) {
            sensorUid = m.getSensorUID();
            sensorType = m.getSensorType();
            measureUnit = m.getMeasureUnit();
            try {
                microVolt = m.getMicroVolt();
                if (isBadDouble(microVolt)) {
                    measureUnit = SensorDefs.MU_NO_VALUE;
                    microVolt = 0;
                    sensorVal = SensorDefs.ERR_VOLTAGE;
                    String sensorName = SensorDefs.getSensorName(sensorType);
                    DebugOutput.logStation(getStationID(), "Sensor " + sensorName
                            + " UID = " + Integer.toString(sensorUid)
                            + " send NaN value"
                    );
                } else {
                    sensorVal = m.calcVal();
                    if (isBadDouble(sensorVal)) {
                        sensorVal = SensorDefs.ERR_FOTMULA;
                        measureUnit = SensorDefs.MU_NO_VALUE;
                        String sensorName = SensorDefs.getSensorName(sensorType);
                        DebugOutput.logStation(getStationID(), "Formula for sensor " + sensorName
                                + " UID = " + Integer.toString(sensorUid)
                                + " calc NaN value"
                        );
                    }
                }
                dbManager.saveSensorHistory(
                currentsampletime,
                        measureUnit,
                        sensorVal,
                        sensorUid,
                        sensorType,
                        serialnumber,
                        microVolt);
            } catch (AirMonitorSensorError e) {
                DebugOutput.logStation(getStationID(), e.getMessage());
                dbManager.saveSensorHistory(
                currentsampletime,
                        SensorDefs.MU_NO_VALUE, //measureUnit
                        SensorDefs.ERR_SENSOR, //sensorVal
                        sensorUid,
                        sensorType,
                        serialnumber,
                        0);//microVolt
            }
        } //for
        saveAirstationGPSHistory(dbManager, currentsampletime, serialnumber);
    }

    protected boolean isBadDouble(double val) {
        return Double.isInfinite(val) || Double.isNaN(val);
    }

//    public void deleteOutOfRangeHistory(int serialnumber, int limitOfDays, IDbSensorManager dbManager) {
//        dbManager.deleteOutOfRangeHistory(serialnumber, limitOfDays);
//        DebugOutput.logStation(serialnumber, "*** Out of time limit history was deleted ***");
//    }

    public void debugPrintDataPacket() {
        DebugOutput.logStation(getStationID(), "*** Parsed data packet ***");
        DebugOutput.logStation(getStationID(), "Timestamp: " + getDateAsString());
        for (SensorBase m : fSensorLst) {
            DebugOutput.logStation(getStationID(), "*** Sensor ***");
            m.logDebugInfo();
        }
        DebugOutput.logStation(getStationID(), "--------------------------");
    }

//    public void setOnlineMode(int boxUID, boolean online, IDbSensorManager dbManager ) {
//        fDbManager.sqlUpdate(SQL_BOX_ONLINE_UPDATE, online, boxUID);
//    }
//
//    public void clearSensorDatamodel(int stationUID, IDbSensorManager dbManager) {
//        fDbManager.sqlUpdate(SQL_CLEAR_SENSOR_MODEL, stationUID);
//    }
//
//    private final static String SQL_BOX_ONLINE_UPDATE = "UPDATE \"device_boxmodel\" "
//            + "SET \"Online\"=? "
//            + "WHERE \"SerialNumber\"=?;";
//
//    private final static String SQL_CLEAR_SENSOR_MODEL = "delete from device_sensordatamodel where serialnumber_id=?;";

}


