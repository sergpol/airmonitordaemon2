package air_monitor_station;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

//JSON: https://code.google.com/p/json-simple/
public class SensorParamJsonFile  extends SensorParamJson{
    
    String fPathToConfigCat;

    public SensorParamJsonFile(int boxUID, String pathToConfigCat) {
        super(boxUID);
        fPathToConfigCat = pathToConfigCat;
    }
    
    public String BuildFullPath(){
        String fullPath = new File(fPathToConfigCat, "sensors.json").toString();
        return fullPath;
    }
    
    @Override
    public JSONObject loadJsonSensors() throws IOException, ParseException{
        JSONParser parser = new JSONParser();
        String fileName = BuildFullPath();
        JSONObject sensors = (JSONObject)parser.parse(new FileReader(fileName));
        return sensors;
    }

}