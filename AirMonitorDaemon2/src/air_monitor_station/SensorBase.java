/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import air_monitor_debug.DebugOutput;
import air_monitor_station.sensors.SensorType;
import air_monitor_station.sensors.ISensorType;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author SAP
 */
public abstract class SensorBase {
    protected double fLastMicroVolt = 0;
    protected int fSensorUID = 0;
    protected VoltageSeries fVoltSeries;
    protected ISensorType fSensorType;
    //protected SensorParams.SensorParamVal fParamVal;
    protected SensorsArrayBase fSensorsAry;
    
    public SensorBase(SensorsArrayBase sensorsAry, ISensorType sensor, int sensorUID){
        fSensorsAry = sensorsAry;
        fSensorUID = sensorUID;
        fLastMicroVolt = 0;
        fVoltSeries = new VoltageSeries();
        fSensorType = sensor;
    } 
    
    public void reset(){
        fLastMicroVolt = 0;
        fVoltSeries.reset();
    }
    
    public void setMetering(double microVolt){
        fVoltSeries.addMicroVolt(microVolt);
        fLastMicroVolt = microVolt;
    }
        
    public void logDebugInfo(){
        int stationId = fSensorsAry.getStationID();
        String sensorName;
        try{
            sensorName = SensorDefs.getSensorName(fSensorType.getSensorType());
        }catch(AirMonitorSensorError e){
            sensorName = e.getMessage();
        }
        DebugOutput.logStation(stationId, "Sensor UID: " + Integer.toString(fSensorUID));
        DebugOutput.logStation(stationId, "Sensor name: " + sensorName);
        DebugOutput.logStation(stationId, "Sensor type: " + Integer.toString(fSensorType.getSensorType()));
        DebugOutput.logStation(stationId, "MeasureUnit: " + Byte.toString(fSensorType.getMeasureUnit()));
        DebugOutput.logStation(stationId, "Voltage in uV: " + Double.toString(fLastMicroVolt));
    }
    
    public boolean equal2(int sensorUID, int sensorType){
        return fSensorUID == sensorUID && fSensorType.getSensorType()== sensorType;
    }
    
    
    public boolean equalByType(int sensorType){
        return fSensorType.getSensorType() == sensorType;
    }

    public byte getMeasureUnit(){
        return fSensorType.getMeasureUnit();
    }
    
    public int getSensorUID(){
        return fSensorUID;
    }
    
    public int getSensorType(){
        return fSensorType.getSensorType();
    }
    
    public double getAvgMicroVolt(){
        return fVoltSeries.getAvgMicroVolt();
    }

    public double getMicroVolt(){
        return fLastMicroVolt;
    }
    
    public abstract double calcAvgVal() throws AirMonitorSensorError;
    
    public abstract double calcVal() throws AirMonitorSensorError;
    
}
