/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import air_monitor_debug.DebugOutput;
import ambience_db.AmbienceDbManager;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SAP
 */
public class StationDescriptionBase implements IStationDescription {

    //DataBaseWorker fDBW;
//    private AmbienceDbManager fDbManager = null;

    public static final byte CODE_SERIAL_NUMBER = 0x01; //byte	Air monitor serial number
    public static final byte CODE_HW_VERSION = 0x02; //String 5	Version hardware
    public static final byte CODE_FW_VERSION = 0x03; //String 5	Version firmware
    public static final byte CODE_SYSTEM_TIME = 0x04; //long	System time
    public static final byte CODE_AMBIENT_TEMPERATURE = 0x05; //float	Ambient temperature, C
    public static final byte CODE_AMBIENT_HUMIDITY = 0x06; //float	Ambient humidity, %
    public static final byte CODE_AMBIENT_LIGHT = 0x07; //float	Ambient light
    public static final byte CODE_SOLAR_BAT_VOLTAGE = 0x08; //float	Voltage solar battery, volts
    public static final byte CODE_LIFE_BAT_VOLTAGE = 0x09; //float	Voltage Li-Fe battery, volts
    public static final byte CODE_LIFE_BAT_VOLUME = 0x0A; //float	Volume Li-Fe battery, %;
    public static final byte CODE_EXT_POWER_VOLTAGE = 0x0B; //float	Voltage external power supply, volts;
    public static final byte CODE_SD_CARD_VOLUME = 0x0C; //byte	Volume SD card, bytees;
    public static final byte CODE_SD_CARD_FREE_SPACE = 0x0D; //	byte	Free space in SD card, bytees
    public static final byte CODE_CONNECTED_SENSORS = 0x0E; //	Array each element contain pair: sensorUID, sensorType
    /*Array is represented as string, which contains comma-separated pairs without blanks. Each pair consists of  
     sensor UID and sensor type values. Those values are separated via semicolon symbol.  
     Sensor_1UID:sensor_1Type, 
     …
     SensorN_UID:sensorN_Type 

     sensorUID is String
     and sensorType is byte

     Where sensorUID=70001..70011

     */
    public static final byte CODE_CONNECTION_PERIOD = 0x0F; //byte Period connection to server, second; (0- always hold connection);
    public static final byte CODE_MAIN_SERVER = 0x10; //String 32	DNS or IP address for main server connection;
    public static final byte CODE_MAIN_LOGIN = 0x11; //String 32	Login for main server connection;
    public static final byte CODE_MAIN_PASSWORD = 0x12; //String 32	Password for main server connection;
    public static final byte CODE_ALTERNATE_SERVER = 0x13; //String 32	DNS or IP address for alternative server connect;
    public static final byte CODE_ALTERNATE_LOGIN = 0x14; //String 32	Login for alternative server connection;
    public static final byte CODE_ALTERNATE_PASSWORD = 0x15; //String 32	Password for alternative server connection;
    public static final byte CODE_GSM_WIFI_CONNECTION = 0x16; //String 5	Chanel connection GSM or WiFi
    /*
     1 – only WiFi;
     2 – only GSM;
     2,1- both, GSM priority;
     2,2 – both, WiFi priority;
     */
    public static final byte CODE_GSM_SIGNAL_QUALITY = 0x17; //float	Quality signal GSM modem
    public static final byte CODE_SIM_CARD_NUMBER = 0x18; //String 15	Number sim-card
    public static final byte CODE_APN_FOR_GPRS = 0x19; //String 32	APN for GPRS network
    public static final byte CODE_GPRS_LOGIN = 0x1A; //String 32	Login for GPRS network
    public static final byte CODE_GPRS_PASSWORD = 0x1B; //String 32	Password for GPRS connection
    public static final byte CODE_WIFI_SIGNAL_QUALITY = 0x1C; //float	Quality signal WiFi modem
    public static final byte CODE_WIFI_NET_NAME = 0x1D; //String 32	Name WiFi network
    public static final byte CODE_WIFI_LOGIN = 0x1E; //String 32	Login for WiFi network
    public static final byte CODE_WIFI_PASSWORD = 0x1F; //String 32	Password for WiFi network
    public static final byte CODE_LAT = 0x20; //double; airstation location lattitude
    public static final byte CODE_LON = 0x21; //double; airstation location longitude

    // Box Parameters
    protected String PARAM_SERIAL_NUMBER; //int	Air monitor serial number
    protected String PARAM_HW_VERSION; //String 5	Version hardware
    protected String PARAM_FW_VERSION; //String 5	Version firmware
    protected String PARAM_SYSTEM_TIME; //long	System time
    protected String PARAM_AMBIENT_TEMPERATURE; //float	Ambient temperature, C
    protected String PARAM_AMBIENT_HUMIDITY; //float	Ambient humidity, %
    protected String PARAM_AMBIENT_LIGHT; //float	Ambient light
    protected String PARAM_SOLAR_BAT_VOLTAGE; //float	Voltage solar battery, volts
    protected String PARAM_LIFE_BAT_VOLTAGE; //float	Voltage Li-Fe battery, volts
    protected String PARAM_LIFE_BAT_VOLUME; //float	Volume Li-Fe battery, %;
    protected String PARAM_EXT_POWER_VOLTAGE; //float	Voltage external power supply, volts;
    protected String PARAM_SD_CARD_VOLUME; //int	Volume SD card, bytes;
    protected String PARAM_SD_CARD_FREE_SPACE; //	int	Free space in SD card, bytes
    //protected String PARAM_CONNECTED_SENSORS; //	Array of 11 pairs sensorUID sensorType
    protected List<Integer> PARAM_SENSOR_UIDS; //	Array of sensor UIDs
    protected List<Integer> PARAM_SENSOR_TYPES; //	Array of sensor types

    protected String PARAM_CONNECTION_PERIOD; //int Period connection to server, second; (0- always hold connection);
    protected String PARAM_MAIN_SERVER; //String 32	DNS or IP address for main server connection;
    protected String PARAM_MAIN_LOGIN; //String 32	Login for main server connection;
    protected String PARAM_MAIN_PASSWORD; //String 32	Password for main server connection;
    protected String PARAM_ALTERNATE_SERVER; //String 32	DNS or IP address for alternative server connect;
    protected String PARAM_ALTERNATE_LOGIN; //String 32	Login for alternative server connection;
    protected String PARAM_ALTERNATE_PASSWORD; //String 32	Password for alternative server connection;
    protected String PARAM_GSM_WIFI_CONNECTION; //String 5	Chanel connection GSM or WiFi
    protected String PARAM_GSM_SIGNAL_QUALITY; //float	Quality signal GSM modem
    protected String PARAM_SIM_CARD_NUMBER; //String 15	Number sim-card
    protected String PARAM_APN_FOR_GPRS; //String 32	APN for GPRS network
    protected String PARAM_GPRS_LOGIN; //String 32	Login for GPRS network
    protected String PARAM_GPRS_PASSWORD; //String 32	Password for GPRS connection
    protected String PARAM_WIFI_SIGNAL_QUALITY; //float	Quality signal WiFi modem
    protected String PARAM_WIFI_NET_NAME; //String 32	Name WiFi network
    protected String PARAM_WIFI_LOGIN; //String 32	Login for WiFi network
    protected String PARAM_WIFI_PASSWORD; //String 32	Password for WiFi network
    
    protected boolean existsLocation = false;
    protected String PARAM_LATTITUDE; //double
    protected String PARAM_LONGITUDE; //double

    public StationDescriptionBase() {
        
    }

    public int getPARAM_SERIAL_NUMBER() {
        //int	Air monitor serial number
        return Integer.parseInt(PARAM_SERIAL_NUMBER);
    }

    public List<Integer> getPARAM_SENSOR_UIDS() {
        return PARAM_SENSOR_UIDS;
    }

    public List<Integer> getPARAM_SENSOR_TYPES() {
        return PARAM_SENSOR_TYPES;
    }

    public String getPARAM_HW_VERSION() {
        //String 5	Version hardware
        if (PARAM_HW_VERSION == null) {
            return "";
        }
        return PARAM_HW_VERSION;
    }

    public String getPARAM_FW_VERSION() {
        //String 5	Version firmware
        if (PARAM_FW_VERSION == null) {
            return "";
        }
        return PARAM_FW_VERSION;
    }

    public Timestamp getPARAM_SYSTEM_TIME() {
        Timestamp timeStamp;
        if (PARAM_SYSTEM_TIME == null || PARAM_SYSTEM_TIME.length() == 0) {
            timeStamp = new Timestamp(0);
        } else {
            long dt = Long.parseLong(PARAM_SYSTEM_TIME) * 1000L; //// *1000 is to convert seconds to milliseconds
            timeStamp = new Timestamp(dt);
        }

        return timeStamp;
    }

    public float getPARAM_AMBIENT_TEMPERATURE() {
        //float	Ambient temperature, C
        if (PARAM_AMBIENT_TEMPERATURE == null || PARAM_AMBIENT_TEMPERATURE.length() == 0) {
            return 0;
        }
        return Float.parseFloat(PARAM_AMBIENT_TEMPERATURE);
    }

    public float getPARAM_AMBIENT_HUMIDITY() {
        //float	Ambient humidity, %
        if (PARAM_AMBIENT_HUMIDITY == null || PARAM_AMBIENT_HUMIDITY.length() == 0) {
            return 0;
        }

        return Float.parseFloat(PARAM_AMBIENT_HUMIDITY);
    }

    public float getPARAM_AMBIENT_LIGHT() {
        if (PARAM_AMBIENT_LIGHT == null || PARAM_AMBIENT_LIGHT.length() == 0) {
            return 0;
        }
        return Float.parseFloat(PARAM_AMBIENT_LIGHT);
    }

    public float getPARAM_SOLAR_BAT_VOLTAGE() {
        if (PARAM_SOLAR_BAT_VOLTAGE == null || PARAM_SOLAR_BAT_VOLTAGE.length() == 0) {
            return 0;
        }

        return Float.parseFloat(PARAM_SOLAR_BAT_VOLTAGE);
    }

    public float getPARAM_LIFE_BAT_VOLTAGE() {
        if (PARAM_LIFE_BAT_VOLTAGE == null || PARAM_LIFE_BAT_VOLTAGE.length() == 0) {
            return 0;
        }

        return Float.parseFloat(PARAM_LIFE_BAT_VOLTAGE);
    }

    public float getPARAM_LIFE_BAT_VOLUME() {
        if (PARAM_LIFE_BAT_VOLUME == null || PARAM_LIFE_BAT_VOLUME.length() == 0) {
            return 0;
        }

        return Float.parseFloat(PARAM_LIFE_BAT_VOLUME);
    }

    public float getPARAM_EXT_POWER_VOLTAGE() {
        if (PARAM_EXT_POWER_VOLTAGE == null || PARAM_EXT_POWER_VOLTAGE.length() == 0) {
            return 0;
        }

        return Float.parseFloat(PARAM_EXT_POWER_VOLTAGE);
    }

    public int getPARAM_SD_CARD_VOLUME() {
        if (PARAM_SD_CARD_VOLUME == null || PARAM_SD_CARD_VOLUME.length() == 0) {
            return 0;
        }

        return Integer.parseInt(PARAM_SD_CARD_VOLUME);
    }

    public int getPARAM_SD_CARD_FREE_SPACE() {
        if (PARAM_SD_CARD_FREE_SPACE == null || PARAM_SD_CARD_FREE_SPACE.length() == 0) {
            return 0;
        }

        return Integer.parseInt(PARAM_SD_CARD_FREE_SPACE);
    }

    public int getPARAM_CONNECTION_PERIOD() {
        if (PARAM_CONNECTION_PERIOD == null || PARAM_CONNECTION_PERIOD.length() == 0) {
            return 0;
        }

        return Integer.parseInt(PARAM_CONNECTION_PERIOD);
    }

    public String getPARAM_MAIN_SERVER() {
        //String 32	DNS or IP address for main server connection;
        if (PARAM_MAIN_SERVER == null || PARAM_MAIN_SERVER.length() > 32) {
            return "";
        }

        return PARAM_MAIN_SERVER;
    }

    public String getPARAM_MAIN_LOGIN() {
        //String 32	Login for main server connection;
        if (PARAM_MAIN_LOGIN == null || PARAM_MAIN_LOGIN.length() > 32) {
            return "";
        }
        return PARAM_MAIN_LOGIN;
    }

    public String getPARAM_MAIN_PASSWORD() {
        //String 32	Password for main server connection;
        if (PARAM_MAIN_PASSWORD == null || PARAM_MAIN_PASSWORD.length() > 32) {
            return "";
        }
        return PARAM_MAIN_PASSWORD;
    }

    public String getPARAM_ALTERNATE_SERVER() {
        //String 32	DNS or IP address for alternative server connect;
        if (PARAM_ALTERNATE_SERVER == null || PARAM_ALTERNATE_SERVER.length() > 32) {
            return "";
        }

        return PARAM_ALTERNATE_SERVER;
    }

    public String getPARAM_ALTERNATE_LOGIN() {
        //String 32	Login for alternative server connection;
        if (PARAM_ALTERNATE_LOGIN == null || PARAM_ALTERNATE_LOGIN.length() > 32) {
            return "";
        }

        return PARAM_ALTERNATE_LOGIN;
    }

    public String getPARAM_ALTERNATE_PASSWORD() {
        //String 32	Password for alternative server connection;
        if (PARAM_ALTERNATE_PASSWORD == null || PARAM_ALTERNATE_PASSWORD.length() > 32) {
            return "";
        }

        return PARAM_ALTERNATE_PASSWORD;
    }

    public String getPARAM_GSM_WIFI_CONNECTION() {
        //String 5	Chanel connection GSM or WiFi
        if (PARAM_GSM_WIFI_CONNECTION == null || PARAM_GSM_WIFI_CONNECTION.length() > 5) {
            return "";
        }

        return PARAM_GSM_WIFI_CONNECTION;
    }

    public float getPARAM_GSM_SIGNAL_QUALITY() {
        //float	Quality signal GSM modem
        if (PARAM_GSM_SIGNAL_QUALITY == null || PARAM_GSM_SIGNAL_QUALITY.length() == 0) {
            return 0;
        }

        return Float.parseFloat(PARAM_GSM_SIGNAL_QUALITY);
    }

    public String getPARAM_SIM_CARD_NUMBER() {
        //String 15	Number sim-card
        if (PARAM_SIM_CARD_NUMBER == null || PARAM_SIM_CARD_NUMBER.length() > 15) {
            return "";
        }

        return PARAM_SIM_CARD_NUMBER;
    }

    public String getPARAM_APN_FOR_GPRS() {
        //String 32	APN for GPRS network
        if (PARAM_APN_FOR_GPRS == null || PARAM_APN_FOR_GPRS.length() > 32) {
            return "";
        }

        return PARAM_APN_FOR_GPRS;
    }

    public String getPARAM_GPRS_LOGIN() {
        //String 32	Login for GPRS network
        if (PARAM_GPRS_LOGIN == null || PARAM_GPRS_LOGIN.length() > 32) {
            return "";
        }

        return PARAM_GPRS_LOGIN;
    }

    public String getPARAM_GPRS_PASSWORD() {
        //String 32	Password for GPRS connection
        if (PARAM_GPRS_PASSWORD == null || PARAM_GPRS_PASSWORD.length() > 32) {
            return "";
        }

        return PARAM_GPRS_PASSWORD;
    }

    public float getPARAM_WIFI_SIGNAL_QUALITY() {
        //float	Quality signal WiFi modem
        if (PARAM_WIFI_SIGNAL_QUALITY == null || PARAM_WIFI_SIGNAL_QUALITY.length() == 0) {
            return 0;
        }

        return Float.parseFloat(PARAM_WIFI_SIGNAL_QUALITY);
    }

    public String getPARAM_WIFI_NET_NAME() {
        //String 32	Name WiFi network
        if (PARAM_WIFI_NET_NAME == null || PARAM_WIFI_NET_NAME.length() > 32) {
            return "";
        }

        return PARAM_WIFI_NET_NAME;
    }

    public String getPARAM_WIFI_LOGIN() {
        //String 32	Login for WiFi network
        if (PARAM_WIFI_LOGIN == null || PARAM_WIFI_LOGIN.length() > 32) {
            return "";
        }

        return PARAM_WIFI_LOGIN;
    }

    public String getPARAM_WIFI_PASSWORD() {
        //String 32	Password for WiFi network
        if (PARAM_WIFI_PASSWORD == null || PARAM_WIFI_PASSWORD.length() > 32) {
            return "";
        }

        return PARAM_WIFI_PASSWORD;
    }

    public int getStationID() {
        return Integer.parseInt(PARAM_SERIAL_NUMBER);
    }

    public double getPARAM_LONGITUDE() {
        //double	
        if (PARAM_LONGITUDE == null || PARAM_LONGITUDE.length() == 0) {
            return 0;
        }

        return Double.parseDouble(PARAM_LONGITUDE);
    }

    public double getPARAM_LATTITUDE() {
        //double	
        if (PARAM_LATTITUDE == null || PARAM_LATTITUDE.length() == 0) {
            return 0;
        }

        return Double.parseDouble(PARAM_LATTITUDE);
    }

    public void setLATTITUDE(String latitude){
        PARAM_LATTITUDE = latitude;
        existsLocation = true;
    }

    public void setLONGITUDE(String longitude){
        PARAM_LONGITUDE = longitude;
        existsLocation = true;
    }
    
    public boolean existsLocation() {
        return existsLocation;
//        if ((PARAM_LATTITUDE == null || PARAM_LATTITUDE.length() == 0)
//                && PARAM_LONGITUDE == null || PARAM_LONGITUDE.length() == 0) {
//            return false;
//        }
//        return true;
    }
    
    public String getLocation(){
        String location;
        if(existsLocation()){
            location = getPARAM_LATTITUDE() + "," + getPARAM_LONGITUDE();
        }
        else
            location = "";
       return location;     
    }
    
    public void parseSensorList(String paramVal) {
        PARAM_SENSOR_UIDS = new ArrayList<>();
        PARAM_SENSOR_TYPES = new ArrayList<>();
        String pairs[] = paramVal.split(",");
        for (String s : pairs) {
            String p[] = s.split(":");
            PARAM_SENSOR_UIDS.add(Integer.parseInt(p[0].trim()));
            PARAM_SENSOR_TYPES.add(Integer.parseInt(p[1].trim()));
        }
    }

    public void logDebugInfo() {

        DebugOutput.log("*** Parsed Packet GET_DEVICE_INFO ***");
        DebugOutput.log("PARAM_SERIAL_NUMBER: " + PARAM_SERIAL_NUMBER); //Air monitor serial number
        DebugOutput.log("PARAM_HW_VERSION: " + PARAM_HW_VERSION); //String 5	Version hardware
        DebugOutput.log("PARAM_FW_VERSION: " + PARAM_FW_VERSION); //String 5	Version firmware
        DebugOutput.log("PARAM_SYSTEM_TIME: " + PARAM_SYSTEM_TIME); //long	System time
        DebugOutput.log("PARAM_AMBIENT_TEMPERATURE: " + PARAM_AMBIENT_TEMPERATURE); //float	Ambient temperature, C
        DebugOutput.log("PARAM_AMBIENT_HUMIDITY: " + PARAM_AMBIENT_HUMIDITY); //float	Ambient humidity, %
        DebugOutput.log("PARAM_AMBIENT_LIGHT: " + PARAM_AMBIENT_LIGHT); //float	Ambient light
        DebugOutput.log("PARAM_SOLAR_BAT_VOLTAGE: " + PARAM_SOLAR_BAT_VOLTAGE); //float	Voltage solar battery, volts
        DebugOutput.log("PARAM_LIFE_BAT_VOLTAGE: " + PARAM_LIFE_BAT_VOLTAGE); //float	Voltage Li-Fe battery, volts
        DebugOutput.log("PARAM_LIFE_BAT_VOLUME: " + PARAM_LIFE_BAT_VOLUME); //float	Volume Li-Fe battery, %);
        DebugOutput.log("PARAM_EXT_POWER_VOLTAGE: " + PARAM_EXT_POWER_VOLTAGE); //float	Voltage external power supply, volts);
        DebugOutput.log("PARAM_SD_CARD_VOLUME: " + PARAM_SD_CARD_VOLUME); //int	Volume SD card, bytes);
        DebugOutput.log("PARAM_SD_CARD_FREE_SPACE: " + PARAM_SD_CARD_FREE_SPACE); //	int	Free space in SD card, bytes
        DebugOutput.log("PARAM_CONNECTED_SENSORS uids: " + getUIDsAsStr()); // Array of sensor UIDs
        DebugOutput.log("PARAM_CONNECTED_SENSORS types: " + getTypessAsStr()); // Array of sensor types
        DebugOutput.log("PARAM_CONNECTION_PERIOD: " + PARAM_CONNECTION_PERIOD); //int Period connection to server, second); .log(": " +0- always hold connection));
        DebugOutput.log("PARAM_MAIN_SERVER: " + PARAM_MAIN_SERVER); //String 32	DNS or IP address for main server connection);
        DebugOutput.log("PARAM_MAIN_LOGIN: " + PARAM_MAIN_LOGIN); //String 32	Login for main server connection);
        DebugOutput.log("PARAM_MAIN_PASSWORD: " + PARAM_MAIN_PASSWORD); //String 32	Password for main server connection);
        DebugOutput.log("PARAM_ALTERNATE_SERVER: " + PARAM_ALTERNATE_SERVER); //String 32	DNS or IP address for alternative server connect);
        DebugOutput.log("PARAM_ALTERNATE_LOGIN: " + PARAM_ALTERNATE_LOGIN); //String 32	Login for alternative server connection);
        DebugOutput.log("PARAM_ALTERNATE_PASSWORD: " + PARAM_ALTERNATE_PASSWORD); //String 32	Password for alternative server connection);
        DebugOutput.log("PARAM_GSM_WIFI_CONNECTION: " + PARAM_GSM_WIFI_CONNECTION); //String 5	Chanel connection GSM or WiFi
        DebugOutput.log("PARAM_GSM_SIGNAL_QUALITY: " + PARAM_GSM_SIGNAL_QUALITY); //float	Quality signal GSM modem
        DebugOutput.log("PARAM_SIM_CARD_NUMBER: " + PARAM_SIM_CARD_NUMBER); //String 15	Number sim-card
        DebugOutput.log("PARAM_APN_FOR_GPRS: " + PARAM_APN_FOR_GPRS); //String 32	APN for GPRS network
        DebugOutput.log("PARAM_GPRS_LOGIN: " + PARAM_GPRS_LOGIN); //String 32	Login for GPRS network
        DebugOutput.log("PARAM_GPRS_PASSWORD: " + PARAM_GPRS_PASSWORD); //String 32	Password for GPRS connection
        DebugOutput.log("PARAM_WIFI_SIGNAL_QUALITY: " + PARAM_WIFI_SIGNAL_QUALITY); //float	Quality signal WiFi modem
        DebugOutput.log("PARAM_WIFI_NET_NAME: " + PARAM_WIFI_NET_NAME); //String 32	Name WiFi network
        DebugOutput.log("PARAM_WIFI_LOGIN: " + PARAM_WIFI_LOGIN); //String 32	Login for WiFi network
        DebugOutput.log("PARAM_WIFI_PASSWORD: " + PARAM_WIFI_PASSWORD); //String 32	Password for WiFi network
    }

    String getUIDsAsStr() {
        String r = "";
        String comma = "";
        if (PARAM_SENSOR_UIDS == null) {
            r = "empty";
        } else {
            for (Integer i : PARAM_SENSOR_UIDS) {
                r = r.concat(comma + i.toString());
                comma = ", ";
            }
        }

        return r;
    }

    String getTypessAsStr() {
        String r = "";
        String comma = "";
        if (PARAM_SENSOR_TYPES == null) {
            r = "empty";
        } else {
            for (Integer i : PARAM_SENSOR_TYPES) {
                r = r.concat(comma + i.toString());
                comma = ", ";
            }
        }
        return r;
    }

//    public void saveStationDesription() {
//        fDbManager.saveStationDesription(this);
//    }

    
}
