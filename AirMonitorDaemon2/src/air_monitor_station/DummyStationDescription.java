/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import java.util.ArrayList;

/**
 *
 * @author SAP
 */
public class DummyStationDescription extends StationDescriptionBase {

    public DummyStationDescription() {
        super();
        PARAM_SERIAL_NUMBER = "0101";
        PARAM_HW_VERSION = " 1.1";
        PARAM_FW_VERSION = "1.1";
        PARAM_SYSTEM_TIME = "1460580119";
        PARAM_AMBIENT_TEMPERATURE = "35.1";
        PARAM_AMBIENT_HUMIDITY = "95.1";
        PARAM_AMBIENT_LIGHT = "152.1";
        PARAM_SOLAR_BAT_VOLTAGE = "14.1";
        PARAM_LIFE_BAT_VOLTAGE = "13.2";
        PARAM_LIFE_BAT_VOLUME = "13";
        PARAM_EXT_POWER_VOLTAGE = "24.3";
        PARAM_SD_CARD_VOLUME = "00000000";
        PARAM_SD_CARD_FREE_SPACE = "00000000";
        PARAM_SENSOR_UIDS = new ArrayList<>();
        PARAM_SENSOR_UIDS.add(21001);
        PARAM_SENSOR_UIDS.add(21002);
        PARAM_SENSOR_UIDS.add(21003);
        PARAM_SENSOR_UIDS.add(21004);
        PARAM_SENSOR_UIDS.add(21005);
        PARAM_SENSOR_UIDS.add(21005);
        PARAM_SENSOR_UIDS.add(21005);
        PARAM_SENSOR_UIDS.add(21006);
        PARAM_SENSOR_UIDS.add(21006);
        PARAM_SENSOR_UIDS.add(21007);
        PARAM_SENSOR_UIDS.add(21007);
        PARAM_SENSOR_UIDS.add(21008);
        PARAM_SENSOR_UIDS.add(21008);
        
        PARAM_SENSOR_TYPES = new ArrayList<>();
        PARAM_SENSOR_TYPES.add(33804);
        PARAM_SENSOR_TYPES.add(33289);
        PARAM_SENSOR_TYPES.add(33031);
        PARAM_SENSOR_TYPES.add(33547);
        PARAM_SENSOR_TYPES.add(34822);
        PARAM_SENSOR_TYPES.add(33030);
        PARAM_SENSOR_TYPES.add(34054);
        PARAM_SENSOR_TYPES.add(35001);
        PARAM_SENSOR_TYPES.add(35002);
        PARAM_SENSOR_TYPES.add(2574);
        PARAM_SENSOR_TYPES.add(3342);
        PARAM_SENSOR_TYPES.add(2831);
        PARAM_SENSOR_TYPES.add(3343);
        
        PARAM_CONNECTION_PERIOD = "0";
        PARAM_MAIN_SERVER = "192.168.168.1";
        PARAM_MAIN_LOGIN = "admin";
        PARAM_MAIN_PASSWORD = "admin";
        PARAM_ALTERNATE_SERVER = "TestServer";
        PARAM_ALTERNATE_LOGIN = "admin";
        PARAM_ALTERNATE_PASSWORD = "admin";
        PARAM_GSM_WIFI_CONNECTION = "1.0";
        PARAM_GSM_SIGNAL_QUALITY = "27.1";
        PARAM_SIM_CARD_NUMBER = "380965894";
        PARAM_APN_FOR_GPRS = "TestPoint";
        PARAM_GPRS_LOGIN = "GPRSadmin";
        PARAM_GPRS_PASSWORD = "GPRSpass";
        PARAM_WIFI_SIGNAL_QUALITY = "10.5";
        PARAM_WIFI_NET_NAME = "TestPointWiFi";
        PARAM_WIFI_LOGIN = "WiFiadmin";
        PARAM_WIFI_PASSWORD = "WiFiPass";
        
    }
    
    public void updateLocation(double lat, double lon){
            existsLocation = true;
            PARAM_LATTITUDE = Double.toString(lat);
            PARAM_LONGITUDE = Double.toString(lon);
    }
}
