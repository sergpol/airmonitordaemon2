/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

public class AdcData {
    // Коды ошибок АЦП
    public static final int ADC_NO_ERR = 0;
    public static final int ADC_WRONG_CH_ERR = 1;
    public static final int ADC_NO_REFERENCE_ERR = 2;
    public static final int ADC_OUT_OF_RANGE_ERR = 3;
    public static final int ADC_LOW_AVCC = 4;
   
    // АЦП девайса
    
    public static final double ADC_REFERENCE_VOLTAGE_UV = 1200000.0;
    public static final int ADC_FULLSCALE_UNIPOLAR = 65535;
    public static final int ADC_FULLSCALE_BIPOLAR = 32767;

    private int errCode;
    private double voltage;
    private int sensorData;
    
    public AdcData(){
        errCode = 0;
        voltage = 0;
    }
    
    public int getErrCode(){
        return errCode;
    }
    
    public double getMicroVolt(){
        return voltage;
    }

    public void setMicroVolt(double val){
        this.voltage = val;
    }
    
    public double getMilliVolt(){
        return voltage / 1000.0; //convert from microvolt to millivolt
    }
    
    public double getVolt(){
        return voltage / 1000000.0; //convert from microvolt to volt
    }
    
    public int getRawData(){
        return sensorData;
    }
    
    public AdcData(int sensorData){
        //this.sensorData = sensorData;
        unpackAdcData(sensorData); 
    }
    
    public void unpackAdcData(int sensorData){
        this.sensorData = sensorData;
        int adcData = (sensorData & 0x0000FFFF); // Слово АЦП 
        int adcGain = (sensorData & 0x00FF0000) >> 16; // Усиление
        int adcPolarityMode = (sensorData & (1 << 24)); // Режим полярности АЦП (биполярный/!униполярный)
        int adcBufferMode = (sensorData & (1 << 25)); // Режим буфера АЦП (буферизированный/!небуферизированный)
        errCode = (sensorData & 0xFC000000) >>> 26; // Код ошибки АЦП        
        
        // Считаем данные
        if(adcPolarityMode != 0){
         // Биполярный режим
             voltage = (((double)adcData - ADC_FULLSCALE_BIPOLAR) * ADC_REFERENCE_VOLTAGE_UV) / ((double)ADC_FULLSCALE_BIPOLAR * adcGain);
         }

        else{
         // Униполярный режим
             voltage = ((double)adcData * ADC_REFERENCE_VOLTAGE_UV) / (ADC_FULLSCALE_UNIPOLAR * adcGain);
         }
    }
    
}
