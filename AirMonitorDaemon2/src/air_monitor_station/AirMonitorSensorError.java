/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

/**
 *
 * @author SAP
 */
public class AirMonitorSensorError extends AirMonitorException {
    public AirMonitorSensorError() {
            super();
    }

    //Constructor that accepts a message
    public AirMonitorSensorError(String message) {
        super(message);
    }
    
}
