package air_monitor_station;

import air_monitor_debug.DebugOutput;
import java.util.HashMap;
import java.util.Map;


public class SensorDefs {
    public static enum SensorKind {UNKNOWN, RESISTIVE, ELECTROCHEMICAL, OPTICAL, DIDGITAL};

    //pollutant types
//    public static final int NULL_TEMP = -273;
//    public static double fTemperature = 20;//NULL_TEMP;
    public static final int SENSOR_MEASURES_CO = 1; // Carbon Monooxide
    public static final int SENSOR_MEASURES_CH4 = 2; // Methane
    public static final int SENSOR_MEASURES_O3 = 3; // Ozone
    public static final int SENSOR_MEASURES_VOC = 4; // Volatile Organic Compound
    public static final int SENSOR_MEASURES_NO2 = 5; // Nitrogen Dioxide
    public static final int SENSOR_MEASURES_SO2 = 6; // Sulphur Dioxide
    public static final int SENSOR_MEASURES_H2S = 7; // Hydrogen Sulphide
    public static final int SENSOR_MEASURES_NH3 = 8; // Ammonia
    public static final int SENSOR_MEASURES_PM = 9; // Particulate Matter
    public static final int SENSOR_MEASURES_ABS_PRESSURE = 10; // Absolute Pressure
    public static final int SENSOR_MEASURES_HUMIDITY = 11; // Relative humidity
    public static final int SENSOR_MEASURES_NOTHING = 12; //For dummy sensor emulation
    public static final int SENSOR_MEASURES_TEMPERATURE = 13; // Temperature

    //sensor names
    public static final int SENSOR_NAME_SGX4DT = 1;
    public static final int SENSOR_NAME_ME3O3 = 2;
    public static final int SENSOR_NAME_ME3NO2 = 3;
    public static final int SENSOR_NAME_EC420SO2 = 4;
    public static final int SENSOR_NAME_SGX7NH3 = 5;
    public static final int SENSOR_NAME_MICS6814 = 6;
    public static final int SENSOR_NAME_TGS2442 = 7;
    public static final int SENSOR_NAME_ASMLC = 8;
    public static final int SENSOR_NAME_TGS2611 = 9;
    public static final int SENSOR_NAME_ASMLK = 10;
    public static final int SENSOR_NAME_MICS2614 = 11;
    public static final int SENSOR_NAME_TGS2602 = 12;
    public static final int SENSOR_NAME_GP2Y1010AU0F = 13;
    public static final int SENSOR_NAME_MPL3115A2 = 14;
    public static final int SENSOR_NAME_SHT21 = 15;
    public static final int SENSOR_NAME_DUMMY = 16;

    public static final int SENSOR_TYPE_ANALOG = (int) 0x8000; //1 << 15;
    public static final int SENSOR_TYPE_DIGITAL = 0; //(0 << 15);

    //SENSOR_TYPES
    // Электрохимические
    public static final int SGX4DT_CO = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_CO << 8) | SENSOR_NAME_SGX4DT);
    public static final int SGX4DT_H2S = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_H2S << 8) | SENSOR_NAME_SGX4DT);
    public static final int ME3O3_O3 = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_O3 << 8) | SENSOR_NAME_ME3O3);
    public static final int ME3NO2_NO2 = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_NO2 << 8) | SENSOR_NAME_ME3NO2);
    public static final int EC420SO2_SO2 = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_SO2 << 8) | SENSOR_NAME_EC420SO2);
    public static final int SGX7NH3_NH3 = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_NH3 << 8) | SENSOR_NAME_SGX7NH3);
    // Резистивные с нагревателями
    public static final int MICS6814_NH3 = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_NH3 << 8) | SENSOR_NAME_MICS6814);
    public static final int MICS6814_CO = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_CO << 8) | SENSOR_NAME_MICS6814);
    public static final int MICS6814_NO2 = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_NO2 << 8) | SENSOR_NAME_MICS6814);
    public static final int TGS2442_CO = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_CO << 8) | SENSOR_NAME_TGS2442);
    public static final int ASMLC_CO = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_CO << 8) | SENSOR_NAME_ASMLC);
    public static final int TGS2611_CH4 = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_CH4 << 8) | SENSOR_NAME_TGS2611);
    public static final int ASMLK_CH4 = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_CH4 << 8) | SENSOR_NAME_ASMLK);
    public static final int MICS2614_O3 = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_O3 << 8) | SENSOR_NAME_MICS2614);
    public static final int TGS2602_VOC = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_VOC << 8) | SENSOR_NAME_TGS2602);

    // Оптические
    public static final int GP2Y1010AU0F_PM = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_PM << 8) | SENSOR_NAME_GP2Y1010AU0F);
    public static final int SDS011_PM25 = 35001;
    public static final int SDS011_PM10 = 35002;
    
    // Пустышка чтобы забить аналоговый канал
    public static final int ANALOG_DUMMY = (SENSOR_TYPE_ANALOG | (SENSOR_MEASURES_NOTHING << 8) | SENSOR_NAME_DUMMY);

    // Цифровые сенсоры
    public static final int MPL3115A2_ABS_PRESSURE = (SENSOR_TYPE_DIGITAL | (SENSOR_MEASURES_ABS_PRESSURE << 8) | SENSOR_NAME_MPL3115A2);
    public static final int SHT21_HUMIDITY = (SENSOR_TYPE_DIGITAL | (SENSOR_MEASURES_HUMIDITY << 8) | SENSOR_NAME_SHT21);
    //new
    public static final int MPL3115A2_TEMPERATURE = (SENSOR_TYPE_DIGITAL | (SENSOR_MEASURES_TEMPERATURE << 8) | SENSOR_NAME_MPL3115A2);
    public static final int SHT21_TEMPERATURE = (SENSOR_TYPE_DIGITAL | (SENSOR_MEASURES_TEMPERATURE << 8) | SENSOR_NAME_SHT21);
//    //for Yuriy
//    public static final int SHT21H_HUMIDITY = 2832;
//    public static final int SHT21T_TEMPERATURE = 3345;
//    public static final int MPL3115A2T_TEMPERATURE = 3344;
//    public static final int MPL3115A2P_ABS_PRESSURE = 2576;
    

    //measure units
    public static final byte MU_NO_VALUE = 0;
    public static final byte MU_TEMPERATURE = 1; //C
    public static final byte MU_HUMIDITY = 2;  //%
    public static final byte MU_CONCENTRATION_PPM = 3; //ppm
    public static final byte MU_PRESSURE = 4; //kPa
    public static final byte MU_DUST_WEIGHT_MG = 5; //mg/m3
    public static final byte MU_DUST_WEIGHT_UG = 6; //ug/m3
    public static final byte MU_CONCENTRATION_PPB = 7; //ppb
    public static final byte MU_ADC_VOLTAGE = 8; //raw data from adc
    public static final byte MU_GPS = 127;
    //public static final byte MU_PM25 = 9; //PM2.5
    //public static final byte MU_PM10 = 10; //PM10
    
    
    //error codes
    public static final int ERR_SENSOR = 1; //AirMonitorSensorError exception
    public static final int ERR_VOLTAGE = 2; //voltage was calc to NaN
    public static final int ERR_FOTMULA = 3; //formula returns NaN
    public static final int ERR_TEMPERATURE_OR_HUMIDITY = 4;//Temperature or Humidity sensors failed
    

    public static String getSensorName(int sensorType) throws AirMonitorSensorError{

        switch (sensorType) {
            //электрохимические
            case SGX4DT_CO:
                return "SGX4DT_CO";
            case SGX4DT_H2S:
                return "SGX4DT_H2S";
            case ME3O3_O3:
                return "ME3O3_O3";
            case ME3NO2_NO2:
                return "ME3NO2_NO2";
            case EC420SO2_SO2:
                return "EC420SO2_SO2";
            case SGX7NH3_NH3:
                return "SGX7NH3_NH3";
            // Резистивные с нагревателями
            case MICS6814_NH3:
                return "MICS6814_NH3";
            case MICS6814_CO:
                return "MICS6814_CO";
            case MICS6814_NO2:
                return "MICS6814_NO2";
            case TGS2442_CO:
                return "TGS2442_CO";
            case ASMLC_CO:
                return "ASMLC_CO";
            case TGS2611_CH4:
                return "TGS2611_CH4";
            case ASMLK_CH4:
                return "ASMLK_CH4";
            case MICS2614_O3:
                return "MICS2614_O3";
            case TGS2602_VOC:
                return "TGS2602_VOC";
            // Оптические
            case GP2Y1010AU0F_PM:
                return "GP2Y1010AU0F_PM";
            case SDS011_PM10:
                return "SDS011_PM10";
            case SDS011_PM25:
                return "SDS011_PM25";
            // Пустышка чтобы забить аналоговый канал
            case ANALOG_DUMMY:
                return "ANALOG_DUMMY";
            // Цифровые сенсоры
            case MPL3115A2_ABS_PRESSURE:
                return "MPL3115A2_ABS_PRESSURE";
            case MPL3115A2_TEMPERATURE:
                return "MPL3115A2_TEMPERATURE";
            case SHT21_HUMIDITY:
                return "SHT21_HUMIDITY";
            case SHT21_TEMPERATURE:
                return "SHT21_TEMPERATURE";
                
            default:
                throw new AirMonitorSensorError("Unknown sensor type: " 
                        + Integer.toString(sensorType) 
                        + " in getSensorName function.");
        }
    }

    public static SensorKind getSensorKind(int sensorType) throws AirMonitorSensorError{

        switch (sensorType) {
            //электрохимические
            case SGX4DT_CO:
            case SGX4DT_H2S:
            case ME3O3_O3:
            case ME3NO2_NO2:
            case EC420SO2_SO2:
            case SGX7NH3_NH3:
                return SensorKind.ELECTROCHEMICAL;
            // Резистивные с нагревателями
            case MICS6814_NH3:
            case MICS6814_CO:
            case MICS6814_NO2:
            case TGS2442_CO:
            case ASMLC_CO:
            case TGS2611_CH4:
            case ASMLK_CH4:
            case MICS2614_O3:
            case TGS2602_VOC:
                return SensorKind.RESISTIVE;
            // Оптические
            case GP2Y1010AU0F_PM:
            case SDS011_PM25:
            case SDS011_PM10:    
                return SensorKind.OPTICAL;
            // Цифровые сенсоры
            case MPL3115A2_ABS_PRESSURE:
            case MPL3115A2_TEMPERATURE:
            case SHT21_HUMIDITY:
            case SHT21_TEMPERATURE:
                return SensorKind.DIDGITAL;
            default:
                throw new AirMonitorSensorError("Unknown sensor type: " 
                        + Integer.toString(sensorType) 
                        + " in getSensorKind function.");
        }
    }
        
    public static int getSensorType(String sensorName) throws AirMonitorSensorError{

        switch (sensorName) {
            //электрохимические
            case "SGX4DT_CO":
                return SGX4DT_CO;
            case "SGX4DT_H2S":
                return SGX4DT_H2S;
            case "ME3O3_O3":
                return ME3O3_O3;
            case "ME3NO2_NO2":
                return ME3NO2_NO2;
            case "EC420SO2_SO2":
                return EC420SO2_SO2;
            case "SGX7NH3_NH3":
                return SGX7NH3_NH3;
            // Резистивные с нагревателями
            case "MICS6814_NH3":
                return MICS6814_NH3;
            case "MICS6814_CO":
                return MICS6814_CO;
            case "MICS6814_NO2":
                return MICS6814_NO2;
            case "TGS2442_CO":
                return TGS2442_CO;
            case "ASMLC_CO":
                return ASMLC_CO;
            case "TGS2611_CH4":
                return TGS2611_CH4;
            case "ASMLK_CH4":
                return ASMLK_CH4;
            case "MICS2614_O3":
                return MICS2614_O3;
            case "TGS2602_VOC":
                return TGS2602_VOC;
            // Оптические
            case "GP2Y1010AU0F_PM":
                return GP2Y1010AU0F_PM;
            case "SDS011_PM25":
                return SDS011_PM25;
            case "SDS011_PM10":
                return SDS011_PM10;
            // Пустышка чтобы забить аналоговый канал
            case "ANALOG_DUMMY":
                return ANALOG_DUMMY;
            // Цифровые сенсоры
            case "MPL3115A2_ABS_PRESSURE":
                return MPL3115A2_ABS_PRESSURE;
            case "MPL3115A2_TEMPERATURE":
                return MPL3115A2_TEMPERATURE;
            case "SHT21_HUMIDITY":
                return SHT21_HUMIDITY;
            case "SHT21_TEMPERATURE":
                return SHT21_TEMPERATURE;
            default:
                throw new AirMonitorSensorError("Unknown sensor name: " 
                        + sensorName + " in getSensorType function.");
        }
    }
        
    public static void print_sensor_names(){
            //электрохимические
            System.out.println("electrochemical");
            System.out.println("SGX4DT_CO: " + Integer.toString(SGX4DT_CO));
            System.out.println("SGX4DT_H2S: " + Integer.toString(SGX4DT_H2S));
            System.out.println("ME3O3_O3: " + Integer.toString(ME3O3_O3));
            System.out.println("ME3NO2_NO2: " + Integer.toString(ME3NO2_NO2));
            System.out.println("EC420SO2_SO2: " + Integer.toString(EC420SO2_SO2));
            System.out.println("SGX7NH3_NH3: " + Integer.toString(SGX7NH3_NH3));

            // Резистивные с нагревателями
            System.out.println("resistive");
            System.out.println("MICS6814_NH3: " + Integer.toString(MICS6814_NH3));
            System.out.println("MICS6814_CO: " + Integer.toString(MICS6814_CO));
            System.out.println("MICS6814_NO2: " + Integer.toString(MICS6814_NO2));
            System.out.println("TGS2442_CO: " + Integer.toString(TGS2442_CO));
            System.out.println("ASMLC_CO: " + Integer.toString(ASMLC_CO));
            System.out.println("TGS2611_CH4: " + Integer.toString(TGS2611_CH4));
            System.out.println("ASMLK_CH4: " + Integer.toString(ASMLK_CH4));
            System.out.println("MICS2614_O3: " + Integer.toString(MICS2614_O3));
            System.out.println("TGS2602_VOC: " + Integer.toString(TGS2602_VOC));
            // Оптические
            System.out.println("optical");
            System.out.println("GP2Y1010AU0F_PM: " + Integer.toString(GP2Y1010AU0F_PM));
            System.out.println("SDS011_PM25: " + Integer.toString(SDS011_PM25));
            System.out.println("SDS011_PM10: " + Integer.toString(SDS011_PM10));
            // Пустышка чтобы забить аналоговый канал
            System.out.println("stub");
            System.out.println("ANALOG_DUMMY: " + Integer.toString(ANALOG_DUMMY));
            // Цифровые сенсоры
            System.out.println("digital");
            System.out.println("MPL3115A2_ABS_PRESSURE: " + Integer.toString(MPL3115A2_ABS_PRESSURE));
            System.out.println("MPL3115A2_TEMPERATURE: " + Integer.toString(MPL3115A2_TEMPERATURE));
            System.out.println("SHT21_HUMIDITY: " + Integer.toString(SHT21_HUMIDITY));
            System.out.println("SHT21_TEMPERATURE: " + Integer.toString(SHT21_TEMPERATURE));

    }
    
}
