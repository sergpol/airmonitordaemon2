package air_monitor_station;

import air_monitor_debug.DebugOutput;
import air_monitor_station.sensors.SensorType;
import air_monitor_station.sensors.SensorTypesArray;
import ambience_db.CassandraDBManager;
import java.util.List;
import air_monitor_station.sensors.ISensorType;
import ambience_db.AmbienceDbManager;

/**
 *
 * @author SAP
 */
public class SensorsArray2 extends SensorsArrayBase {

    private SensorTypesArray fSensorTypes = new SensorTypesArray();

    public SensorsArray2(int stationUID, StationDescription stationDescr) {
        super(stationUID, stationDescr);
    }

    protected void parseSensorMetering(int sensorUID, int sensorType, int rawValue) {
        SensorBase metering = findSensorMetering(sensorUID, sensorType);
        if (metering == null) {
            ISensorType sensor = fSensorTypes.find(sensorType);
            if (sensor == null) {
                DebugOutput.logStation(fStationUID, "Sensor UID: "
                        + Integer.toString(sensorUID)
                        + " type: " + Integer.toString(sensorType)
                        + " is not founf in sensors array");
                return;
            } else {
                metering = new Sensor2(this, sensor, sensorUID);
                fSensorLst.add(metering);
                metering.setMetering(rawValue);
            }
        } else {
            metering.setMetering(rawValue);
        }

    }
}
