/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import air_monitor_debug.DebugOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author SAP
 */
public abstract class SensorParamJson extends SensorParams {

    public SensorParamJson(int boxUID) {
        super(boxUID);
    }

    public abstract JSONObject loadJsonSensors() throws IOException, ParseException;

    @Override
    public void loadParams(List<Integer> sensorUIDs) {
        try {
            JSONObject sensors = loadJsonSensors();
            for (Integer uid : sensorUIDs) {

                Object val = sensors.get(uid.toString());
                if (val instanceof JSONObject) {
                    try {
                        SensorParamVal p = parseSensor(uid, (JSONObject) val);
                        params.add(p);
                    } catch (AirMonitorSensorError e) {
                        DebugOutput.logStation(getBoxUID(), e.getMessage());
                    }

                } else if (val instanceof JSONArray) {
                    Iterator<JSONObject> iter = ((JSONArray) val).iterator();
                    while (iter.hasNext()) {
                        JSONObject sensor = iter.next();
                        try {
                            SensorParamVal p = parseSensor(uid, sensor);
                            params.add(p);
                        } catch (AirMonitorSensorError e) {
                            DebugOutput.logStation(getBoxUID(), e.getMessage());
                        }
                    }
                } else if (val == null) {//configuration is not found
                    DebugOutput.logStation(getBoxUID(), "SensorParamJson: Can't load configuration for sensor " + uid.toString());
                } else {
                    DebugOutput.logStation(getBoxUID(), "SensorParamJson: Can't parse config for sensor " + uid.toString());
                }
            }
        } catch (ParseException e) {
            DebugOutput.logStation(getBoxUID(),"Parse error of configuration object in position: " + e.getPosition());
            DebugOutput.logStation(getBoxUID(), e.getMessage());
        } catch (IOException e) {
            DebugOutput.logStation(getBoxUID(), e.getMessage());
        }
    }

    public SensorParamVal parseSensor(int sensorUID, JSONObject sensor) throws AirMonitorSensorError {
        SensorParamVal p;
        String sensorName = (String) sensor.get("sensor_name");
        int sensorType = SensorDefs.getSensorType(sensorName);
        switch (SensorDefs.getSensorKind(sensorType)) {
            case RESISTIVE:
                p = new ResSensorParamVal(sensorType);
                break;
            case ELECTROCHEMICAL:
                p = new EchSensorParamVal(sensorType);
                break;
            case OPTICAL:
                p = new OptSensorParamVal(sensorType);
                break;
            case DIDGITAL:
                p = new DigSensorParamVal(sensorType);
                break;
            case UNKNOWN:
                throw new AirMonitorSensorError("Unknown sensor type " + Integer.toString(sensorType));
            default:
                throw new AirMonitorSensorError("Unknown kind of the sensor " + Integer.toString(sensorType));
        }
        p.parseSensorFromJson(sensorUID, sensor);
        return p;
    }
}
