/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ambience_db;

import air_monitor_debug.DebugOutput;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author SAP
 */
public class CassandraDBManager implements IDbSensorManager{

    private static Cluster fCluster = null;
    private static Session fSession = null;

    //private int fAirStationId = 0;
    private PreparedStatement fInsertStatement = null;

    public CassandraDBManager() {
        //fAirStationId = stationId;
    }

    public static void connect(String serverURL, String dbName) {
        PoolingOptions poolingOptions = new PoolingOptions();

        poolingOptions
                .setConnectionsPerHost(HostDistance.LOCAL, 4, 100)
                .setConnectionsPerHost(HostDistance.REMOTE, 2, 100);

        fCluster = Cluster.builder().addContactPoint(serverURL).withPoolingOptions(poolingOptions).build();
        fSession = fCluster.connect(dbName);
    }
    
    public static void closeConnection(){
        if (fSession != null){
            fSession.close();
            fSession = null;
        }
        if(fCluster != null){
            fCluster.close();
            fCluster = null;
        }
    }
    
    public void saveSensor(
            int serialnumber,
            Date currentsampletime,
            int sensorUid,
            int sensorType,
            byte measureUnit,
            double avgVal,
            double avgMicroVolt){
    }    
    
    public void saveSensorHistory(
            Date metering_datetime,
            byte measure_unit,
            double value,
            int sensor_id,
            int sensor_type,
            int serialnumber,
            double raw_value) {

            BoundStatement st = prepareInsertMeteringStatement(
                    fSession,
                    serialnumber,
                    sensor_id,
                    sensor_type,
                    metering_datetime,
                    value,
                    measure_unit,
                    raw_value);
            fSession.execute(st);
    }
    
    private BoundStatement prepareInsertMeteringStatement(
            Session session,
            int serialnumber,
            int sensor_id,
            int sensor_type,
            Date timestamp_of_metering,
            double value,
            int measure_unit,
            double raw_value) {

        if (fInsertStatement == null) {
            fInsertStatement = session.prepare(insertMeteringCQL);
        }

        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTime(date);
        int month_of_metering = calendar.get(Calendar.MONTH) + 1;
        int year_of_metering = calendar.get(Calendar.YEAR);

        BoundStatement bound = fInsertStatement.bind()
                .setInt("airstation_id", serialnumber)
                .setInt("sensor_id", sensor_id)
                .setInt("sensor_type", sensor_type)
                .setInt("year_of_metering", year_of_metering)
                .setInt("month_of_metering", month_of_metering)
                .setTimestamp("timestamp_of_metering", timestamp_of_metering)
                .setDouble("value", value)
                .setInt("measure_unit", measure_unit)
                .setDouble("raw_value", raw_value);

        return bound;
    }

    public void log(int serialNumber, String msg) {
        //System.out.println(msg);
        DebugOutput.logStation(serialNumber, msg);
    }
    
    static final String insertMeteringCQL = "INSERT INTO ambience.meterings("
            + " airstation_id,"
            + " sensor_id,"
            + " sensor_type,"
            + " year_of_metering,"
            + " month_of_metering,"
            + " timestamp_of_metering,"
            + " value,"
            + " measure_unit,"
            + " raw_value)"
            + " VALUES("
            + " :airstation_id,"
            + " :sensor_id,"
            + " :sensor_type,"
            + " :year_of_metering,"
            + " :month_of_metering,"
            + " :timestamp_of_metering,"
            + " :value,"
            + " :measure_unit,"
            + " :raw_value);";

}

//CREATE TABLE ambience.meterings (
//    airstation_id int,
//    sensor_id int,
//    sensor_type int,
//    year_of_metering int,
//    month_of_metering int,
//    timestamp_of_metering timestamp,
//    measure_unit int,
//    raw_value double,
//    value double,
//    PRIMARY KEY ((airstation_id, sensor_id, sensor_type, year_of_metering, month_of_metering), datetime_of_metering)
//) WITH CLUSTERING ORDER BY (datetime_of_metering ASC)
//    AND bloom_filter_fp_chance = 0.01
//    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
//    AND comment = ''
//    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
//    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
//    AND crc_check_chance = 1.0
//    AND dclocal_read_repair_chance = 0.1
//    AND default_time_to_live = 0
//    AND gc_grace_seconds = 864000
//    AND max_index_interval = 2048
//    AND memtable_flush_period_in_ms = 0
//    AND min_index_interval = 128
//    AND read_repair_chance = 0.0
//    AND speculative_retry = '99PERCENTILE';
