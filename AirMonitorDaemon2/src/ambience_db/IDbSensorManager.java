/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ambience_db;

import java.util.Date;

/**
 *
 * @author SAP
 */
public interface IDbSensorManager {
    public void saveSensor(
            int serialnumber,
            Date currentsampletime,
            int sensorUid,
            int sensorType,
            byte measureUnit,
            double avgVal,
            double avgMicroVolt) ;

    public void saveSensorHistory(Date currentsampletime,
            byte measureUnit,
            double val,
            int sensorUid,
            int sensorType,
            int serialnumber,
            double rawVal);
}
