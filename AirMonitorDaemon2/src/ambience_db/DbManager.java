/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ambience_db;

import air_monitor_debug.DebugOutput;
import cast.Cast;
import java.sql.*;
import com.mchange.v2.c3p0.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedList;

public class DbManager{
    private static final String dbDriverName = "org.postgresql.Driver"; //"com.mysql.jdbc.Driver";
    private static ComboPooledDataSource fDbSource;
    private static boolean fIsWorking = false;

    public static boolean init(String aHost, int aPort, String aDbName, String aUser, String aPass) {
        String aURL = "jdbc:postgresql://" + aHost + ":" + Integer.toString(aPort) + "/" + aDbName;
        fDbSource = new ComboPooledDataSource();
        fIsWorking = false;
        try {
            fDbSource.setDriverClass(dbDriverName);
            fDbSource.setJdbcUrl(aURL);
            fDbSource.setUser(aUser);
            fDbSource.setPassword(aPass);
            fDbSource.setMinPoolSize(1);
            fDbSource.setMaxPoolSize(10);
            fDbSource.setAutoCommitOnClose(true);
            fDbSource.setCheckoutTimeout(3000);
            fIsWorking = testConnection();

        } catch (Exception e) {
            fDbSource.close();
            fDbSource = null;
            DebugOutput.log("DataBaseWorker : SetUpAndStart -> " + e.getMessage());
            fIsWorking = false;
        }
        return fIsWorking;
    }

    public static boolean testConnection(){
        // TODO: implement testConnection
        return true;
    }
    
    public static void shutDownAndStop() {
        fDbSource.close();
        fDbSource = null;
    }

    public boolean isWorking(){
        return fIsWorking && !(fDbSource==null);
    }
    
    private static Connection getConnection() throws SQLException {
        return fDbSource.getConnection();
    }

    public Object[] sqlSelectOneRow(String sql, Object... parameters) {

        if (!isWorking()) {
            return null;
        }

        try (Connection con = getConnection()) {
            Object[] result = null;

            try (PreparedStatement ps = con.prepareStatement(sql)) {
                if (parameters != null) {
                    for (int i = 0; i < parameters.length; i++) {
                        ps.setObject(i + 1, parameters[i]);
                    }
                }

                try (ResultSet rs = ps.executeQuery()) {
                    int columnCount = rs.getMetaData().getColumnCount();
                    if (rs.next()) {
                        //Object[] dataRow = new Object[columnCount];
                        result = new Object[columnCount];
                        for (int i = 0; i < columnCount; i++) {
                            result[i] = rs.getObject(i + 1);
                        }
                        
                    }
                }
            }
            return result;
        } catch (SQLException ex) {
            DebugOutput.log("DbManager sqlSelectOneRow error: " + ex.getMessage());
            return null;
        }
    }

    public int sqlUpdate(String sql, Object... parameters) {
        if (!isWorking()) {
            return -1;
        }

        try (Connection con = getConnection()) {
            int updatedCnt;
            try (PreparedStatement ps = con.prepareStatement(sql)) {
                if (parameters != null) {
                    //DebugOutput.log("Values inserted or updated in db:");
                    for (int i = 0; i < parameters.length; i++) {
                        ps.setObject(i + 1, parameters[i]);
                        //DebugOutput.log(Integer.toString(i+1) + ": " + parameters[i]);
                    }
                }
                updatedCnt = ps.executeUpdate();
            }
            return updatedCnt;
        } catch (Exception ex) {
            DebugOutput.log("DbManager sqlUpdate error: " + ex.getMessage());
            return -1;
        }
    }

}
