/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ambience_db;

import air_monitor_debug.DebugOutput;
import air_monitor_station.IStationDescription;
import air_monitor_station.StationDescription;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author SAP
 */
public class AmbienceDbManager extends DbManager implements IDbSensorManager {

    public void deleteOutOfRangeHistory(int serialnumber, int limitOfDays) {
        sqlUpdate(SQL_DELETE_HISTORY, serialnumber, limitOfDays);
    }

    public void saveSensorHistory(Date currentsampletime,
            byte measureUnit,
            double val,
            int sensorUid,
            int sensorType,
            int serialnumber,
            double rawVal) {

        Timestamp ts = new java.sql.Timestamp(currentsampletime.getTime());

        sqlUpdate(SQL_INSERT_HISTORY,
                ts,
                measureUnit,
                val,
                sensorUid,
                sensorType,
                serialnumber,
                rawVal
        );
    }

    public void saveSensor(
            int serialnumber,
            Date currentsampletime,
            int sensorUid,
            int sensorType,
            byte measureUnit,
            double avgVal,
            double avgMicroVolt) {

        DebugOutput.logStation(serialnumber, "saveSensor");
        if (existsSensor(serialnumber, sensorUid, sensorType)) {
            updateSensor(currentsampletime,
                    measureUnit,
                    avgVal,
                    sensorUid,
                    sensorType,
                    serialnumber,
                    avgMicroVolt);
        } else {
            insertSensor(currentsampletime,
                    measureUnit,
                    avgVal,
                    sensorUid,
                    sensorType,
                    serialnumber,
                    avgMicroVolt);
        }
    }

    public boolean existsSensor(int boxUID, int sensorUid, int sensorType) {
        Object[] row = sqlSelectOneRow(SQL_SENSOR_EXISTS, boxUID, sensorUid, sensorType);
        return row != null;
    }

    private void updateSensor(Date currentsampletime,
            byte measureUnit,
            double avgVal,
            int sensorUid,
            int sensorType,
            int serialnumber,
            double avgVoltage) {
        Timestamp ts = new java.sql.Timestamp(currentsampletime.getTime());
        DebugOutput.logStation(serialnumber, "*** updateSensor ***" + Integer.toString(sensorUid));
        sqlUpdate(SQL_SENSOR_UPDATE,
                ts,
                measureUnit,
                avgVal,
                avgVoltage,
                serialnumber,
                sensorUid,
                sensorType
        );
    }

    private void insertSensor(Date currentsampletime,
            byte measureUnit,
            double avgVal,
            int sensorUid,
            int sensorType,
            int serialnumber,
            double avgVoltage) {

        Timestamp ts = new java.sql.Timestamp(currentsampletime.getTime());
        sqlUpdate(SQL_SENSOR_INSERT,
                ts,
                measureUnit,
                avgVal,
                sensorUid,
                sensorType,
                serialnumber,
                avgVoltage
        );
    }

    public void saveStationDesription(IStationDescription station) {
        if (existsStationDesription(station.getStationID())) {
            updateStationDescription(station);
        } else {
            insertStationDescription(station);
        }
    }

    public void insertStationDescription(IStationDescription station) {
        String location = station.getLocation();

        sqlUpdate(SQL_STATION_INSERT,
                station.getPARAM_SERIAL_NUMBER(),
                station.getPARAM_HW_VERSION(),
                station.getPARAM_FW_VERSION(),
                station.getPARAM_SYSTEM_TIME(),
                station.getPARAM_LATTITUDE(), //lat
                station.getPARAM_LONGITUDE(), //lon
                true, //online
                station.getPARAM_AMBIENT_TEMPERATURE(),
                station.getPARAM_AMBIENT_HUMIDITY(),
                station.getPARAM_AMBIENT_LIGHT(),
                station.getPARAM_SOLAR_BAT_VOLTAGE(),
                station.getPARAM_LIFE_BAT_VOLTAGE(),
                station.getPARAM_LIFE_BAT_VOLUME(),
                station.getPARAM_EXT_POWER_VOLTAGE(),
                station.getPARAM_SD_CARD_VOLUME(),
                station.getPARAM_SD_CARD_FREE_SPACE(),
                station.getPARAM_CONNECTION_PERIOD(),
                station.getPARAM_MAIN_SERVER(),
                station.getPARAM_MAIN_LOGIN(),
                station.getPARAM_MAIN_PASSWORD(),
                station.getPARAM_ALTERNATE_SERVER(),
                station.getPARAM_ALTERNATE_LOGIN(),
                station.getPARAM_ALTERNATE_PASSWORD(),
                station.getPARAM_GSM_WIFI_CONNECTION(),
                station.getPARAM_GSM_SIGNAL_QUALITY(),
                station.getPARAM_SIM_CARD_NUMBER(),
                station.getPARAM_APN_FOR_GPRS(),
                station.getPARAM_GPRS_LOGIN(),
                station.getPARAM_GPRS_PASSWORD(),
                station.getPARAM_WIFI_SIGNAL_QUALITY(),
                station.getPARAM_WIFI_NET_NAME(),
                station.getPARAM_WIFI_LOGIN(),
                station.getPARAM_WIFI_PASSWORD(),
                1, //user id
                "", // city
                location// location
        );
    }

    public void updateStationDescription(IStationDescription station) {
        sqlUpdate(SQL_STATION_UPDATE,
                station.getPARAM_HW_VERSION(),
                station.getPARAM_FW_VERSION(),
                station.getPARAM_SYSTEM_TIME(),
                station.getPARAM_AMBIENT_TEMPERATURE(),
                station.getPARAM_AMBIENT_HUMIDITY(),
                station.getPARAM_AMBIENT_LIGHT(),
                station.getPARAM_SOLAR_BAT_VOLTAGE(),
                station.getPARAM_LIFE_BAT_VOLTAGE(),
                station.getPARAM_LIFE_BAT_VOLUME(),
                station.getPARAM_EXT_POWER_VOLTAGE(),
                station.getPARAM_SD_CARD_VOLUME(),
                station.getPARAM_SD_CARD_FREE_SPACE(),
                station.getPARAM_CONNECTION_PERIOD(),
                station.getPARAM_MAIN_SERVER(),
                station.getPARAM_MAIN_LOGIN(),
                station.getPARAM_MAIN_PASSWORD(),
                station.getPARAM_ALTERNATE_SERVER(),
                station.getPARAM_ALTERNATE_LOGIN(),
                station.getPARAM_ALTERNATE_PASSWORD(),
                station.getPARAM_GSM_WIFI_CONNECTION(),
                station.getPARAM_GSM_SIGNAL_QUALITY(),
                station.getPARAM_SIM_CARD_NUMBER(),
                station.getPARAM_APN_FOR_GPRS(),
                station.getPARAM_GPRS_LOGIN(),
                station.getPARAM_GPRS_PASSWORD(),
                station.getPARAM_WIFI_SIGNAL_QUALITY(),
                station.getPARAM_WIFI_NET_NAME(),
                station.getPARAM_WIFI_LOGIN(),
                station.getPARAM_WIFI_PASSWORD(),
                true,
                station.getStationID()
        );

        if (station.existsLocation()) {
            String location = station.getLocation();
            sqlUpdate(SQL_STATION_UPDATE_LOCATION,
                    station.getPARAM_LATTITUDE(),
                    station.getPARAM_LONGITUDE(),
                    location,
                    station.getStationID()
            );
        }

    }

    public boolean existsStationDesription(int stationUID) {
        Object[] airStation;
        airStation = sqlSelectOneRow(SQL_STATION_EXISTS, stationUID);
        if (airStation == null) {
            return false;
        } else {
            return true;
        }
    }

    public void loadAirStationLocation(IStationDescription stationDescription){
        int id = stationDescription.getStationID();
        Object[]  location = sqlSelectOneRow("SELECT \"Latitude\", \"Longitude\"  FROM device_boxmodel where \"SerialNumber\"=?;", id);
        String lat = location[0].toString();
        String lon = location[1].toString();
        stationDescription.setLATTITUDE(lat);
        stationDescription.setLONGITUDE(lon);
    }
    
    public void setOnlineMode(int boxUID, boolean online) {
        sqlUpdate(SQL_BOX_ONLINE_UPDATE, online, boxUID);
    }

    public void setOnlineModeFalse() {
        sqlUpdate(SQL_BOX_SET_ONLINE_FALSE);
    }

    public void clearSensorDatamodel(int stationUID) {
        sqlUpdate(SQL_CLEAR_SENSOR_MODEL, stationUID);
    }

    private final static String SQL_BOX_ONLINE_UPDATE = "UPDATE \"device_boxmodel\" "
            + "SET \"Online\"=? "
            + "WHERE \"SerialNumber\"=?;";

    private final static String SQL_BOX_SET_ONLINE_FALSE = "UPDATE \"device_boxmodel\" "
            + "SET \"Online\"=FALSE; ";

    private final static String SQL_CLEAR_SENSOR_MODEL = "delete from device_sensordatamodel where serialnumber_id=?;";

    private final static String SQL_STATION_INSERT
            = "INSERT INTO device_boxmodel(\n"
            + "            \"SerialNumber\", \"HWVersion\", \"FWVersion\", \"SystemTime\", \"Latitude\", "
            + "            \"Longitude\", \"Online\", \"AmbientTemperature\", \"AmbientHumidity\", "
            + "            \"AmbientLight\", \"SolarBatteryVoltage\", \"LiFeBatteryVoltage\", "
            + "            \"LiFeBatteryVolume\", \"ExternalPowerVoltage\", \"SDCardVolume\", "
            + "            \"SDCardFreeSpace\", \"ConnectionPeriod\", \"MainServer\", \"MainLogin\", "
            + "            \"MainPassword\", \"AlternateServer\", \"AlternateLogin\", \"AlternatePassword\", "
            + "            \"GSMWiFiConnection\", \"GSMSignalQuality\", \"SIMCardNumber\", \"APNForGPRS\", "
            + "            \"GPRSLogin\", \"GPRSPassword\", \"WiFiSignalQuality\", \"WiFiNetName\", "
            + "            \"WiFiLogin\", \"WiFiPassword\", \"Holder_id\", \"city\", \"location\")"
            + "    VALUES (?, ?, ?, ?, ?, "
            + "            ?, ?, ?, ?, "
            + "            ?, ?, ?, "
            + "            ?, ?, ?, "
            + "            ?, ?, ?, ?, "
            + "            ?, ?, ?, ?, "
            + "            ?, ?, ?, ?, "
            + "            ?, ?, ?, ?, "
            + "            ?, ?, ?, ?, ?);";

    private static final String SQL_STATION_UPDATE
            = "UPDATE \"device_boxmodel\" "
            + "SET "
            + "\"HWVersion\"=?, "
            + "\"FWVersion\"=?, "
            + "\"SystemTime\"=?, "
            + "\"AmbientTemperature\"=?, "
            + "\"AmbientHumidity\"=?, "
            + "\"AmbientLight\"=?, "
            + "\"SolarBatteryVoltage\"=?, "
            + "\"LiFeBatteryVoltage\"=?, "
            + "\"LiFeBatteryVolume\"=?, "
            + "\"ExternalPowerVoltage\"=?, "
            + "\"SDCardVolume\"=?, "
            + "\"SDCardFreeSpace\"=?, "
            + "\"ConnectionPeriod\"=?, "
            + "\"MainServer\"=?, "
            + "\"MainLogin\"=?, "
            + "\"MainPassword\"=?, "
            + "\"AlternateServer\"=?, "
            + "\"AlternateLogin\"=?, "
            + "\"AlternatePassword\"=?, "
            + "\"GSMWiFiConnection\"=?, "
            + "\"GSMSignalQuality\"=?, "
            + "\"SIMCardNumber\"=?, "
            + "\"APNForGPRS\"=?, "
            + "\"GPRSLogin\"=?, "
            + "\"GPRSPassword\"=?, "
            + "\"WiFiSignalQuality\"=?, "
            + "\"WiFiNetName\"=?, "
            + "\"WiFiLogin\"=?, "
            + "\"WiFiPassword\"=?, "
            + "\"Online\"=? "
            + "WHERE "
            + "\"SerialNumber\"=?;";

    private static final String SQL_STATION_UPDATE_LOCATION
            = "UPDATE \"device_boxmodel\" "
            + "SET "
            + "\"Latitude\"=?, "
            + "\"Longitude\"=?, "
            + "\"location\"=? "
            + "WHERE "
            + "\"SerialNumber\"=?;";

    private static final String SQL_STATION_EXISTS
            = "SELECT "
            + "\"SerialNumber\" "
            + "FROM "
            + "\"device_boxmodel\" "
            + "WHERE "
            + "\"SerialNumber\"=? "
            + "LIMIT 1;";

    private final static String SQL_DELETE_HISTORY = "delete from device_sensorhistorymodel "
            + "where serialnumber_id=? \n"
            + "and currentsampletime < current_date - ?;";

    private final static String SQL_SENSOR_INSERT = "INSERT INTO device_sensordatamodel("
            + " currentsampletime, "
            + " measure_unit, "
            + " val, "
            + " sensor_uid, "
            + " sensor_type, "
            + " serialnumber_id, "
            + " raw_val)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?);";

    private final static String SQL_INSERT_HISTORY = "INSERT INTO device_sensorhistorymodel("
            + " currentsampletime,"
            + " measure_unit,"
            + " val,"
            + " sensor_uid,"
            + " sensor_type, "
            + " serialnumber_id,"
            + " raw_val)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?);";

    private final static String SQL_SENSOR_UPDATE = "UPDATE device_sensordatamodel "
            + "SET currentsampletime=?, measure_unit=?, val=?, "
            + "raw_val=? "
            + "WHERE serialnumber_id = ? and sensor_uid = ? and sensor_type = ?;";

    private final static String SQL_SENSOR_EXISTS
            = "select id from device_sensordatamodel "
            + "where serialnumber_id = ? and sensor_uid = ? and sensor_type = ?;";

}
