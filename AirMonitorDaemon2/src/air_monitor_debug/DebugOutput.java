package air_monitor_debug;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DebugOutput {

    public static final int OUTPUT_FILE = 1;
    public static final int OUTPUT_CONSOLE = 2;
    public static final int OUTPUT_CONSOLE_FILE = OUTPUT_FILE | OUTPUT_CONSOLE;

    public static int output = OUTPUT_CONSOLE_FILE;
    public static String logPath = "";

    public static void printByteArray(int aSize, byte[] buffer) {
        final int leftPad = 26;
        int cb, sub = 0;
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("%" + leftPad + "s", ""));

        for (int i = 0; i < aSize; i++) {
            sb.append('$');
            cb = buffer[i] & 0xFF;
            if (cb < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(cb).toUpperCase());
            sb.append(' ');
            sub++;
            if (sub > 15) {
                //System.out.println(sb.toString());
                log(sb.toString());
                sub = 0;
                sb.delete(leftPad, sb.length());
            }
        }
        if ((sub > 0) && (sub < 16)) {
            //System.out.println(sb.toString());
            log(sb.toString());
        }
    }

    public static void log(String msg) {
        String timestamp = getTimeStamp();
        if((output & OUTPUT_CONSOLE) != 0)
            writeLnStd(timestamp + " " + msg);
        if((output & OUTPUT_FILE) != 0)
            writeLnToFile(timestamp + " " + msg);
    }

    public static void logStation(int boxUID, String msg) {
        log(" box " + Integer.toString(boxUID) + ": " + msg);
    }

    public static String shortToHexStr(short val) {
        byte[] ret = new byte[2];
        ret[0] = (byte) (val & 0xff);
        ret[1] = (byte) ((val >> 8) & 0xff);
        String hex = String.format("%02X", ret[1]) + String.format("%02X", ret[0]);
        return hex;
    }

    private static String getTimeStamp() {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return f.format(new Date());
    }

    private static String getNewFileName() {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String fn = "log-" + f.format(new Date()) + ".txt";
        return fn;
    }
    
    public static void writeLnStd(String str) {
        if ((output & OUTPUT_CONSOLE) != 0) {
            System.out.println(str);
        }
    }

    private static final int fMaxSize = 100000000;
    private static String fFileName = ""; 
    
    public static void writeLnToFile(String str) {
        try {
            if (fFileName.length() == 0){
                //create new file
                fFileName = getNewFileName();
                Path path = Paths.get(logPath, fFileName);
                File f = new File(path.toString());
                if (f.exists()){
                    throw new IOException("Log file " + fFileName + " already exists.");
                }
                Files.write(path, (str + "\n").getBytes() , StandardOpenOption.CREATE);
            }
            else {
                Path path = Paths.get(logPath, fFileName);
                File f = new File(path.toString());
                if (!f.exists()){
                    //that is strange, file must exists 
                    Files.write(path, (str + "\n").getBytes() , StandardOpenOption.CREATE);
                }
                else if (f.exists() && f.length() < fMaxSize)
                    Files.write(path, (str + "\n").getBytes() , StandardOpenOption.APPEND);
                else {
                    fFileName = getNewFileName();
                    path = Paths.get(logPath, fFileName);
                    f = new File(path.toString());
                    if (f.exists()){
                        throw new IOException("Log file " + fFileName + " already exists.");
                    }
                    Files.write(path, (str + "\n").getBytes() , StandardOpenOption.CREATE);
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
