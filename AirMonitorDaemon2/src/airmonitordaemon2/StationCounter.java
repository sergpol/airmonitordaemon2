/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airmonitordaemon2;

import ambience_db.AmbienceDbManager;

/**
 *
 * @author SAP
 */
public class StationCounter {
    private int fStationId = 0;
    private int fCounter = 0;
    //private AmbienceDbManager fDbManager;
    
    public StationCounter(int stationId){
        fStationId = stationId;
//        fDbManager = new AmbienceDbManager();
    }
    
    public int getStationId(){
        return fStationId;
    }
            
    public int increaseCounter(){
        fCounter++;
        return fCounter;
//        if(fCounter==1){
//            fDbManager.setOnlineMode(fStationId, true);
//        }
    }
            
    public int decreaseCounter(){
        fCounter--;
        return fCounter;
//        if(fCounter==0){
//            fDbManager.setOnlineMode(fStationId, false);
//        }
    }
}
