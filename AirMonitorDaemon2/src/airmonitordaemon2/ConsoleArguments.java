/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airmonitordaemon2;


import air_monitor_debug.DebugOutput;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import static org.kohsuke.args4j.ExampleMode.ALL;
import org.kohsuke.args4j.Option;

/**
 *
 * @author Sanya
 */
public class ConsoleArguments
{
  @Option(name = "-tcpp", usage = "TCP Port for Device Connection")
  private int tcpListenedPort = 0;
  
  @Option(name = "-dbh", usage = "Data Base Connection Host")
  private String dbHost = null;
  
  @Option(name = "-dbp", usage = "Data Base Connection Port")
  private int dbPort = 0;
  
  @Option(name = "-dbn", usage = "Data Base Name")
  private String dbName = null;
  
  @Option(name = "-dbu", usage = "Data Base User")
  private String dbUser = null;
  
  @Option(name = "-dbk", usage = "Data Base User Password")
  private String dbPassword = null;
  
  @Option(name = "-wdb", usage = "Write to database")
  private int wdb = 0;
  
  @Option(name = "-logpath", usage = "Path to log file")
  private String logPath = null;
  
  @Option(name = "-parmpath", usage = "Path to sensor's parameters files")
  private String parmPath = null;
  
  @Option(name = "-cdbh", usage = "Cassandra Data Base Connection Host")
  private String cdbHost = null;

  @Option(name = "-cdbn", usage = "Cassandra Data Base Name")
  private String cdbName = null;
  
  
  public ConsoleArguments(String[] args) //throws IOException
  {
    CmdLineParser parser = new CmdLineParser(this);
    // if you have a wider console, you could increase the value;
    // here 80 is also the default
    parser.setUsageWidth(160);
    try
    {
      // parse the arguments.
      parser.parseArgument(args);
      // you can parse additional arguments if you want.
      // parser.parseArgument("more","args");
      // after parsing arguments, you should check
      // if enough arguments are given.
      //if(arguments.isEmpty()) throw new CmdLineException(parser,"No argument is given");
    }
    catch(CmdLineException e)
    {
        DebugOutput.log("ERROR while parsing parameters : " + parser.printExample(ALL));
    }
    
    // this will redirect the output to the specified output
    //DebugOutput.log(out);
    //if(recursive) DebugOutput.log("-r flag is set");
    //if(data) DebugOutput.log(("-custom flag is set");
    //DebugOutput.log("-str was "+str);
    //if(num >= 0) DebugOutput.log("-n was "+num);
    // access non-option arguments
    //DebugOutput.log("other arguments are:");
    //for(String s : arguments) DebugOutput.log(s);

//local db
    if(tcpListenedPort == 0) tcpListenedPort = 6001;
    if(dbHost == null) dbHost = "localhost"; 
    if(dbPort == 0) dbPort = 5432;
    if(dbName == null) dbName = "ambiencedata";
    if(dbUser == null) dbUser = "admin";
    if(dbPassword == null) dbPassword = "admin";
    
    
    if(logPath == null) logPath = "";
    if(parmPath == null) parmPath = "";

    if(cdbHost == null) cdbHost = "127.0.0.1"; 
    if(cdbName == null) cdbName = "ambience";
    
    
////db on server
//    if(tcpListenedPort == 0) tcpListenedPort = 6001;
//    if(dbHost == null) dbHost = "107.170.254.113";
//    if(dbPort == 0) dbPort = 5432;
//    if(dbName == null) dbName = "ambiencedata";
//    if(dbUser == null) dbUser = "postgres"; 
//    if(dbPassword == null) dbPassword = "tes2wSf4";  
       
//db on server
//    if(tcpListenedPort == 0) tcpListenedPort = 6001;
//    if(dbHost == null) dbHost = "127.0.0.1";
//    if(dbPort == 0) dbPort = 5432;
//    if(dbName == null) dbName = "ambiencedata";
//    if(dbUser == null) dbUser = "postgres"; 
//    if(dbPassword == null) dbPassword = "postgres";  

  }

  public String getParmPath()
  {
    return parmPath;
  }
  
  public int getTcpListenedPort()
  {
    return tcpListenedPort;
  }

  public String getDBHost()
  {
    return dbHost;
  }

  public String getCassandraDBHost()
  {
    return cdbHost;
  }

  public int getDBPort()
  {
    return dbPort;
  }

  public String getDBName()
  {
    return dbName;
  }

  public String getCassandraDBName()
  {
    return cdbName;
  }
    
  public String getDBUser()
  {
    return dbUser;
  }

  public String getDBPassword()
  {
    return dbPassword;
  }
  
  public boolean getSaveBigData(){
      return (wdb==0 ? false: true);
  }
  
  public String getLogPath(){
      return logPath;
  }
}
