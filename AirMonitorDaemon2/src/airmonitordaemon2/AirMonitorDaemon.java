package airmonitordaemon2;

import air_monitor_debug.DebugOutput;
import ambience_db.AmbienceDbManager;
import ambience_db.CassandraDBManager;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class AirMonitorDaemon {

    private static ArrayList<StationCounter> fStationLst = new ArrayList<StationCounter>();
    private static ConsoleArguments arguments;
    private static volatile boolean fServerInWork = true;
    private static ServerSocket fClientListener = null;
    private static final ExecutorService fClientThreadPool = Executors.newFixedThreadPool(100);
    
    public static void main(String[] args) {

        //Read Command Line Arguments
        arguments = new ConsoleArguments(args);
        DebugOutput.logPath = arguments.getLogPath();
        log("AirMonitorServer : Service Started");
        log(" - TCP Port = " + Integer.toString(arguments.getTcpListenedPort()));
        log(" - DB Host = " + arguments.getDBHost());
        log(" - DB Port = " + Integer.toString(arguments.getDBPort()));
        log(" - DB Name = " + arguments.getDBName());
        log(" - DB User Name = " + arguments.getDBUser());
        log(" - DB User Pass = " + arguments.getDBPassword());
        log(" - Write to DB = " + Boolean.toString(arguments.getSaveBigData()));
        log(" - Log path = " + arguments.getLogPath());
        log(" - Sensors config path = " + arguments.getParmPath());

        AmbienceDbManager dbManager = new AmbienceDbManager();
        dbManager.setOnlineModeFalse();
        
        //Create Shutdown Hook
        Thread runtimeHookThread = new Thread() {
            @Override
            public void run() {
                shutdownHook();
            }
        };
        Runtime.getRuntime().addShutdownHook(runtimeHookThread);

        AmbienceDbManager.init(arguments.getDBHost(),
                arguments.getDBPort(),
                arguments.getDBName(),
                arguments.getDBUser(),
                arguments.getDBPassword());
        if(arguments.getSaveBigData()){        
            String serverURL = arguments.getCassandraDBHost();
            String cassandraDBName = arguments.getCassandraDBName();
            CassandraDBManager.connect(serverURL, cassandraDBName);
        }

        try {
            fClientListener = new ServerSocket(arguments.getTcpListenedPort());
            log("AirMonitorServer : Opened Port = " + Integer.toString(arguments.getTcpListenedPort()));
            log("AirMonitorServer : IP Address = " + InetAddress.getLocalHost().getHostAddress());

            Socket ClientSocket = null;

            while (fServerInWork) {
                try {
                    ClientSocket = fClientListener.accept();
                    log("AirMonitor Server : Connection accepted");
//                    ClientSocket.setSoTimeout(10000);
                    ClientSocket.setSoTimeout(120 * 1000);

                } catch (IOException e) {
                    log("AirMonitorServer: Listening aborted." + e.toString());
                    continue;
                }

//                fClientThreadPool.execute(new SocketHandler(ClientSocket, arguments.getSaveBigData()));
                fClientThreadPool.execute(new SocketHandler(ClientSocket, arguments));
                //Runnable connectionHandler = new SocketHandler(ClientSocket, arguments.getSaveBigData());
                //new Thread(connectionHandler).start();

            }
            //fClientThreadPool.shutdown();
            SocketHandler.terminate();  // terminates all sockets
            try{
                fClientThreadPool.awaitTermination(120, TimeUnit.SECONDS);//waiting until all threads finishing
            }catch(InterruptedException e){
                log(e.getMessage());
            }
            fClientListener.close();
            dbManager.setOnlineModeFalse();

            log("AirMonitorServer : Closed Port = " + Integer.toString(arguments.getTcpListenedPort()));
        } catch (IOException ex) {
            log("AirMonitorServer : Error while openning Port "
                    + Integer.toString(arguments.getTcpListenedPort())
                    + ". " + ex.getMessage());
            //fServerInWork = false;
        } finally {
//:Cassandra            CassandraDBManager.closeConnection();
        }

        log("AirMonitorServer : Stop");

    }

    private static void shutdownHook() {
        log("ShutdownHook Started");
        fServerInWork = false;
        SocketHandler.terminate();
        try {
            fClientListener.close();
        } catch (IOException e) {
            log("AirMonitorServer : Error while closing server socket. " + e.toString());
        }

        
        AmbienceDbManager dbManager = new AmbienceDbManager();
        dbManager.setOnlineModeFalse();
 
//        fClientThreadPool.awaitTermination(120, TimeUnit.SECONDS);//waiting until all threads finishing

 
        log("ShutdownHook Completed");
    }

    private static void log(String msg) {
        DebugOutput.log(msg);
    }

    private static void err(String msg) {
        DebugOutput.log("Error: " + msg);
    }

    private static String getTimeStamp() {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return f.format(new Date());
    }

    public static ConsoleArguments getArguments() {
        return arguments;
    }
    
    public static int increaseStationCounter(int stationId){
        for(StationCounter station : fStationLst){
            if(station.getStationId() == stationId){
                int cnt = station.increaseCounter();
                DebugOutput.logStation(stationId, "station counter = " + cnt);
                return cnt;
            }
        }
        StationCounter sc = new StationCounter(stationId);
        fStationLst.add(sc);
        int cnt = sc.increaseCounter();
        DebugOutput.logStation(stationId, "station counter = " + cnt);
        return cnt;
    }
    
    public static int decreaseStationCounter(int stationId){
        for(StationCounter station : fStationLst){
            if(station.getStationId() == stationId){
                int cnt = station.decreaseCounter();
                DebugOutput.logStation(stationId, "station counter = " + cnt);
                return cnt;
            }
        }
        
        return -1; //station was not found
    }
}
