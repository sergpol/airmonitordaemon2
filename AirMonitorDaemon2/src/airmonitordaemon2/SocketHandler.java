/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airmonitordaemon2;

import air_monitor_station.TcpPacketIO;
import java.io.IOException;
import java.net.Socket;

import air_monitor_debug.DebugOutput;
import air_monitor_station.AirMonitorCrcError;
import air_monitor_station.StationDescription;
import air_monitor_station.AirMonitorResponseError;
import air_monitor_station.SensorsArray;
//import air_monitor_database.DataBaseWorker;
import air_monitor_station.AirMonitorException;
import air_monitor_station.SensorsArray2;
import air_monitor_station.SensorsArrayBase;
import ambience_db.AmbienceDbManager;
import ambience_db.CassandraDBManager;
import java.sql.Timestamp;

/**
 *
 * @author SAP
 */
//how to interrupt: http://www.javaspecialists.eu/archive/Issue056.html
public class SocketHandler implements Runnable {

    //private  HandlersLst
    private static volatile boolean fRunning = true;
    private static final int EMPTY_PACKET_LENGTH = 12;
    private final Socket fSocket;
    private final TcpPacketIO fPacketIO;
    private final StationDescription fStationDescr;
    private SensorsArrayBase fSensorsAry;
    //private DataBaseWorker fDBW;
    private AmbienceDbManager fDbManager = null;
    private CassandraDBManager fBigDataManager = null;
    private final ConsoleArguments fArguments;
    int fStationID = 0;

//    public SocketHandler(Socket clientSocket, boolean writeToDb) throws IOException {
    public SocketHandler(Socket clientSocket, ConsoleArguments arguments) throws IOException {
        fArguments = arguments;
        fSocket = clientSocket;
        fPacketIO = new TcpPacketIO(fSocket);
        //fDBW = new DataBaseWorker();
        fDbManager = new AmbienceDbManager();
        fStationDescr = new StationDescription(fDbManager);
    }

    //private static final long DB_WRITE_INTERVAL = 600; //10 min
    final static int SAVE_POINT_INTERVAL = 30; //sec
        final static int HISTORY_SAVE_POINT_INTERVAL = 30; //sec
    final static int DEL_HISTORY_SAVE_POINT_INTERVAL = 60* 60 * 24; //sec

    public static void terminate() {
        fRunning = false;
    }

    @Override
    public void run() {
        //int fStationID = 0;
        int sleeping = 0;
        try {
            DebugOutput.log("<***-------------------------------------------------------***>");
            DebugOutput.log("AirMonitorClientHandler : Start");

            DebugOutput.log("AirMonitorClientHandler : Local Address = " + fSocket.getLocalAddress());
            DebugOutput.log("AirMonitorClientHandler : Local Port = " + Integer.toString(fSocket.getLocalPort()));

            DebugOutput.log("AirMonitorClientHandler : Inet Address = " + fSocket.getInetAddress());
            DebugOutput.log("AirMonitorClientHandler : Inet Port = " + Integer.toString(fSocket.getPort()));

            receiveStationDescription();
            fStationID = fStationDescr.getStationID();
            fStationDescr.saveStationDesription();
            AirMonitorDaemon.increaseStationCounter(fStationID);
            setOnlineMode(true);
            DebugOutput.logStation(fStationID, "Set online");

            if (fArguments.getSaveBigData()) {
                fBigDataManager = new CassandraDBManager();
            } else {
                fBigDataManager = null;
            }

            float temperature = fStationDescr.getPARAM_AMBIENT_TEMPERATURE();
            float humidity = fStationDescr.getPARAM_AMBIENT_HUMIDITY();

            fDbManager.clearSensorDatamodel(fStationID);

            if (fStationDescr.getPARAM_FW_VERSION().equals("")) {//old station

                fSensorsAry = new SensorsArray(fStationID,
                        fStationDescr,
                        //fStationDescr.getPARAM_SENSOR_UIDS(),
                        temperature,
                        humidity,
                        AirMonitorDaemon.getArguments().getParmPath());
                
                loadAirStationLocation();
                

            } else if (fStationDescr.getPARAM_FW_VERSION().equals("1.0")) {//new station starling
                sleeping = 2000; //2 sec
                if(fStationDescr.getPARAM_FW_VERSION().equals("1.0"))
                    loadAirStationLocation();
                fSensorsAry = new SensorsArray2(fStationID, fStationDescr);
            } else if (fStationDescr.getPARAM_FW_VERSION().equals("1.1")) {//new station starling
                sleeping = 2000; //2 sec
                fSensorsAry = new SensorsArray2(fStationID, fStationDescr);
            } else {
                throw new AirMonitorException("Unkonw FW_VERSION: " + fStationDescr.getPARAM_FW_VERSION());
            }

            fDbManager.deleteOutOfRangeHistory(fStationID, 30); //delete old data in postgresql

            processPackets2(fStationID, sleeping);

        } catch (IOException e) {
            DebugOutput.logStation(fStationID, "AirMonitorSocketHandler IOException:  -> " + e.getMessage());
        } catch (AirMonitorResponseError e) {
            DebugOutput.logStation(fStationID, "AirMonitorSocketHandler AirMonitorResponseError:  -> " + e.getMessage());
        } catch (AirMonitorCrcError e) {
            DebugOutput.logStation(fStationID, "AirMonitorSocketHandler AirMonitorCrcError:  -> " + e.getMessage());
        } catch (AirMonitorException e) {
            DebugOutput.logStation(fStationID, "AirMonitorSocketHandler error:  -> " + e.getMessage());
        } finally {
            if (fStationID != 0) {
                int cnt = AirMonitorDaemon.decreaseStationCounter(fStationID);
                if (cnt == 0) {
                    setOnlineMode(false);
                    DebugOutput.logStation(fStationID, "Set offline");
                }
            }

            //FIXME: close connection in AirMonitorDaemon
//            if (fBigDataManager != null) {
//                CassandraDBManager.closeConnection();
//            }
            try {
                fSocket.close();
                DebugOutput.logStation(fStationID, "AirMonitor Socket Handler : Socket closed");
            } catch (IOException e) {
                DebugOutput.logStation(fStationID, "AirMonitor Socket Handler : Error while closing socket");
            }
            DebugOutput.logStation(fStationID, "AirMonitor Client Handler : Stop");
            DebugOutput.logStation(fStationID, "<***-------------------------------------------------------***>");
        }
    }

    public void setOnlineMode(boolean mode) {
        if (fStationID != 0) {
            fDbManager.setOnlineMode(fStationID, mode);
        }
    }

//    public void processPackets(int stationID) throws IOException,
//            AirMonitorResponseError,
//            AirMonitorCrcError,
//            AirMonitorException {
//
//        Timestamp savePoint = new Timestamp(0);
//        Timestamp historySavePoint = new Timestamp(0);
//        Timestamp deleteHistorySavePoint = new Timestamp(0);
//
//        Timestamp packetTimestamp;
//
//        while (fRunning) {
//            DebugOutput.logStation(stationID, "Receiving sensors data");
//            if (receiveStationCurrentData()) {
//                packetTimestamp = fSensorsAry.getSqlTimestamp();
//                if (fBigDataManager != null) {
//                    fSensorsAry.saveMeteringsHistory(fBigDataManager);//Cassandra db
//                }
//                if (packetTimestamp.compareTo(savePoint) > 0) {
//                    fSensorsAry.saveAvgMeterings(fDbManager);
//                    DebugOutput.logStation(stationID, "*** Last data saved to db " + savePoint.toString());
//
//                    if (packetTimestamp.compareTo(historySavePoint) > 0) {
//                        fSensorsAry.saveAvgMeteringsHistory(fDbManager);
//                        DebugOutput.logStation(stationID, "*** History saved to db " + savePoint.toString());
//                        SensorsArrayBase.calcNewSavePoint(HISTORY_SAVE_POINT_INTERVAL, packetTimestamp, historySavePoint);
//                    }
//
//                    if (packetTimestamp.compareTo(deleteHistorySavePoint) > 0) {
//                        fDbManager.deleteOutOfRangeHistory(stationID, 30);
////                        DebugOutput.logStation(fStationID, "*** Out of time limit history was deleted " + savePoint.toString());
//                        SensorsArrayBase.calcNewSavePoint(DEL_HISTORY_SAVE_POINT_INTERVAL, packetTimestamp, deleteHistorySavePoint);
//                    }
//
//                    fSensorsAry.resetMetering();
//                    SensorsArrayBase.calcNewSavePoint(SAVE_POINT_INTERVAL, packetTimestamp, savePoint);
//                }
//            } else {
//                DebugOutput.logStation(stationID, "*** Empty packet received ***");
//                try {
//                    int sec = 60; //1000 milliseconds is one second.
//                    DebugOutput.logStation(stationID, "Waiting " + Integer.toString(sec) + " sec after empty packet receiving...");
//                    Thread.sleep(sec * 1000);
//                } catch (InterruptedException e) {
//                    break;
//                }
//            }
////            try {
////                Thread.sleep(2000);//2 sec
////            } catch (InterruptedException e) {
////                break;
////            }
//        }//while
//
//    }
    public void processPackets2(int stationID, int sleeping) throws IOException,
            AirMonitorResponseError,
            AirMonitorCrcError,
            AirMonitorException {

        Timestamp savePoint = new Timestamp(0);
        Timestamp deleteHistorySavePoint = new Timestamp(0);

        Timestamp packetTimestamp;

        while (fRunning) {
            DebugOutput.logStation(stationID, "Receiving sensors data by new communication protocol");
            if (receiveStationCurrentData()) {
                packetTimestamp = fSensorsAry.getSqlTimestamp();
                if (fBigDataManager != null) {
                    fSensorsAry.saveMeteringsHistory(fBigDataManager);//Cassandra db
                }

                DebugOutput.logStation(stationID, "fSensorsAry.saveMeterings();");
                fSensorsAry.saveMeterings(fDbManager);
                DebugOutput.logStation(stationID, "saved fSensorsAry.saveMeterings();");

                if (packetTimestamp.compareTo(savePoint) > 0) {
                    fSensorsAry.saveAvgMeteringsHistory(fDbManager);
                    fSensorsAry.resetMetering();
                    SensorsArrayBase.calcNewSavePoint(HISTORY_SAVE_POINT_INTERVAL, packetTimestamp, savePoint);
                }

                if (packetTimestamp.compareTo(deleteHistorySavePoint) > 0) {
                    fDbManager.deleteOutOfRangeHistory(stationID, 30);
                    //DebugOutput.logStation(fStationID, "*** Out of time limit history was deleted " + savePoint.toString());
                    SensorsArrayBase.calcNewSavePoint(DEL_HISTORY_SAVE_POINT_INTERVAL, packetTimestamp, deleteHistorySavePoint);
                }

            } else {
                DebugOutput.logStation(stationID, "*** Empty packet received ***");
                try {
                    int sec = 60; //1000 milliseconds is one second.
                    DebugOutput.logStation(stationID, "Waiting " + Integer.toString(sec) + " sec after empty packet receiving...");
                    Thread.sleep(sec * 1000);
                } catch (InterruptedException e) {
                    DebugOutput.logStation(stationID, e.getMessage());
                    break;
                }
            }
            if (sleeping > 0) {
                try {
                    Thread.sleep(sleeping);
                } catch (InterruptedException e) {
                    DebugOutput.logStation(stationID, e.getMessage());
                    break;
                }
            }
        }//while
    }

    private boolean receiveStationCurrentData() throws IOException,
            AirMonitorResponseError, AirMonitorCrcError {
        int stationID = fStationDescr.getStationID();
        fPacketIO.buildRequestGetCurrentData();
        fPacketIO.writePacket();
        fPacketIO.readPacket();
        if (fPacketIO.getPacketLength() == EMPTY_PACKET_LENGTH) {
            DebugOutput.logStation(stationID, "*** EMPTY PACKET RECEIVED ***");
            return false;
        } else {
            int cmdId = fPacketIO.getCommandId(TcpPacketIO.RESPONSE_CMD_OFFSET);
            if (cmdId != TcpPacketIO.GET_CURRENT_DATA) {
                throw new AirMonitorResponseError("Response command must be GET_CURRENT_DATA");
            }
            //parse packet
            DebugOutput.logStation(stationID, "Packet GET_CURRENT_DATA was received successfully ");
            DebugOutput.printByteArray(fPacketIO.getPacketLength(), fPacketIO.getPacketData());
            fSensorsAry.parsePacket(fPacketIO);
            fSensorsAry.debugPrintDataPacket();
        }
        return true;
    }

    private void receiveStationDescription() throws IOException, AirMonitorCrcError {
        fPacketIO.buildRequestGetDeviceInfo();
        fPacketIO.writePacket();
        fPacketIO.readPacket();
        DebugOutput.log("Packet GET_DEVICE_INFO was received successfully ");
        DebugOutput.printByteArray(fPacketIO.getPacketLength(), fPacketIO.getPacketData());
        fStationDescr.parsePacket(fPacketIO);
        fStationDescr.logDebugInfo();
    }

    public int getStationId() {
        return fStationID;
    }
    
    public void loadAirStationLocation(){
        fDbManager.loadAirStationLocation(fStationDescr);
    }

//    public void setOnlineMode(int boxUID, boolean online) {
//        fDBW.sqlUpdate(SQL_BOX_ONLINE_UPDATE, online, boxUID);
//    }
//
//    public void clearSensorDatamodel(int stationUID) {
//        fDBW.sqlUpdate(SQL_CLEAR_SENSOR_MODEL, stationUID);
//    }
//
//    private final static String SQL_BOX_ONLINE_UPDATE = "UPDATE \"device_boxmodel\" "
//            + "SET \"Online\"=? "
//            + "WHERE \"SerialNumber\"=?;";
//
//    private final static String SQL_CLEAR_SENSOR_MODEL = "delete from device_sensordatamodel where serialnumber_id=?;";
}
