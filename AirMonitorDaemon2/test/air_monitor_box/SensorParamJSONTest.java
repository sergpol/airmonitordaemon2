/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_box;

import air_monitor_station.SensorParamJsonFile;
import air_monitor_station.SensorParamJson;
import air_monitor_station.AirMonitorSensorError;
import air_monitor_station.SensorDefs;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SAP
 */
public class SensorParamJSONTest {
    String path = "D:\\psa\\PyCharmPrj\\ams\\AirMonitor\\"
        + "ServerApplicationForAirMonitor2\\AirMonitorDaemon2\\"
        + "test_config\\";

    
    public SensorParamJSONTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testLoadParams(){
        System.out.println("loadParams");
        List<Integer> uids = Arrays.asList(9001, 10001, 7002);
        SensorParamJsonFile instance = new SensorParamJsonFile(80000, path);
        instance.loadParams(uids);
        try{
            SensorParamJson.ResSensorParamVal p = instance.getResSensorParam(10001, SensorDefs.getSensorType("MICS6814_NH3"));
            assertNotNull(p);
            assertEquals(p.getSensorUid(), 10001);
            assertEquals(p.getSensorType(), SensorDefs.getSensorType("MICS6814_NH3"));
            assertEquals(((SensorParamJson.ResSensorParamVal)p).getR0(), 200.55, 0);
            assertTrue(((SensorParamJson.ResSensorParamVal)p).containsR0());
            assertEquals(((SensorParamJson.ResSensorParamVal)p).getRl(), 100, 0);
            
            SensorParamJson.DigSensorParamVal p1 = instance.getDigSensorParam(7002, SensorDefs.getSensorType("SHT21_TEMPERATURE"));
            assertNotNull(p1);
            assertEquals(p1.getSensorUid(), 7002);

            SensorParamJson.DigSensorParamVal p2 = instance.getDigSensorParam(7002, SensorDefs.getSensorType("SHT21_HUMIDITY"));
            assertNotNull(p2);
            assertEquals(p2.getSensorUid(), 7002);
            
        }catch(AirMonitorSensorError e){
            fail(e.getMessage());
        }    
    }
        
    @Test
    public void testLoadJsonSensors() {
        System.out.println("loadJsonSensors");
        
        SensorParamJsonFile instance = new SensorParamJsonFile(80000, path);
        try{
            JSONObject obj = instance.loadJsonSensors();
            JSONObject sensor9001 = (JSONObject)obj.get("9001");
            String sensorName = (String)sensor9001.get("sensor_name");
            assertEquals(sensorName, "SGX4DT_H2S");
            Object sensor7002 = obj.get("7002");
            assertTrue(sensor7002 instanceof JSONArray);
            for(Object s: (JSONArray)sensor7002){
                assertTrue(s instanceof JSONObject);
                System.out.println(s);
            }
            
        }
        catch(IOException e){
            System.out.println(e);
        }
        catch(ParseException e){
            System.out.println(e);
        }
    }    
    
    
    
    
    
    
    
    /**
     * Test of parseParams method, of class SensorParamJSON.
     */
//    @Test
//    public void testParseParams() {
//        System.out.println("parseParams");
//        String path = "D:\\psa\\PyCharmPrj\\ams\\AirMonitor\\"
//        + "ServerApplicationForAirMonitor2\\AirMonitorDaemon2\\"
//        + "test_config\\";
//        
//        SensorParamJsonFile instance = new SensorParamJsonFile(80002, path);
//        instance.loadParams();
//        try{
//            SensorParamJson.SensorParamVal p = instance.getSensorParam(1001, SensorDefs.getSensorType("MICS6814_NH3"));
//            assertNotNull(p);
//            assertEquals(p.getSensorUid(), 1001);
//            assertEquals(p.getSensorType(), SensorDefs.getSensorType("MICS6814_NH3"));
//            assertEquals(p.getMultiplier(), 1, 0);
//            assertEquals(p.getOffset(), 0, 0);
//            assertTrue(p instanceof SensorParamJson.ResSensorParamVal);
//            assertEquals(((SensorParamJson.ResSensorParamVal)p).getR0(), 10.9, 0);
//            assertEquals(((SensorParamJson.ResSensorParamVal)p).getRl(), 91, 0);
//            assertTrue(((SensorParamJson.ResSensorParamVal)p).containsR0());
//        }catch(AirMonitorSensorError e){
//            fail("Failed test for 1001: " + e.getMessage());
//        }
//        
//        try{
//            SensorParamJson.ResSensorParamVal p = instance.getResSensorParam(1002, SensorDefs.getSensorType("MICS6814_NH3"));
//            assertNull(p);
//        }catch(AirMonitorSensorError e){
//            fail("Failed test for 1001: " + e.getMessage());
//        }
//        
//        try{
//            SensorParamJson.ResSensorParamVal p = instance.getResSensorParam(1002, SensorDefs.getSensorType("MICS6814_CO"));
//            assertNotNull(p);
//            assertEquals(p.getSensorUid(), 1002);
//            assertEquals(p.getSensorType(), SensorDefs.getSensorType("MICS6814_CO"));
//            assertEquals(p.getMultiplier(), 1.1, 0);
//            assertEquals(p.getOffset(), 2.5, 0);
//            assertEquals(((SensorParamJson.ResSensorParamVal)p).getR0(), 200.55, 0);
//            assertFalse(((SensorParamJson.ResSensorParamVal)p).containsR0());
//            assertEquals(((SensorParamJson.ResSensorParamVal)p).getRl(), 200.55, 0);
//        }catch(AirMonitorSensorError e){
//            fail("Failed test for 1001: " + e.getMessage());
//        }
        
        
        
        
        
    
    
}
