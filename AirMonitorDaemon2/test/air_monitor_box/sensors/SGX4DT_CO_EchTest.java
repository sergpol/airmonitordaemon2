/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_box.sensors;

import air_monitor_station.sensors.SGX4DT_CO_Ech;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SAP
 */
public class SGX4DT_CO_EchTest {
    final double t[] = {-30.0, -20.0, -10.0, 0.0, 10.0, 20.0, 30.0, 40.0, 50.0};
    final double expResult[] = {0.3, 0.417, 0.571, 0.703, 0.9, 1.0, 1.102, 1.17, 1.15};

    
    public SGX4DT_CO_EchTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of calcCoefK method, of class SGX4DT_CO_Ech.
     */
    @Test
    public void testCalcCoefK() {
        System.out.println("calcCoefK");
        SGX4DT_CO_Ech instance = new SGX4DT_CO_Ech();
        assertEquals(t.length, expResult.length);
        for(int i = 0; i<t.length; i++){
            double result = instance.calcCoefK(t[i])/100;
            assertEquals(expResult[i], result, 0.025);
        }
    }
    
    /**
     * Test of calcCoefC method, of class SGX4DT_CO_Ech.
     */
    @Test
    public void testCalcCoefC() {
        System.out.println("calcCoefC");
        SGX4DT_CO_Ech instance = new SGX4DT_CO_Ech();
        for(int i = 0; i<t.length; i++){
            double result = instance.calcCoefC(t[i]);
            assertEquals(0.0, result, 0.0);
        }
    }
    
}
