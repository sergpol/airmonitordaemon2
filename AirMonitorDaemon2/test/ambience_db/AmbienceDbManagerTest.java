/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ambience_db;

import air_monitor_station.DummyStationDescription;
import air_monitor_station.StationDescription;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SAP
 */
public class AmbienceDbManagerTest {
    
    public AmbienceDbManagerTest() {
    }


    /**
     * Test of saveStationDesription method, of class AmbienceDbManager.
     */
    @Test
    public void testExistsStationDesription() {
        System.out.println("existsStationDesription");
        DummyStationDescription station = new DummyStationDescription();
        boolean res = AmbienceDbManager.init("localhost",
                5432,
                "AirStation",
                "admin",
                "admin");

        //System.out.println(res);
        
        AmbienceDbManager instance = new AmbienceDbManager();
        instance.sqlUpdate("DELETE FROM device_boxmodel;");
        boolean rc = instance.existsStationDesription(station.getStationID());
        assertFalse(rc);
        instance.insertStationDescription(station);
        rc = instance.existsStationDesription(station.getStationID());
        assertTrue(rc);
        station.updateLocation(44, -79);
        instance.updateStationDescription(station);
        
        
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    
}
