/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SAP
 */
public class StationDescriptionTest {
    
    public StationDescriptionTest() {
    }

    /**
     * Test of getPARAM_SENSOR_UIDS method, of class StationDescription.
     */
    @Test
    public void testParseSensorList() {
        String params = "1004:34822,1005:33030,1006:34054,5002:35085,6002:33804,7002:2831,8002:2574,9001:34561,9002:33025,10001:34821,11001:33032,12001:33290";
        System.out.println("getPARAM_SENSOR_UIDS");
        StationDescription instance = new StationDescription(null);
        List<Integer> expUIDs = Arrays.asList(1004,1005,1006,5002,6002,7002,8002,9001,9002,10001,11001,12001);
        List<Integer> expTypes = Arrays.asList(34822,33030,34054,35085,33804,2831,2574,34561,33025,34821,33032,33290);
        instance.parseSensorList(params);
        List<Integer> uids = instance.getPARAM_SENSOR_UIDS();
        List<Integer> types = instance.getPARAM_SENSOR_TYPES();
        assertEquals(expUIDs, uids);
        assertEquals(expTypes, types);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    
}
