/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package air_monitor_station;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author SAP
 */
public class SensorDefsTest {
    
    public SensorDefsTest() {
    }

    /**
     * Test of getSensorName method, of class SensorDefs.
     */
    @Test
    public void testGetSensorName() throws Exception {
        System.out.println("getSensorName");
        int sensorType = 0;
        String expResult = "";
        String result = SensorDefs.getSensorName(sensorType);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSensorKind method, of class SensorDefs.
     */
    @Test
    public void testGetSensorKind() throws Exception {
        System.out.println("getSensorKind");
        int sensorType = 0;
        SensorDefs.SensorKind expResult = null;
        SensorDefs.SensorKind result = SensorDefs.getSensorKind(sensorType);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSensorType method, of class SensorDefs.
     */
    @Test
    public void testGetSensorType() throws Exception {
        System.out.println("getSensorType");
        String sensorName = "";
        int expResult = 0;
        int result = SensorDefs.getSensorType(sensorName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of print_sensor_names method, of class SensorDefs.
     */
    @Test
    public void testPrint_sensor_names() {
        System.out.println("print_sensor_names");
        SensorDefs.print_sensor_names();
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    
}

//sensor types
//electrochemical
//SGX4DT_CO: 33025
//SGX4DT_H2S: 34561
//ME3O3_O3: 33538
//ME3NO2_NO2: 34051
//EC420SO2_SO2: 34308
//SGX7NH3_NH3: 34821
//resistive
//MICS6814_NH3: 34822
//MICS6814_CO: 33030
//MICS6814_NO2: 34054
//TGS2442_CO: 33031
//ASMLC_CO: 33032
//TGS2611_CH4: 33289
//ASMLK_CH4: 33290
//MICS2614_O3: 33547
//TGS2602_VOC: 33804
//optical
//GP2Y1010AU0F_PM: 35085
//stub
//ANALOG_DUMMY: 35856
//digital
//MPL3115A2_ABS_PRESSURE: 2574
//MPL3115A2_TEMPERATURE: 3342
//SHT21_HUMIDITY: 2831
//SHT21_TEMPERATURE: 3343
